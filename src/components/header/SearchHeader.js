import React from 'react'
import { View, TextInput, TouchableOpacity, Platform, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import colors from '../style/Colors'
import myStyles from '../style'
import LinearGradient from 'react-native-linear-gradient'

const SearchHeader = (props) => {
  return (
    <LinearGradient start={{ x: 0, y: 1 }} end={{ x: 0, y: 0 }} colors={['#4782c3', '#155396']}>
      <View style={[styles.container]}>
        <TextInput
          style={[styles.cardSearchInput, myStyles.FontLight]}
          placeholder={props.placeholder ? props.placeholder : ''}
          onChangeText={props.value ? props.value : () => alert('please define your props value')}
        />
        <Icon name="search" size={16} style={styles.cardSearchIcon} />
      </View>
    </LinearGradient>
  )
}

export default SearchHeader

const styles = StyleSheet.create({
  container: {
    height: Platform === 'ios' ? 60 : 65,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop:'2%',
    paddingBottom:'25%'
  },
  cardSearchInput: {
    flex: 1,
    backgroundColor: colors.white,
    color: colors.darkSecond,
    borderRadius: 3,
    position: 'relative',
    height: '70%',
    fontSize: 15,
    paddingHorizontal:40
  },
  cardSearchIcon: {
    color: colors.grayScale,
    position: 'absolute',
    top:'35%',
    marginLeft:'5%'
  },
  cardSearchButton: {
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 12,
    marginHorizontal: 8,
    padding: 10,
    borderRadius: 6
  }
})