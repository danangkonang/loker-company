import axios from 'axios';
import { CS_KEY } from 'react-native-dotenv';


/**
 * @function getDataCandidate fun for get data candidate in jobs
 * @param {token, jobId, candidateId} data for request data candidate in jobs
 */

export const getDataCandidate = data => {
  const { token, jobsId, candidateId } = data
  return {
    type: 'GET_DATA_CANDIDATE',
    payload: axios.get(`${CS_KEY}/company/jobs/candidate/${jobsId}/detail/${candidateId}/`, {
      headers: {
        Accept: 'application/json',
        'Content-type': 'aplication/json',
        Authorization: `JWT ${token}`
      }
    })
  }
}

export const educationDetail = (data) => {
	return {
		type: 'DETAIL_EDUCATION',
		payload: data
	}
}