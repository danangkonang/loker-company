import React, { useState } from 'react'
import { View, StyleSheet, TextInput } from 'react-native'
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import IonIcon from 'react-native-vector-icons/Ionicons'
import { connect } from 'react-redux'
// Components
import Text from '../../components/Text'
import colors from '../../components/style/Colors'
import myStyles from '../../components/style'
import Button from '../../components/button'

const LayoutEditRequirement = (props) => {

	const [state, setState] = useState({
		description: null
	})

	const { jobDetail } = props.job

	return (
		<View style={[myStyles.Flex1, { paddingHorizontal: 20, paddingVertical: 10 }]}>
			<View style={{ flexDirection: 'row' }}>
				{/* Minimal experience */}
				<View style={[myStyles.Flex1, { marginVertical: 5 }]}>
					<Text 
						text="Minimal experience"
						color={colors.darkSecond}
						fontSize={16}
						bold
					/>
					<View style={{ flexDirection: 'row', marginVertical: 15}}>
						<TextInput 
							placeholder="1"
							placeholderTextColor={colors.grayScale}
							style={styles.OptionStyle}
						/>
						<View style={[myStyles.Flex1, { justifyContent: 'center', paddingHorizontal: 15}]}>
							<Text 
								text="Year"
								color={colors.darkSecond}
								fontSize={16}
								light
							/>
						</View>
					</View>
				</View>

				{/* Minimal GPA */}
				<View style={[myStyles.Flex1, { marginVertical: 5 }]}>
					<Text 
						text="Minimal GPA"
						color={colors.darkSecond}
						fontSize={16}
						bold
					/>
					<View style={{ flexDirection: 'row', marginVertical: 15}}>
						<TextInput 
							placeholder="4.00"
							placeholderTextColor={colors.grayScale}
							style={styles.OptionStyle}
						/>
						<View style={[myStyles.Flex1, { justifyContent: 'center', paddingHorizontal: 15}]}>
							
						</View>
					</View>
				</View>
			</View>
			
			{/* Minimal education */}
			<View style={{ marginVertical: 5}}>
				<Text 
					text="Minimal education"
					color={colors.darkSecond}
					fontSize={16}
					bold
				/>
				<View style={[styles.TextInput, { flexDirection: 'row', height: 50 }]}>
					<TextInput
						placeholder="SMA"
						placeholderTextColor={colors.grayScale}
						style={[myStyles.Flex1, styles.TextInputStyle]}
					/>
					<View style={[myStyles.Center, myStyles.bgWhite, { width: 30 }]}>
						<MdIcon name="chevron-down" size={18} color={colors.secondary} />
					</View>
				</View>
			</View>

			{/* Major */}
			<View style={{ marginVertical: 5}}>
				<Text 
					text="Major"
					color={colors.darkSecond}
					fontSize={16}
					bold
				/>
				<View style={[styles.TextInput, { flexDirection: 'row', height: 50 }]}>
					<TextInput
						placeholder="SMA"
						placeholderTextColor={colors.grayScale}
						style={[myStyles.Flex1, styles.TextInputStyle]}
					/>
					<View style={[myStyles.Center, myStyles.bgWhite, { width: 30 }]}>
						<MdIcon name="chevron-down" size={18} color={colors.secondary} />
					</View>
				</View>
			</View>

			{/* Personality */}
			<View style={{ marginVertical: 5}}>
				<Text 
					text="Personality"
					color={colors.darkSecond}
					fontSize={16}
					bold
				/>
				<View style={[styles.TextInput, { flexDirection: 'row', height: 50 }]}>
					<TextInput
						placeholder="Search & select personality"
						placeholderTextColor={colors.grayScale}
						style={[myStyles.Flex1, styles.TextInputStyle, { fontFamily: 'DIN-Light'}]}
					/>
					<View style={[myStyles.Center, myStyles.bgWhite, { width: 50 }]}>
						<IonIcon name="md-search" size={20} color={colors.darkScale} />
					</View>
				</View>
			</View>

			{/* Language */}
			<View style={{ marginVertical: 5}}>
				<Text 
					text="Language"
					color={colors.darkSecond}
					fontSize={16}
					bold
				/>
				<View style={[styles.TextInput, { flexDirection: 'row', height: 50 }]}>
					<TextInput
						placeholder="Search & select language"
						placeholderTextColor={colors.grayScale}
						style={[myStyles.Flex1, styles.TextInputStyle, { fontFamily: 'DIN-Light'}]}
					/>
					<View style={[myStyles.Center, myStyles.bgWhite, { width: 50 }]}>
						<IonIcon name="md-search" size={20} color={colors.darkScale} />
					</View>
				</View>
			</View>

			{/* Skills */}
			<View style={{ marginVertical: 5}}>
				<Text 
					text="Skills"
					color={colors.darkSecond}
					fontSize={16}
					bold
				/>
				<View style={[styles.TextInput, { flexDirection: 'row', height: 50 }]}>
					<TextInput
						placeholder="Search & select skills"
						placeholderTextColor={colors.grayScale}
						style={[myStyles.Flex1, styles.TextInputStyle, { fontFamily: 'DIN-Light'}]}
					/>
					<View style={[myStyles.Center, myStyles.bgWhite, { width: 50 }]}>
						<IonIcon name="md-search" size={20} color={colors.darkScale} />
					</View>
				</View>
			</View>

			<View style={{ marginVertical: 5}}>
				<Text 
					text="Job description"
					color={colors.darkSecond}
					fontSize={16}
					bold
				/>
				<View style={[styles.TextInput, { height: 150, paddingVertical: 5 }]}>
					<TextInput
						placeholder={
							state.description === null ? 
								jobDetail.requirement.job_desc ?
									jobDetail.requirement.job_desc
									:
									"Job description"
								:
								state.description
						}
						placeholderTextColor={colors.grayScale}
						style={styles.TextInputStyle}
						onChangeText={(text) => setState({
							...state,
							description: text
						})}
						value={
							state.description === null ? 
								jobDetail.requirement.job_desc
								:
								state.description
						}
						multiline
					/>
				</View>
			</View>

			{/* Button Save */}
			<View style={[myStyles.Center, { width: '100%', marginTop: 10 }]}>
				<Button 
					width="40%"
					text="Save"
					color={colors.white}
					onclick={() => alert('Save Button')}
				/>
			</View>

		</View>
	)
}

// mapStateToProps
const mapStateToProps = (state) => {
	return {
		employer: state.employer,
		job: state.job,
		candidate: state.candidate
	}
}

export default connect(mapStateToProps)(LayoutEditRequirement);

/**
 * @StyleSheet
 */
const styles = StyleSheet.create({
	TextInput: {  
		width: '100%', 
		borderWidth: 2, 
		borderColor: colors.gray, 
		borderRadius: 8, 
		marginVertical: 15,
	},
	TextInputStyle: { 
		paddingLeft: 15,
		paddingRight: 10, 
		color: colors.darkScale, 
		backgroundColor: colors.white,
		fontSize: 14,
		fontFamily: 'DIN-Medium'
	},
	OptionStyle: { 
		borderRadius: 6, 
		borderWidth: 2, 
		borderColor: colors.gray, 
		textAlign: 'center', 
		paddingHorizontal: 15, 
		paddingVertical: 10,
	}
})
