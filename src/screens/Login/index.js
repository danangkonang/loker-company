import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, ScrollView, Text, Image, TouchableOpacity, SafeAreaView, StyleSheet, BackHandler, Dimensions, StatusBar } from 'react-native'
import AsyncStorage from "@react-native-community/async-storage";
// Component
import CustomTextInput from '../../components/customTextInput'
import PasswordInput from '../../components/customTextInput/Password'
import Button from '../../components/button'
import SweetAlert from '../../components/Modal/SweetAlert'
import ErrorText from '../../components/customTextInput/Error'
import Loading from '../../components/loading/LoadingSpinner'
// Styles
import myStyles from '../../components/style'
import colors from '../../components/style/Colors'
import LinearGradient from 'react-native-linear-gradient'
import { employerAuthentication } from '../../_redux/_actions/employer'
//validation 
import { valid_email } from '../../components/validation'

const ScreenHeight = Dimensions.get('window').height
const ScreenWidth = Dimensions.get('window').width

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      errorEmail: false,
      errorEmailText: '',
      hidePassword: true,
      password: '',
      errorPassword: false,
      errorPasswordText: '',
      showError: false,
      showSuccess: false,
      msgErr: '',
      typeAlert: null
    }
  }

  componentDidMount() {
    this.androidBack = BackHandler.addEventListener('hardwareBackPress', () => {
      this._handleBackPress()
      return true
    })
  }

  componentWillUnmount() {
    this.androidBack.remove()
  }

  render() {
    const {
      email,
      errorEmail,
      errorEmailText,
      hidePassword,
      password,
      errorPassword,
      errorPasswordText,
    } = this.state
    const { loading } = this.props.employer
    if (loading) {
      return (
        <SafeAreaView style={myStyles.Flex1}>
          <Loading />
        </SafeAreaView>
      )
    } else {
      return (
      <LinearGradient
         start={{ x: 0, y: 0 }}
         end={{ x: 0, y: 1 }}
         colors={[colors.white, colors.white, '#155396', '#155396']}
         style={myStyles.Flex1}
      >
         <SafeAreaView style={myStyles.Flex1}>
            <View style={[myStyles.Flex1]}>
               <StatusBar hidden={false} backgroundColor={colors.white} barStyle="dark-content" />
                  <View style={styles.Login}>
                     <ScrollView contentContainerStyle={{ height: ScreenHeight * .95 }} showsVerticalScrollIndicator={false}>
                        <Text style={[styles.TextHeader, myStyles.FontBold, myStyles.textCenter]}>
                        Welcome back to
                        </Text>
                        <View style={styles.Logo}>
                        <Image source={require('../../assets/img/logo_cs.png')} style={myStyles.ImgContain} />
                        </View>

                        <View style={[myStyles.Center, styles.Form]}>
                           <CustomTextInput
                              width='70%'
                              marginBottom={8}
                              placeholder="Email Address"
                              type='emailAddress'
                              keyboardType='email-address'
                              borderRadius={4}
                              borderColor={colors.gray}
                              borderWidth={1}
                              onchange={(email) => this.setState({ email, errorEmail: false })}
                              value={email}
                              noCapitalize
                              zIndex={20}
                           />
                           {
                              errorEmail &&
                              <ErrorText
                                 text={errorEmailText}
                              />
                           }
                           <PasswordInput
                              width='70%'
                              borderWidth={1}
                              borderRadius={5}
                              password={hidePassword}
                              value={password}
                              placeholder="Password"
                              onChangeText={(password) => this.setState({ password, errorPassword: false })}
                              onClick={() => this.setState({ hidePassword: !hidePassword })}
                           />
                           {
                              errorPassword &&
                              <ErrorText
                                 width='70%'
                                 text={errorPasswordText}
                              />
                           }
                           <TouchableOpacity onPress={() => this._forgotPassword()} style={styles.ForgotPassword}>
                              <Text style={[styles.ForgotPasswordText, myStyles.FontBold]}>
                              Forgot password?
                              </Text>
                           </TouchableOpacity>
                           <Button
                              marginVertical={15}
                              width='40%'
                              backgroundColor={colors.primary}
                              color={colors.white}
                              text="Login"
                              onclick={() => this.onLogin()}
                              isSave={false}
                           />
                        </View>

                        <LinearGradient start={{ x: 0, y: 1 }} end={{ x: 0, y: 0 }} colors={['#155396', '#4782c3']} style={styles.Footer}>
                           <TouchableOpacity onPress={() => this.handleLink()} style={styles.Link}>
                              <Text style={[styles.LinkText, myStyles.FontBold]}>
                                 Haven't created an account?
                              </Text>
                           </TouchableOpacity>
                        </LinearGradient>

                     </ScrollView>
                  </View>

               <SweetAlert
                  visible={this.state.showError}
                  type={this.state.typeAlert}
                  text={this.state.msgErr}
                  isAlert={true}
                  onOke={() => this.setState({ showError: !this.state.showError })}
                  textOke="Okay"
               />
               <SweetAlert
                  visible={this.state.showSuccess}
                  type={8}
                  text={`Login Success \n You will redirect to homepage.`}
                  isAlert={true}
                  onOke={() => {
                     this.setState({ showSuccess: !this.state.showSuccess })
                     this.props.navigation.navigate('home')
                  }}
                  textOke="Okay"
               />
            </View>
         </SafeAreaView>
      </LinearGradient>
      )
    }
  }

  /**
		@function _validationEmail to validate Input Email Address
	 */
  _validationEmail = (email) => {
    let validate_email = valid_email(email)
    if (email === '') {
      this.setState({
        errorEmail: true,
        errorEmailText: 'Please enter email address!'
      })
    } else if (!validate_email) {
      this.setState({
        errorEmail: true,
        errorEmailText: 'Please enter a valid email address!'
      })
    } else {
      this.setState({
        errorEmail: false,
        errorEmailText: ''
      })
      return true
    }

    return false
  }

	/**
	 	@function _validationPassword to Validate Input Password
	 */
  _validationPassword = (password) => {
    if (password === '') {
      this.setState({
        errorPassword: true,
        errorPasswordText: 'Please enter password!'
      })
    } else {
      this.setState({
        errorPassword: false,
        errorPasswordText: ''
      })
      return true
    }

    return false
  }

	/**
	 	@function onLogin to handle Click Button Login
	 */
   onLogin = async () => {
      const { email, password } = this.state
      
      if (this._validationEmail(email) && this._validationPassword(password)) {
         try {
         await this.props.dispatch(employerAuthentication(email, password))
         let { token } = this.props.employer.employer_data
         if (token) {
            await AsyncStorage.setItem('employerToken', token);
            this.setState({
               showSuccess: true,
               email: '',
               errorEmail: false,
               errorEmailText: '',
               password: '',
               errorPassword: false,
               errorPasswordText: '',
               showError: false,
            })
         } else {
            this.setState({
               showError: !this.state.showError,
               msgErr: 'Login failed, please try again!',
               typeAlert: 7
            })
         }
         } catch (error) {
            if (error == 'Error: Network Error') {
               this.setState({
                  showError: !this.state.showError,
                  msgErr: 'No Internet connection,\n Turn on your wifi/data or try again later',
                  typeAlert: 9
               })
            } else if (error.message === 'timeout of 0ms exceeded') {
               this.setState({
                  showError: !this.state.showError,
                  msgErr: 'Request Timeout, Please Check your Connection',
                  typeAlert: 9
               })
            } else {
               this.setState({
                  showError: !this.state.showError,
                  msgErr: 'Your email/password is wrong!',
                  typeAlert: 7
               })
            }
         }
      }
   }

	/**
		@function _forgotPassword to handle Clicked Forgot Password
	 */
  _forgotPassword = () => {
    // alert('Forgot Password Feature not available')
    const { navigate } = this.props.navigation
    navigate('forgotpassword')
  }

	/**
	 	@function handleLink to Handle Click Haven't created an account
	 Redirect to Registration
	 */
  handleLink = () => {
    const { navigate } = this.props.navigation
    navigate('registrasi')
  }

	/**
		@function _handleBackPress to Handle Back Button
		Exit App if Back
	 */
  _handleBackPress() {
    BackHandler.exitApp();
    return false
  }
}

const mapStateToProps = state => ({
  employer: state.employer
})

export default connect(mapStateToProps)(Login);

const styles = StyleSheet.create({
	Login: {
		width: ScreenWidth,
		backgroundColor: colors.white,
		justifyContent: 'space-between',
	},
	TextHeader: {
		width: '100%',
		height: 40,
		color: colors.primary,
		fontSize: 24,
		marginTop: '20%'
	},
	Logo: {
		width: '100%',
		height: 40,
		paddingHorizontal: '15%',
	},
	Form: {
		flex: 1,
		width: '100%',
	},
	ForgotPassword: {
		width: '100%',
		justifyContent: 'center',
		alignItems: 'flex-end',
		paddingRight: '17%',
		paddingVertical: 5,
		marginBottom: 20,
		marginTop: 5
	},
	ForgotPasswordText: {
		color: colors.primary,
		fontSize: 14,
	},
	Footer: {
		height: '15%',
		width: '100%',
	},
	Link: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	LinkText: {
		color: colors.white, 
		fontSize: 15,
	}
})



// let res =await fetch(`http://128.199.224.138:8080/api/v1/authentication/employer/`,{
      //       method:'POST',
      //       headers: {
      //          'Content-Type': 'application/json'
      //        },
      //        body:JSON.stringify({
      //          username: email,
      //          password: password
      //        })
      //    })
      //    let respon =await res.json()
      //    console.log(respon)
