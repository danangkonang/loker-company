import React from 'react'
import { View, Image, StyleSheet } from 'react-native'
// Components 
import colors from '../style/Colors'
import myStyles from '../style'

const Logo = (props) => {
	return (
		<View style={styles.Logo}>
			<View style={styles.LogoImage}>
				<Image 
					style={[myStyles.ImgContain, { borderWidth: 5, borderRadius: 100/2, margin: -2, borderColor: colors.white }]} 
					source={{uri: props.image ? props.image : 'https://backend.career.support/media/employer/Career_Support_Indonesia.png'}} />
			</View>
		</View>
	)
}

export default Logo;

const styles = StyleSheet.create({
	Logo: {
		height: 100,
		marginTop: -50,
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		zIndex: 999,
	},
	LogoImage: {
		width: 100,
		height: 100,
		borderRadius: 100/2,
		backgroundColor: colors.white,
		borderWidth: 6,
		borderColor: colors.white,
		overflow: 'hidden'
	},
})
