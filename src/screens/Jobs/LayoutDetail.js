import React, { useState } from 'react'
import { View, StyleSheet, Dimensions, TouchableOpacity } from 'react-native'
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import { connect } from 'react-redux'
import { WebView } from 'react-native-webview'
// Components
import myStyles from '../../components/style'
import colors from '../../components/style/Colors';
import Text from '../../components/Text'
import { formatRupiah } from '../../components/utils'
// Dimensions
const screenHeight = Dimensions.get('window').height

const LayoutDetail = (props) => {

	const [viewMore, showViewMore] = useState(false)

	const { jobDetail } = props

	return (
		<View style={[styles.Container, { height: viewMore ? 'auto' : screenHeight * .425, overflow: 'hidden', paddingBottom: '10%' }]}>
			<View style={[myStyles.Center, { paddingTop: 5 }]}>
				<View style={[myStyles.Center, {
					width: '100%',
					paddingHorizontal: 10
				}]}>
					<Text 
						fontSize={22}
						color={colors.primary}
						text={jobDetail.detail.job_specialization.name ? jobDetail.detail.job_specialization.name : ''}
						bold
						align="center"
					/>
					<Text 
						fontSize={16}
						color={colors.primary}
						text={jobDetail.detail.company.company_name ? jobDetail.detail.company.company_name : ''}
					/>
				</View>

				<View style={{
					flexDirection: 'row',
					marginVertical: 8,
					paddingHorizontal: 10
				}}>
					<Text 
						fontSize={13}
						color={colors.primary}
						text={jobDetail.detail.job_specialization.category ? jobDetail.detail.job_specialization.category.name : ''}
					/>
					<MdIcon name='circle-medium' size={18} color={colors.primary} style={{marginHorizontal: 3}} />
					<Text 
						fontSize={13}
						color={colors.primary}
						text={
							`${jobDetail.detail.city[0].name}, ${jobDetail.detail.city[0].province.country.name}`
						}
					/>
				</View>
			</View>

			<View style={styles.Section}>
				<View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
					<Text 
						fontSize={16}
						color={colors.darkScale}
						text="Employment type"
						bold
						bottom
					/>
					<Text 
						fontSize={14}
						color={colors.darkScale}
						text={jobDetail.detail.employment_type.name ? jobDetail.detail.employment_type.name : ''}
						light
					/>
				</View>

				<View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
					<Text 
						fontSize={16}
						color={colors.darkScale}
						text="Job Category"
						bold
						bottom
					/>
					<Text 
						fontSize={14}
						color={colors.darkScale}
						text={jobDetail.detail.job_specialization.category ? jobDetail.detail.job_specialization.category.name : ''}
						light
					/>
				</View>
			</View>

			<View style={styles.Section}>
				<View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
					<Text 
						fontSize={16}
						color={colors.darkScale}
						text="Seniority Level"
						bold
						bottom
					/>
					<Text 
						fontSize={14}
						color={colors.darkScale}
						text='Junior'
						light
					/>
				</View>

				<View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
					<Text 
						fontSize={16}
						color={colors.darkScale}
						text="Salary range"
						bold
						bottom
					/>
					<Text 
						fontSize={14}
						color={colors.darkScale}
						text={
							`Rp ${formatRupiah(jobDetail.detail.expect_salary_from)} - Rp. ${formatRupiah(jobDetail.detail.expect_salary_to)}`
						}
						light
					/>
				</View>
			</View>

			<View style={styles.Section}>
				<View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
					<Text 
						fontSize={16}
						color={colors.darkScale}
						text={`Minimum years \nexperience`}
						bold
						bottom
					/>
					<Text 
						fontSize={14}
						color={colors.darkScale}
						text={
							`${jobDetail.requirement.year_exp} Years`
						}
						light
					/>
				</View>

				<View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
					<Text 
						fontSize={16}
						color={colors.darkScale}
						text={`Minimum education \nlevel`}
						bold
						bottom
					/>
					<Text 
						fontSize={14}
						color={colors.darkScale}
						text={jobDetail.requirement.education_level.degree ? jobDetail.requirement.education_level.degree : ''}
						light
					/>
				</View>
			</View>

			<View style={styles.Section}>
				<View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
					<Text 
						fontSize={16}
						color={colors.darkScale}
						text="Minimum GPA"
						bold
						bottom
					/>
					<Text 
						fontSize={14}
						color={colors.darkScale}
						text={jobDetail.requirement.gpa ? jobDetail.requirement.gpa : ''}
						light
					/>
				</View>

				<View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
					<Text 
						fontSize={16}
						color={colors.darkScale}
						text="Major"
						bold
						bottom
					/>
					{
						jobDetail.requirement.major.length > 0 &&
						jobDetail.requirement.major.map(item => (
							<Text 
								key={item.id}
								fontSize={14}
								color={colors.darkScale}
								text={`${item.major}`}
								light
							/>
						))
					}
				</View>
			</View>

			<View style={styles.Section}>
				<View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
					<Text 
						fontSize={16}
						color={colors.darkScale}
						text="Personality"
						bold
						bottom
					/>
					{
						jobDetail.requirement.personality.length > 0 &&
						jobDetail.requirement.personality.map(item => (
							<Text 
								key={item.id}
								fontSize={14}
								color={colors.darkScale}
								text={`- ${item.personality}`}
								light
							/>
						))
					}
				</View>

				<View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
					<Text 
						fontSize={16}
						color={colors.darkScale}
						text="Skill"
						bold
						bottom
					/>
					{
						jobDetail.requirement.skill.length > 0 &&
						jobDetail.requirement.skill.map(item => (
							<Text 
								key={item.id}
								fontSize={14}
								color={colors.darkScale}
								text={`- ${item.skill}`}
								light
							/>
						))
					}
				</View>
			</View>

			<View style={styles.Section}>
				<View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
					<Text 
						fontSize={16}
						color={colors.darkScale}
						text="Language"
						bold
						bottom
					/>
					{
						jobDetail.requirement.language.length > 0 &&
						jobDetail.requirement.language.map(item => (
							<Text 
								key={item.id}
								fontSize={14}
								color={colors.darkScale}
								text={`- ${item.language}`}
								light
							/>
						))
					}
				</View>

				<View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
					<Text 
						fontSize={16}
						color={colors.darkScale}
						text="Country"
						bold
						bottom
					/>
					<Text 
						fontSize={14}
						color={colors.darkScale}
						text={jobDetail.detail.city[0].province.country.name ? jobDetail.detail.city[0].province.country.name : ''}
						light
					/>
				</View>
			</View>

			<View style={styles.Section}>
				<View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
					<Text 
						fontSize={16}
						color={colors.darkScale}
						text="Province"
						bold
						bottom
					/>
					<Text 
						fontSize={14}
						color={colors.darkScale}
						text={jobDetail.detail.city[0].province.name ? jobDetail.detail.city[0].province.name : ''}
						light
					/>
				</View>

				<View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
					<Text 
						fontSize={16}
						color={colors.darkScale}
						text="City"
						bold
						bottom
					/>
					<Text 
						fontSize={14}
						color={colors.darkScale}
						text={jobDetail.detail.city[0].name ? jobDetail.detail.city[0].name : ''}
						light
					/>
				</View>
			</View>

			<View style={styles.Section}>
				<View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
					<Text 
						fontSize={16}
						color={colors.darkScale}
						text="Job Description"
						bold
						bottom
					/>
					<WebView
						source={{ html	: jobDetail.requirement.job_desc ? `<div style="font-size: 40px !important; color: #919191 !important;font-weight: 100 !important; height: 200 !important;">${jobDetail.requirement.job_desc}</div>` : ''}}
						containerStyle={{ paddingVertical: 5, minHeight: 150 }}
					/>
				</View>
			</View>

			<View style={[ myStyles.Center, { position: 'absolute', bottom: 0, backgroundColor: colors.white, width: '100%', minHeight: 25 }]}>
				{
					viewMore ?
					<TouchableOpacity style={[myStyles.Center, { paddingHorizontal: 15, paddingVertical: 5 }]} activeOpacity={.6} onPress={() => showViewMore(!viewMore)}>
						<MdIcon name="chevron-up" size={25} color={colors.primary} style={{ marginVertical: -5 }} />
					</TouchableOpacity>
					:
					<TouchableOpacity style={[myStyles.Center, { paddingHorizontal: 15, paddingVertical: 5 }]} activeOpacity={.6} onPress={() => showViewMore(!viewMore)}>
						<Text 
							text="View more"
							fontSize={14}
							color={colors.primary}
						/>
						<MdIcon name="chevron-down" size={25} color={colors.primary} style={{ marginVertical: -5 }} />
					</TouchableOpacity>
				}
			</View>
		</View>
	)
}

// mapStateToProps
const mapStateToProps = (state) => {
	return {
		jobDetail: state.job.jobDetail
	}
}
export default connect(mapStateToProps)(LayoutDetail);

const styles = StyleSheet.create({
	Container: {
		width: '100%',
		paddingTop: 10,
	},
	Section: {
		minHeight: 40,
		marginTop: 15,
		paddingLeft: 20,
		paddingRight: 10,
		flexDirection: 'row',
		justifyContent: 'flex-start'
	}
})
