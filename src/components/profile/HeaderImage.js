/*
  Component Header & Image Background
  Passing Props to Use this component
  Created : 23 October 2019       Author : Risman Abdilah
  
  Props : 
  - imageURI    ->  String
										PATH to Image
  - position   	->  String
										Value for Job Position
	-	company			->	String
										Default Value CareerSupport Indonesia
  - type 				->  String
                  	Value for Job Type
  - location    ->  String
                  	Value for Job Location

    Like Your Coding, Like Your Better Future
 */

import React from 'react'
import { View, Image, Text, TouchableOpacity, StyleSheet } from 'react-native'
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import colors from '../style/Colors'
import myStyles from '../style'

const HeaderImage = (props) => {
	return (
		<View style={styles.container}>
			<Image
				source={{
					uri: props.imageURI ? props.imageURI : 'https://4.imimg.com/data4/KS/NW/MY-2/about_us-500x500.jpg'
				}}
				style={styles.Image}
			/>
			<Text style={[styles.headerText, myStyles.FontBold, { fontSize: 20, fontWeight: 'bold', top: props.verified ? '6%': '20%', paddingVertical: 13, paddingHorizontal: 33 }]}>
				{props.fullName ? props.fullName : '[Name hidden]'}
			</Text>
			<Text style={[styles.headerText, myStyles.FontBold, { fontSize: 15, fontWeight: '800', top: props.verified ? '20%' : '35%', color: colors.white, paddingVertical: 10, paddingHorizontal: 20 }]}>
				{props.education ? props.education : 'Education'}
			</Text>
			{
				props.verified &&
				<View style={{ height: 20, position: 'absolute', width: 80, top: '40%', alignSelf: 'center' }}>
					<View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', borderRadius: 50, paddingHorizontal: 10, flexDirection: 'row' }}>
						<MdIcon name="check" size={18} style={{ fontWeight: 'bold' }} color={colors.pink} />
						<Text style={[myStyles.FontBold, { fontWeight: 'bold', color: colors.pink, marginLeft: 2 }]}>Verified</Text>
					</View>
				</View>
			}
			<Text style={[styles.headerText, myStyles.FontMedium, { fontSize: 16.5, top: props.verified ? '56%' : '50%', color: colors.white, paddingHorizontal: 20 }]}>
				{props.major ? props.major : 'Job Type'}
			</Text>

		</View>
	)
}

export default HeaderImage

const styles = StyleSheet.create({
	container: {
		height: 200,
		backgroundColor: 'rgba(0,0,0,0.8)',
		position: 'relative',
		borderBottomWidth: 1,
		borderBottomColor: colors.gray
	},
	Image: {
		flex: 1,
		width: undefined,
		height: undefined,
		resizeMode: 'cover',
		opacity: .5,
		backgroundColor: 'rgba(0,0,0,2)'
	},
	headerText: {
		color: colors.white,
		position: 'absolute',
		width: '100%',
		textAlign: 'center',
		paddingVertical: 4,
		textShadowColor: '#0c0b0a',
		textShadowOffset: {
			width: 1,
			height: 0
		},
		textShadowRadius: 1.55,
	},
	ButtonReport: {
		position: 'absolute',
		right: 10,
		top: 10,
		borderRadius: 100 / 2,
		padding: 6
	},
	Icon: {
		position: 'absolute',
		right: 5,
		top: '10%',
		fontWeight: 'bold'
	}
})