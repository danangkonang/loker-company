import React from 'react'
import { TextInput } from 'react-native'
import colors from '../style/Colors'
import styles from '../style'

const DefaultText = (props) => {
  const {
    placeholder,
    width,
    center,
    password,
    marginTop,
    marginBottom,
    marginLeft,
    marginRight,
    marginVertical,
    marginHorizontal,
    paddingVertical,
    paddingHorizontal,
    borderRadius,
    borderColor,
    borderWidth,
    shadowRadius,
    type,
    keyboardType,
    onchange,
    value,
    defaultValue,
    isError,
    zIndex,
    blur,
    noElevation,
    multiline,
    maxLength,
    backgroundColor,
    disabled,
    noCapitalize,
    borderRadiusEnd,
    borderRadiusStart
  } = props
  return (
    <TextInput
      placeholder={placeholder ? placeholder : 'Career Support'}
      style={
        [{
          width: width ? width : '50%',
          textAlign: center ? 'center' : 'left',
          marginBottom: marginBottom ? marginBottom : 5,
          marginTop: marginTop ? marginTop : 5,
          marginLeft: marginLeft ? marginLeft : 0,
          marginRight: marginRight ? marginRight : 0,
          marginVertical: marginVertical ? marginVertical : 0,
          marginHorizontal: marginHorizontal ? marginHorizontal : 0,
          paddingVertical: paddingVertical ? paddingVertical : 10,
          paddingHorizontal: paddingHorizontal ? paddingHorizontal : 20,
          borderRadius: borderRadius ? borderRadius : 1,
          borderWidth: borderWidth ? borderWidth : 1,
          borderColor: borderColor ? borderColor : colors.gray,
          shadowRadius: shadowRadius ? shadowRadius : 3,
          elevation: noElevation ? 0 : 2.5,
          borderBottomLeftRadius: isError ? 1 : borderRadius,
          borderBottomRightRadius: isError ? 1 : borderRadius,
          borderBottomEndRadius: isError ? 1 : borderRadiusEnd,
          borderBottomStartRadius: isError ? 1 : borderRadiusStart,
          zIndex: zIndex ? zIndex : 0,
          color: isError ? colors.red : colors.darkScale,
          backgroundColor: backgroundColor ? backgroundColor : colors.white,
          shadowColor: '#545454', 
					shadowOffset: { width: 1, height: 1 }, 
					shadowOpacity: .5,
        }, styles.FontMedium]
      }
      value={value ? value : null}
      secureTextEntry={password ? true : false}
      textContentType={type ? type : 'none'}
      placeholderTextColor={colors.grayScale}
      keyboardType={keyboardType ? keyboardType : null}
      onBlur={blur ? blur : null}
      multiline={multiline ? multiline : false}
      maxLength={maxLength ? maxLength : null}
      editable={disabled ? false : true}
      defaultValue={defaultValue}
      autoCapitalize={noCapitalize ? "none" : "sentences"}
    />
  )
}

export default DefaultText;
