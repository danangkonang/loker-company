import React from 'react'
import { View, TouchableOpacity, StyleSheet } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'
// Components
import colors from '../../components/style/Colors'
import Text from '../../components/Text'
import myStyles from '../../components/style'
import { formatDate } from '../../components/constants/format'

// FUNCTIONAL COMPONENTS
const LayoutJobActive = (props) => {
	// Define variables props
	const { title, startDate, endDate, status, total_views, total_applicants, onViewMore, onEdit } = props

	return (
		<View style={styles.Container}>

			<TouchableOpacity 
				onPress={onViewMore}
				activeOpacity={.6}
				style={styles.SectionLeft}
			>
				<Text 
					text={title ? title : ''}
					color={colors.darkSecond}
					fontSize={15}
					bold
					numberOfLines={2}
				/>

				<View style={{ paddingVertical: 5, marginTop: 10}}>
					<Text 
						text="Period"
						color={colors.darkScale}
						fontSize={14}
						bold
					/>
					<Text 
						text={`${formatDate(startDate)} ${endDate ? `- ${formatDate(endDate)}` : ''}`}
						color={colors.darkScale}
						fontSize={13}
						light
					/>
				</View>

				<View style={{ paddingVertical: 5, marginTop: 10}}>
					<Text 
						text="Status"
						color={colors.darkScale}
						fontSize={14}
						bold
					/>
					<Text 
						text={status}
						color={colors.darkScale}
						fontSize={13}
						light
					/>
				</View>
			</TouchableOpacity>

			<View style={styles.SectionRight}>
				<LinearGradient 
					start={{ x: 0, y: .5 }}
					colors={[colors.redSecond, colors.red]}
					style={styles.WidgetTop}
				>
					<View style={styles.Applicants}>
						<Text
							text={`${total_applicants}`}
							fontSize={15}
							color={colors.white}
							bold
						/>
						<Text
							text="  Applicants"
							fontSize={10}
							color={colors.white}
						/>
					</View>
					<View style={styles.Views}>
						<Text
							text={`${total_views}`}
							fontSize={15}
							color={colors.white}
							bold
						/>
						<Text
							text="  Views"
							fontSize={10}
							color={colors.white}
						/>
					</View>
				</LinearGradient>

				<LinearGradient 
					start={{ x: 0, y: 1 }}
					colors={['#4782c3', '#155396']}
					style={[myStyles.Flex1, myStyles.Center]}
				>
					<TouchableOpacity onPress={onEdit}>
						<MdIcon name="pencil" size={18} color={colors.white} />
					</TouchableOpacity>
				</LinearGradient>

				<TouchableOpacity activeOpacity={.6} style={styles.More} onPress={onViewMore}>
					<MdIcon name="chevron-right" size={25} color={colors.primary} />
				</TouchableOpacity>
			</View>

		</View>
	)
}

// Exporting Component
export default LayoutJobActive;

/**
 * @StyleSheet
 */
const styles = StyleSheet.create({
	Container: {
		flexDirection: 'row',
		marginBottom: 20,
		maxHeight: 180
	},
	SectionLeft: {
		flex: 1,
		borderWidth: 1.5,
		borderTopLeftRadius: 10,
		borderBottomLeftRadius: 10,
		borderColor: colors.gray,
		paddingLeft: 15,
		paddingRight: 10,
		paddingVertical: 10,
		justifyContent: 'space-between'
	},
	SectionRight: {
		maxWidth: '30%',
		backgroundColor: colors.cyan,
		borderTopRightRadius: 10,
		borderBottomRightRadius: 10,
	},
	WidgetTop: {
		height: '50%',
		borderTopRightRadius: 10,
		paddingHorizontal: 6
	},
	Applicants: { 
		flex: 1, 
		flexDirection: 'row', 
		borderBottomWidth: 1, 
		borderBottomColor: colors.whiteScale, 
		alignItems: 'center', 
		paddingHorizontal: 3,
	},
	Views: { 
		flex: 1, 
		flexDirection: 'row', 
		alignItems: 'center', 
		paddingHorizontal: 3,
	},
	More: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: colors.cyan,
		borderBottomRightRadius: 10
	}
})