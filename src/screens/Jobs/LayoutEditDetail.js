import React, { useState } from 'react'
import { View, TextInput, TouchableOpacity, StyleSheet, Platform, ScrollView } from 'react-native'
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import { connect } from 'react-redux'
import DateTimePicker from '@react-native-community/datetimepicker'
// Components 
import Text from '../../components/Text'
import colors from '../../components/style/Colors'
import Button from '../../components/button'
import myStyles from '../../components/style'
import { formatDate, setDate } from '../../components/constants/format'
import { getDetailJob, updateDetailJob } from '../../_redux/_actions/jobs'
import ModalLoading from '../../components/loading/ModalLoading'

// FUNCTIONAL COMPONENT
const LayoutDetailEdit = (props) => {

	// State
	const [state, setState] = useState({
		jobCategoryId: null,
		jobCategoryName: null,
		showCategory: false,
		employmentTypeId: null,
		employmentTypeName: null,
		showEmployment: false,
		jobPositionId: null,
		jobPositionName: null,
		showJobPosition: false,
		jobTitleId: null,
		jobTitleName: null,
		showJobTitle: false,
		showStartDate: false,
		dateStart: new Date(),
		startDate: null,
		startDateError: false,
		showEndDate: false,
		dateEnd: new Date(),
		endDate: null,
		endDateError: false,
		expectSalaryFrom: null,
		expectSalaryTo: null,
		countryId: null,
		countryName: null,
		showCountry: false,
		provinceId: null,
		provinceName: null,
		showProvince: false,
		cityId: null,
		cityName: null,
		showCity: false
	})

	// Loading 
	const [isLoading, setLoading] = useState(false)

	// Define Variable props
	const { jobDetail, ListCategory, ListEmployment, ListPosition, JobSpecialization, ListCountry, province, city } = props.job
	
	return (
		<View style={[myStyles.Flex1, { paddingHorizontal: 20, paddingVertical: 15 }]}>
			<View style={{ marginVertical: 5 }}>
				<Text 
					text="Job category"
					color={colors.darkSecond}
					fontSize={16}
					bold
				/>
				<View style={[styles.TextInput, { flexDirection: 'row', height: 50 }]}>
					<TextInput
						placeholder={
							state.jobCategoryName === null ? 
								jobDetail.detail.job_specialization.category.name ?
									jobDetail.detail.job_specialization.category.name
									:
									"Job category"
								:
								state.jobCategoryName
						}
						placeholderTextColor={colors.grayScale}
						style={[myStyles.Flex1, styles.TextInputStyle]}
						onChangeText={(value) => setState({
							...state,
							jobCategoryName: value,
							showCategory: true
						})}
						value={
							state.jobCategoryName === null ? 
								jobDetail.detail.job_specialization.category.name
								:
								state.jobCategoryName
						}
					/>
					<View style={[myStyles.Center, myStyles.bgWhite, { width: 30 }]}>
						<MdIcon name="chevron-down" size={18} color={colors.secondary} />
					</View>
				</View>
			</View>
			
			{
				state.showCategory &&
				<ScrollView 
					style={{
						position: 'absolute',
						width: '100%',
						maxHeight: 155,
						overflow: 'hidden',
						top: '7.8%',
						zIndex: 1,
						alignSelf: 'center',
						backgroundColor: colors.white,
						borderColor: colors.gray,
						borderLeftWidth: 2,
						borderBottomWidth: 2,
						borderRightWidth: 2,
						borderBottomLeftRadius: 10,
						borderBottomRightRadius: 10,
					}}
				>
					{
						ListCategory.map((item, index) => {
							return (
								<TouchableOpacity 
									onPress={() => setState({
										...state,
										jobCategoryId: item.id,
										jobCategoryName: item.name,
										showCategory: false
									})}
									key={index}
									style={{
										width: '100%',
										paddingVertical: 10,
										paddingHorizontal: 10,
										backgroundColor: colors.cyan,
										borderTopColor: colors.white,
										borderTopWidth: 2,
									}}
								>
									<Text 
										text={item.name}
										fontSize={14}
										color={colors.primary}
										medium
										left={5}
									/>
								</TouchableOpacity>
							)
						})
					}
				</ScrollView>
			}

			<View style={{ marginVertical: 5}}>
				<Text 
					text="Employment type"
					color={colors.darkSecond}
					fontSize={16}
					bold
				/>
				<View style={[styles.TextInput, { flexDirection: 'row', height: 50 }]}>
					<TextInput
						placeholder={
							state.employmentTypeName === null ? 
								jobDetail.detail.employment_type.name ?
									jobDetail.detail.employment_type.name
									:
									"Employment type"
								:
								state.employmentTypeName
						}
						placeholderTextColor={colors.grayScale}
						style={[myStyles.Flex1, styles.TextInputStyle]}
						onChangeText={(value) => setState({
							...state,
							employmentTypeName: value,
							showEmployment: true
						})}
						value={
							state.employmentTypeName === null ? 
								jobDetail.detail.employment_type.name
								:
								state.employmentTypeName
						}
					/>
					<View style={[myStyles.Center, myStyles.bgWhite, { width: 30 }]}>
						<MdIcon name="chevron-down" size={18} color={colors.secondary} />
					</View>
				</View>
			</View>

			{
				state.showEmployment &&
				<ScrollView 
					style={{
						position: 'absolute',
						width: '100%',
						maxHeight: 155,
						overflow: 'hidden',
						top: '16.2%',
						zIndex: 1,
						alignSelf: 'center',
						backgroundColor: colors.white,
						borderColor: colors.gray,
						borderLeftWidth: 2,
						borderBottomWidth: 2,
						borderRightWidth: 2,
						borderBottomLeftRadius: 10,
						borderBottomRightRadius: 10,
					}}
				>
					{
						ListEmployment.map((item, index) => {
							return (
								<TouchableOpacity 
									onPress={() => setState({
										...state,
										employmentTypeId: item.id,
										employmentTypeName: item.name,
										showEmployment: false
									})}
									key={index}
									style={{
										width: '100%',
										paddingVertical: 10,
										paddingHorizontal: 10,
										backgroundColor: colors.cyan,
										borderTopColor: colors.white,
										borderTopWidth: 2,
									}}
								>
									<Text 
										text={item.name}
										fontSize={14}
										color={colors.primary}
										medium
										left={5}
									/>
								</TouchableOpacity>
							)
						})
					}
				</ScrollView>
			}

			<View style={{ marginVertical: 5}}>
				<Text 
					text="Job position"
					color={colors.darkSecond}
					fontSize={16}
					bold
				/>
				<View style={[styles.TextInput, { flexDirection: 'row', height: 50 }]}>
					<TextInput
						placeholder={
							state.jobPositionName === null ? 
								jobDetail.detail.position.name ?
									jobDetail.detail.position.name
									:
									"Job Position"
								:
								state.jobPositionName
						}
						placeholderTextColor={colors.grayScale}
						style={[myStyles.Flex1, styles.TextInputStyle]}
						onChangeText={(value) => setState({
							...state,
							jobPositionName: value,
							showJobPosition: true
						})}
						value={
							state.jobPositionName === null ? 
								jobDetail.detail.position.name
								:
								state.jobPositionName
						}
					/>
					<View style={[myStyles.Center, myStyles.bgWhite, { width: 30 }]}>
						<MdIcon name="chevron-down" size={18} color={colors.secondary} />
					</View>
				</View>
			</View>

			{
				state.showJobPosition &&
				<ScrollView 
					style={{
						position: 'absolute',
						width: '100%',
						maxHeight: 155,
						overflow: 'hidden',
						top: '24.9%',
						zIndex: 1,
						alignSelf: 'center',
						backgroundColor: colors.white,
						borderColor: colors.gray,
						borderLeftWidth: 2,
						borderBottomWidth: 2,
						borderRightWidth: 2,
						borderBottomLeftRadius: 10,
						borderBottomRightRadius: 10,
					}}
				>
					{
						ListPosition.map((item, index) => {
							return (
								<TouchableOpacity 
									onPress={() => setState({
										...state,
										jobPositionId: item.id,
										jobPositionName: item.name,
										showJobPosition: false
									})}
									key={index}
									style={{
										width: '100%',
										paddingVertical: 10,
										paddingHorizontal: 10,
										backgroundColor: colors.cyan,
										borderTopColor: colors.white,
										borderTopWidth: 2,
									}}
								>
									<Text 
										text={item.name}
										fontSize={14}
										color={colors.primary}
										medium
										left={5}
									/>
								</TouchableOpacity>
							)
						})
					}
				</ScrollView>
			}

			<View style={{ marginVertical: 5}}>
				<Text 
					text="Job title"
					color={colors.darkSecond}
					fontSize={16}
					bold
				/>
				<View style={[styles.TextInput, { height: 50 }]}>
					<TextInput
						placeholder={
							state.jobTitleName === null ? 
								jobDetail.detail.job_specialization.name ?
									jobDetail.detail.job_specialization.name
									:
									"Job Title"
								:
								state.jobTitleName
						}
						placeholderTextColor={colors.grayScale}
						style={[myStyles.Flex1, styles.TextInputStyle]}
						onChangeText={(value) => setState({
							...state,
							jobTitleName: value,
							showJobTitle: true
						})}
						value={
							state.jobTitleName === null ? 
								jobDetail.detail.job_specialization.name
								:
								state.jobTitleName
						}
					/>
				</View>
			</View>

			{
				state.showJobTitle &&
				<ScrollView 
					style={{
						position: 'absolute',
						width: '100%',
						maxHeight: 155,
						overflow: 'hidden',
						top: '33.5%',
						zIndex: 1,
						alignSelf: 'center',
						backgroundColor: colors.white,
						borderColor: colors.gray,
						borderLeftWidth: 2,
						borderBottomWidth: 2,
						borderRightWidth: 2,
						borderBottomLeftRadius: 10,
						borderBottomRightRadius: 10,
					}}
				>
					{
						JobSpecialization.map((item, index) => {
							return (
								<TouchableOpacity 
									onPress={() => setState({
										...state,
										jobTitleId: item.id,
										jobTitleName: item.name,
										showJobTitle: false
									})}
									key={index}
									style={{
										width: '100%',
										paddingVertical: 10,
										paddingHorizontal: 10,
										backgroundColor: colors.cyan,
										borderTopColor: colors.white,
										borderTopWidth: 2,
									}}
								>
									<Text 
										text={item.name}
										fontSize={14}
										color={colors.primary}
										medium
										left={5}
									/>
								</TouchableOpacity>
							)
						})
					}
				</ScrollView>
			}

			<View style={{ marginVertical: 5}}>
				<Text 
					text="Period"
					color={colors.darkSecond}
					fontSize={16}
					bold
				/>
				<View style={styles.OptionStyle}>
					<TouchableOpacity 
						onPress={() => setState({
							...state,
							showStartDate: !state.showStartDate,
							showEndDate: false
						})} 
						style={[styles.OptionValue, { marginRight: 15 }]}
					>
						<Text 
							text={
								state.startDate === null ? 
								jobDetail.detail.period_from ?
									formatDate(jobDetail.detail.period_from)
									:
									"YYYY-MM-DD"
								:
								formatDate(state.startDate)
							}
							color={colors.grayScale}
							fontSize={14}
						/>
					</TouchableOpacity>
					<Text 
						text="-"
						color={colors.darkScale}
						fontSize={18}
						light
					/>
					<TouchableOpacity 
						onPress={() => setState({
							...state,
							showEndDate: !state.showEndDate,
							showStartDate: false
						})}
						style={[styles.OptionValue, { marginLeft: 15 }]}
					>
						<Text 
							text={
								state.endDate === null ? 
								jobDetail.detail.period_to ?
									formatDate(jobDetail.detail.period_to)
									:
									"YYYY-MM-DD"
								:
								formatDate(state.endDate)
							}
							color={colors.grayScale}
							fontSize={14}
						/>
					</TouchableOpacity>
				</View>
				{
					state.showStartDate &&
					<DateTimePicker 
						value={state.dateStart}
						mode='date'
						is24Hour={true}
						display="default"
						onChange={(event, date) => setStartDate(event, date, state, setState, props)} 
					/>
				}
				{
					state.showEndDate &&
					<DateTimePicker 
						value={state.dateEnd}
						mode='date'
						is24Hour={true}
						display="default"
						onChange={(event, date) => setEndDate(event, date, state, setState, props)} 
					/>
				}
			</View>

			<View style={{ marginVertical: 5}}>
				<Text 
					text="Monthly salary"
					color={colors.darkSecond}
					fontSize={16}
					bold
				/>
				<View style={styles.OptionStyle}>
					<Text 
						text="Rp "
						color={colors.grayScale}
						fontSize={14}
					/>
					<TextInput
						placeholder="6.000.000"
						placeholderTextColor={colors.grayScale}
						style={[myStyles.Flex1, styles.OptionValue, { marginRight: 15, alignItems: 'flex-start' }]}
						onChangeText={(value) => {
							setState({
								...state,
								expectSalaryFrom: value
							})
						}}
						keyboardType="numeric"
						value={state.expectSalaryFrom === null ? jobDetail.detail.expect_salary_from.replace('.00', ''): state.expectSalaryFrom}
					/>
					<Text 
						text="-"
						color={colors.darkScale}
						fontSize={18}
						light
					/>
					<Text 
						text="Rp "
						color={colors.grayScale}
						fontSize={14}
						left={15}
					/>
					<TextInput
						placeholder="16.000.000"
						placeholderTextColor={colors.grayScale}
						style={[myStyles.Flex1, styles.OptionValue, { alignItems: 'flex-start' }]}
						onChangeText={(value) => {
							setState({
								...state,
								expectSalaryTo: value
							})
						}}
						keyboardType="numeric"
						value={state.expectSalaryTo === null ? jobDetail.detail.expect_salary_to.replace('.00', ''): state.expectSalaryTo}
					/>
				</View>
			</View>

			<View style={{ marginTop: 5, marginBottom: 15 }}>
				<Text 
					text="Work location"
					color={colors.primary}
					fontSize={16}
				/>
			</View>

			<View style={{ marginVertical: 5}}>
				<Text 
					text="Country"
					color={colors.darkSecond}
					fontSize={16}
					bold
				/>
				<View style={[styles.TextInput, { flexDirection: 'row', height: 50 }]}>
					<TextInput
						placeholder={
							state.countryName === null ? 
								jobDetail.detail.city[0].province.country.name ?
									jobDetail.detail.city[0].province.country.name
									:
									"Country"
								:
								state.countryName
						}
						placeholderTextColor={colors.grayScale}
						style={[myStyles.Flex1, styles.TextInputStyle]}
						onChangeText={(value) => {
							setState({
								...state,
								countryName: value,
								showCountry: true
							})
						}}
						value={state.countryName === null ? jobDetail.detail.city[0].province.country.name : state.countryName}
					/>
					<View style={[myStyles.Center, myStyles.bgWhite, { width: 30 }]}>
						<MdIcon name="chevron-down" size={18} color={colors.secondary} />
					</View>
				</View>
			</View>

			{
				state.showCountry &&
				<ScrollView 
					style={{
						position: 'absolute',
						width: '100%',
						maxHeight: 155,
						overflow: 'hidden',
						top: '78.2%',
						zIndex: 1,
						alignSelf: 'center',
						backgroundColor: colors.white,
						borderColor: colors.gray,
						borderLeftWidth: 2,
						borderBottomWidth: 2,
						borderRightWidth: 2,
						borderBottomLeftRadius: 10,
						borderBottomRightRadius: 10,
					}}
				>
					{
						ListCountry.map((item, index) => {
							return (
								<TouchableOpacity 
									onPress={() => setState({
										...state,
										countryId: item.id,
										countryName: item.name,
										showCountry: false
									})}
									key={index}
									style={{
										width: '100%',
										paddingVertical: 10,
										paddingHorizontal: 10,
										backgroundColor: colors.cyan,
										borderTopColor: colors.white,
										borderTopWidth: 2,
									}}
								>
									<Text 
										text={item.name}
										fontSize={14}
										color={colors.primary}
										medium
										left={5}
									/>
								</TouchableOpacity>
							)
						})
					}
				</ScrollView>
			}

			<View style={{ marginVertical: 5}}>
				<Text 
					text="Province"
					color={colors.darkSecond}
					fontSize={16}
					bold
				/>
				<View style={[styles.TextInput, { flexDirection: 'row', height: 50 }]}>
					<TextInput
						placeholder={
							state.provinceName === null ? 
								jobDetail.detail.city[0].province.name ?
									jobDetail.detail.city[0].province.name
									:
									"Province"
								:
								state.provinceName
						}
						placeholderTextColor={colors.grayScale}
						style={[myStyles.Flex1, styles.TextInputStyle]}
						onChangeText={(value) => {
							setState({
								...state,
								provinceName: value,
								showProvince: true
							})
						}}
						value={state.provinceName === null ? jobDetail.detail.city[0].province.name : state.provinceName}
					/>
					<View style={[myStyles.Center, myStyles.bgWhite, { width: 30 }]}>
						<MdIcon name="chevron-down" size={18} color={colors.secondary} />
					</View>
				</View>
			</View>

			{
				state.showProvince &&
				<ScrollView 
					style={{
						position: 'absolute',
						width: '100%',
						maxHeight: 155,
						overflow: 'hidden',
						top: '86.6%',
						zIndex: 1,
						alignSelf: 'center',
						backgroundColor: colors.white,
						borderColor: colors.gray,
						borderLeftWidth: 2,
						borderBottomWidth: 2,
						borderRightWidth: 2,
						borderBottomLeftRadius: 10,
						borderBottomRightRadius: 10,
					}}
				>
					{
						province.map((item, index) => {
							return (
								<TouchableOpacity 
									onPress={() => setState({
										...state,
										provinceId: item.id,
										provinceName: item.name,
										showProvince: false
									})}
									key={index}
									style={{
										width: '100%',
										paddingVertical: 10,
										paddingHorizontal: 10,
										backgroundColor: colors.cyan,
										borderTopColor: colors.white,
										borderTopWidth: 2,
									}}
								>
									<Text 
										text={item.name}
										fontSize={14}
										color={colors.primary}
										medium
										left={5}
									/>
								</TouchableOpacity>
							)
						})
					}
				</ScrollView>
			}

			<View style={{ marginVertical: 5 }}>
				<Text 
					text="City"
					color={colors.darkSecond}
					fontSize={16}
					bold
				/>
				<View style={[styles.TextInput, { flexDirection: 'row', height: 50 }]}>
					<TextInput
						placeholder={
							state.cityName === null ? 
								jobDetail.detail.city[0].name ?
									jobDetail.detail.city[0].name
									:
									"City"
								:
								state.cityName
						}
						placeholderTextColor={colors.grayScale}
						style={[myStyles.Flex1, styles.TextInputStyle]}
						onChangeText={(value) => {
							setState({
								...state,
								cityName: value,
								showCity: true
							})
						}}
						value={state.cityName === null ? jobDetail.detail.city[0].name : state.cityName}
					/>
					<View style={[myStyles.Center, myStyles.bgWhite, { width: 30 }]}>
						<MdIcon name="chevron-down" size={18} color={colors.secondary} />
					</View>
				</View>
			</View>

			{
				state.showCity &&
				<ScrollView 
					style={{
						width: '100%',
						maxHeight: 155,
						overflow: 'hidden',
						zIndex: 1,
						marginTop: -25,
						marginBottom: 10,
						alignSelf: 'center',
						backgroundColor: colors.white,
						borderColor: colors.gray,
						borderLeftWidth: 2,
						borderBottomWidth: 2,
						borderRightWidth: 2,
						borderBottomLeftRadius: 10,
						borderBottomRightRadius: 10,
					}}
				>
					{
						city.map((item, index) => {
							return (
								<TouchableOpacity 
									onPress={() => setState({
										...state,
										cityId: item.id,
										cityName: item.name,
										showCity: false
									})}
									key={index}
									style={{
										width: '100%',
										paddingVertical: 10,
										paddingHorizontal: 10,
										backgroundColor: colors.cyan,
										borderTopColor: colors.white,
										borderTopWidth: 2,
									}}
								>
									<Text 
										text={item.name}
										fontSize={14}
										color={colors.primary}
										medium
										left={5}
									/>
								</TouchableOpacity>
							)
						})
					}
				</ScrollView>
			}

			<View style={[myStyles.Center, { width: '100%', marginTop: 10 }]}>
				<Button 
					width="40%"
					text="Update"
					color={colors.white}
					onclick={() => updateDetail(jobDetail.id, jobDetail.detail, state, setLoading, props)}
				/>
			</View>

			<ModalLoading 
				isVisible={isLoading}
				text="Please wait..."
			/>

		</View>
	)
}

/**
 * @function updateDetail
 * @param is state, setState, props
 */
const updateDetail = (id, detail, state, setLoading, props) => {

	// Play Loading
	setLoading(true)
	// GET TOKEN
	const { token } = props.employer.employer_data
	// SET DATA
	var data = JSON.stringify({
		job_category: detail.job_specialization.category.id,
		company: {
			...detail.company
		},
		employment_type: state.employmentTypeId === null ? detail.employment_type.id : state.employmentTypeId,
		job_specialization: state.jobTitleId === null ? detail.job_specialization.id : state.jobTitleId,
		position: state.jobPositionId === null ? detail.position.id : state.jobPositionId,
		city: state.cityId === null ? [detail.city[0].id] : [state.cityId],
		period_from: state.startDate === null ? setDate(detail.period_from) : state.startDate,
		period_to: state.endDate === null ? setDate(detail.period_to) : state.endDate,
		expect_salary_from: state.expectSalaryFrom === null ? parseInt(detail.expect_salary_from) : parseInt(state.expectSalaryFrom),
		expect_salary_to: state.expectSalaryTo === null ? parseInt(detail.expect_salary_to) : parseInt(state.expectSalaryTo),
	})

	// console.log(JSON.parse(data))
	// DISPATCH ACTION
	setTimeout(async () => {
		try {
			await props.dispatch(updateDetailJob(id, data, token))
			await props.dispatch(getDetailJob(id, token))
			props.navigation.goBack()
			setLoading(false)
		} catch (error) {
			setLoading(false)
			console.log(error.response)
		}
	}, 2000)
}

/**
 * @function setStartDate
 * @param is event, date & props
 */
const setStartDate = (event, date, state, setState, props) => {
	if (Platform.OS === 'ios') {

		date = date || state.dateStart

		setState({
			...state,
			startDate: setDate(date),
			dateStart: date,
			startDateError: false,
		})
	} 
}

/**
 * @function setEndDate
 * @param is event, date & props
 */
const setEndDate = (event, date, state, setState, props) => {
	if (Platform.OS === 'ios') {
		
		date = date || state.dateStart

		setState({
			...state,
			endDate: setDate(date),
			dateEnd: date,
			endDateError: false
		})
	} 
}

// mapStateToProps
const mapStateToProps = (state) => {
	return {
		employer: state.employer,
		job: state.job,
		candidate: state.candidate
	}
}

// EXPORTING COMPONENT
export default connect(mapStateToProps)(LayoutDetailEdit);

/**
 * @StyleSheet
 */
const styles = StyleSheet.create({
	TextInput: {  
		width: '100%', 
		borderWidth: 2, 
		borderColor: colors.gray, 
		borderRadius: 6, 
		marginVertical: 15,
	},
	TextInputStyle: { 
		paddingLeft: 15,
		paddingRight: 10, 
		color: colors.darkScale, 
		backgroundColor: colors.white,
		fontSize: 14,
		fontFamily: 'DIN-Medium'
	},
	OptionStyle: { 
		flexDirection: 'row', 
		width: '100%', 
		marginVertical: 15, 
		justifyContent: 'flex-start', 
		alignItems: 'center',
	},
	OptionValue: {
		paddingHorizontal: 15,
		borderRadius: 6, 
		borderColor: colors.gray,
		borderWidth: 2,
		height: 50,
		color: colors.darkScale,
		justifyContent: 'center',
		alignItems: 'center',
		fontFamily: 'DIN-Medium'
	}
})