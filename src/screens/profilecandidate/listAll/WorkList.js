import React, { Component } from 'react'
import {
	View,
	Text,
	ScrollView,
	Dimensions,
	SafeAreaView,
	StyleSheet,
} from 'react-native'
import Header from '../../../components/header/index'
import HeaderImage from '../../../components/profile/HeaderImage'
import myStyles from '../../../components/style'
import colors from '../../../components/style/Colors'
import List from './List'
import { connect } from 'react-redux'
import LogoUser from '../../../components/profile/LogoUser'
import { modifyMonth } from '../../../components/utils'
let AndroidHeight = Dimensions.get('window').height
import DetailWorking from '../../profilecandidate/WorkingDetail'
class Education extends Component {

	constructor(props) {
		super(props);
		this.state = {
			isDelete: false,
			scroll: true,
			showModal: false,
			achiveAddModal: false,
			achiveDetailModal: false,
			achiveEditModal: false,
			userToken: '',
			typeDelete: null,
			data: [1, 2, 3, 4, 5, 6],
			modalWorking: false,
			detailWork:{},
		}
	}

	render() {
		return (
			<SafeAreaView style={myStyles.Flex1}>
				<View style={myStyles.Flex1}>
					<Header
						logo={true}
						onpress={() => this.props.navigation.goBack()}
					/>
					<ScrollView
						showsVerticalScrollIndicator={false}
						scrollEnabled={this.state.scroll}
					>
						<HeaderImage
							fullName={
								this.props.candidate.data_candidate.cv_unlocked ? this.props.candidate.data_candidate.user.full_name : "[Name Hidden]"
							}
							major={
								this.props.candidate.data_candidate.last_education.major.label
							}
							education={
								this.props.candidate.data_candidate.last_education.school.label
							}
						/>
						<LogoUser image={this.props.candidate.data_candidate.avatar} />
						<View style={[myStyles.ProfileSection, { minHeight: AndroidHeight - 281 }]}>
							<View style={{ flex: 1, marginTop: 15, marginBottom: 5, borderRadius: 10, backgroundColor: colors.white, }}>
								<View style={styles.Header}>
									<Text style={[styles.Title, myStyles.FontBold]}>
										Working & experience</Text>
								</View>

								<View style={[{ flex: 1, paddingVertical: 10, paddingHorizontal: 20, borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }]}>
									{
										this.props.candidate.workexp_list.map((item, idx) => {
											let started = new Date(item.start_date).getFullYear()
											let ended = new Date(item.end_date).getFullYear()
											return (
												<List
													key={idx}
													click={()=>this.detailWorking(item)}
													Logo={true}
													SourceImage={item.logo}
													Title={item.name}
													TextDesc={item.title}
													Years={`${modifyMonth(item.start_date)} ${started}  -  ${item.end_date ? `${modifyMonth(item.start_date)} ${ended}` : 'Now'}`}
												/>
											)
										})
									}
								</View>
							</View>
						</View>
					</ScrollView>
					<DetailWorking
						data={this.state.detailWork}
						isVisible={this.state.modalWorking}
						onClose={() => this.setState({ modalWorking: false })}
					/>
				</View>
			</SafeAreaView>
		)
	}
	detailWorking =async (data) => {
		await this.setState({detailWork:data})
		this.setState({ modalWorking: true })
		// console.log(this.state.detailWork)
	}
}

const mapStateToProps = state => ({
	candidate: state.candidate
})

export default connect(mapStateToProps, null)(Education);

const styles = StyleSheet.create({
	Header: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		width: '100%',
		paddingVertical: 15,
		paddingHorizontal: 20,
		borderBottomWidth: .7,
		borderBottomColor: colors.gray,
	},
	Title: {
		fontSize: 18,
		color: colors.primary,
		fontWeight: 'bold',
	}
})
