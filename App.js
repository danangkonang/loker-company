import React, { Component } from 'react';
import { Provider } from 'react-redux'
import store from './src/_redux/_store'
import Routes from './src/routes';


export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Routes />
      </Provider>
    )
  }
}