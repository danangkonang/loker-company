import React, { useState } from 'react'
import { View, SafeAreaView, StatusBar, ScrollView, TouchableWithoutFeedback, StyleSheet } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import { connect } from 'react-redux'
// Components
import Text from '../../components/Text'
import colors from '../../components/style/Colors'
import myStyles from '../../components/style'
import AppHeader from '../../components/header'
import Banner from '../../components/Layout/Banner'
import Logo from '../../components/Layout/Logo'
import LayoutEditDetail from './LayoutEditDetail'
import LayoutEditRequirement from './LayoutEditRequirement'

const AppEditJob = (props) => {

	const [keyTab, setKey] = useState(1)
	const { goBack } = props.navigation

	return (
		<LinearGradient
			start={
				{x: 0, y: 0}
			}
			end={
				{x: 0, y: 1}
			}
			colors={['#155396', '#155396', colors.white, colors.white]}
			style={myStyles.Flex1}
		>
			<SafeAreaView style={myStyles.Flex1}>
				<StatusBar barStyle="light-content" />
				<AppHeader
					logo={true}
					onpress={() => goBack()}
				/>
				<View style={[myStyles.Flex1, myStyles.bgWhite]}>
					<ScrollView showsVerticalScrollIndicator={false}>
						<Banner 
							onPress={() => alert('Delete?')}
						/>
						<Logo 
							image=""
						/>

						<LinearGradient 
							colors={[colors.whiteScale, colors.cyan, colors.cyan]}
							style={{ marginTop: -50, paddingTop: 75 }}
						>
							<View style={[myStyles.Center, { paddingVertical: 5 }]}>
								<Text 
									fontSize={22}
									color={colors.primary}
									text='UI / UX Designer'
									bold
								/>
								<Text 
									fontSize={16}
									color={colors.primary}
									text='CareerSupport Indonesia'
								/>

								<View style={{
									flexDirection: 'row',
									marginVertical: 8
								}}>
									<Text 
										fontSize={13}
										color={colors.primary}
										text='Web & Mobile'
									/>
									<MdIcon name='circle-medium' size={18} color={colors.primary} style={{marginHorizontal: 3}} />
									<Text 
										fontSize={13}
										color={colors.primary}
										text='Jakarta, Indonesia'
									/>
								</View>
							</View>

							<View style={[myStyles.Flex1, { marginTop: 20}]}>
								<View style={{ backgroundColor: colors.cyan, flexDirection: 'row' }}>
									<TouchableWithoutFeedback onPress={() => setKey(1)}>
										<View style={[styles.TabButton, {
											backgroundColor: keyTab === 1 ? colors.cyan : colors.white,
										}]}>
											<View style={[styles.TabView, {
												backgroundColor: keyTab === 1 ? colors.white : colors.cyan,
												borderTopRightRadius: keyTab === 1 ? 15 : 0,
												borderBottomRightRadius: keyTab === 1 ? 0 : 15,
											}]}>
												<Text 
													fontSize={16}
													color={colors.primary}
													text="Detail"
													light={keyTab === 1 ? false : true}
												/>
											</View>
										</View>
									</TouchableWithoutFeedback>

									<TouchableWithoutFeedback onPress={() => setKey(2)}>
										<View style={[styles.TabButton, {
											backgroundColor: keyTab === 1 ? colors.white : colors.cyan,
										}]}>
											<View style={[styles.TabView, {
												backgroundColor: keyTab === 1 ? colors.cyan : colors.white,
												borderBottomLeftRadius: keyTab === 1 ? 15 : 0,
												borderTopLeftRadius: keyTab === 1 ? 0 : 15,
											}]}>
												<Text 
													fontSize={16}
													color={colors.primary}
													text="Requirements"
													light={keyTab === 2 ? false : true}
												/>
											</View>
										</View>
									</TouchableWithoutFeedback>
								</View>
							</View>	
						</LinearGradient>

						{
							keyTab === 1 &&
							<LayoutEditDetail
								navigation={props.navigation}
							/>
						}

						{
							keyTab === 2 &&
							<LayoutEditRequirement
								navigation={props.navigation}
							/>
						}

					</ScrollView>
				</View>
			</SafeAreaView>
		</LinearGradient>
	)
}

// mapStateToProps
const mapStateToProps = (state) => {
	return {
		employer: state.employer,
		job: state.job,
		candidate: state.candidate
	}
}

export default connect(mapStateToProps)(AppEditJob);

const styles = StyleSheet.create({
	TabButton: {
		width: '50%',
		alignItems: 'center',
		justifyContent: 'center',
	},
	TabView: {
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		paddingTop: 10,
		paddingBottom: 15,
		overflow: 'hidden',
	},
})