import React, { Component } from 'react'
import {
	View,
	Text,
	ScrollView,
	SafeAreaView,
	StyleSheet,
	TouchableOpacity,
	RefreshControl
} from 'react-native'

import AsyncStorage from "@react-native-community/async-storage";
import { getDataCandidate, } from '../../_redux/_actions/candidate'
import { connect } from 'react-redux'

import IonIcons from 'react-native-vector-icons/Ionicons'
import Header from '../../components/header'
import HeaderImage from '../../components/profile/HeaderImage'
import LogoUser from '../../components/profile/LogoUser'
import SocialMedia from '../../components/navigation/SocialMedia'
import Button from '../../components/button'
import myStyles from '../../components/style'
import colors from '../../components/style/Colors'
import RenderList from '../../components/profile/RenderListMax'
import LinearGradient from 'react-native-linear-gradient'
import DetailEducation from '../profilecandidate/EducationDetail'
import DetailWorking from '../profilecandidate/WorkingDetail'
import DetailOrganization from '../profilecandidate/OrganizationDetail'
import DetailAchievements from '../profilecandidate/AchievementsDetail'
import DetailPortfolio from '../profilecandidate/PortfolioDetail'
import DetailLanguage from '../profilecandidate/LanguageDetail'
import DetailSkill from '../profilecandidate/SkillDetail'
import TextComponent from '../../components/Text'
import DetailPersonal from './DetailPersonal';
import { modifyDateFormat } from '../../components/utils';
class Profile extends Component {
	constructor(props) {
		super(props)
		this._isMounted = false
		this.state = {
			modalEducation: false,
			modalWorking: false,
			modalOrganization: false,
			modalSkill: false,
			modalLanguage: false,
			modalAchievements: false,
			modalPortfolio: false,
			isRefresh: false,
			employerToken: null,
			toogleDetail: false,
			detailEdu: {},
			detailWork: {},
			detailOrg: {},
			detailAchi: {},
			detailFolio: {},
			detailLanguage: {},
		}
	}


	async componentDidMount() {
		const employerToken = await AsyncStorage.getItem('employerToken');
		this.setState({ employerToken })
	}

	getData = async (token) => {
		const { candidateId, jobsId } = await this.props.navigation.state.params
		await this.props.getDataCandidate({ token, jobsId, candidateId })
	}

	render() {

		const { candidate } = this.props
		return (
			<LinearGradient
				start={
					{ x: 0, y: 0 }
				}
				end={
					{ x: 0, y: 1 }
				}
				colors={['#155396', '#155396', colors.white, colors.white]}
				style={{
					flex: 1
				}}
			>
				<SafeAreaView style={myStyles.Flex1}>
					<View style={[myStyles.Flex1, { backgroundColor: colors.white }]}>
						<Header logo={true} icon="chevron-left" iconSize={30} onpress={() => this.props.navigation.goBack()} />
						<ScrollView
							refreshControl={
								<RefreshControl
									refreshing={this.state.isRefresh}
									onRefresh={async () => {
										this.setState({ isRefresh: true })
										await this.getData(this.state.employerToken)
										this.setState({ isRefresh: false })
									}}
									title="Pull up to refresh"
									titleColor="#4782c3"
									tintColor="#4782c3"
									colors={['#4782c3']}
								/>
							}
							style={myStyles.Flex1}>
							<HeaderImage
								fullName={
									candidate.isLoading != true ?
										candidate.data_candidate.cv_unlocked ? candidate.data_candidate.user.full_name : "[Name Hidden]"
										: '-'
								}
								major={
									candidate.data_candidate.last_education ?
										candidate.data_candidate.last_education.major.label
										: '-'
								}
								education={
									candidate.data_candidate.last_education ?
										candidate.data_candidate.last_education.school.label
										: '-'
								}
								verified={
									candidate.data_candidate ?
										candidate.data_candidate.is_verified : false
								}
							/>
							<LogoUser
								image={
									candidate.data_candidate.avatar ?
										candidate.data_candidate.avatar
										: null
								}
							/>
							<View style={[myStyles.Flex1, styles.Section]}>
								<View style={styles.Description}>
									<Text style={[styles.TextDesc, myStyles.FontLight, { textAlign: 'center' }]}>
										{
											candidate.data_candidate.description ?
												candidate.data_candidate.description
												: '-'
										}
									</Text>
								</View>
								{/* :
									'-'
							} */}
								<View style={styles.Contact}>
									{
										!candidate.data_candidate.cv_unlocked ?
											<>
												<View style={styles.ContactItem}>
													<View style={styles.ContactIcon}>
														<IonIcons name="ios-phone-portrait" size={24} color="#FF1493" />
													</View>
													<View style={{ justifyContent: 'center' }}>
														<Text style={[styles.TextDesc, myStyles.FontMedium]}>
															{
																candidate.data_candidate.phone ?
																	candidate.data_candidate.phone
																	: '-'
															}
														</Text>
													</View>
												</View>
												<View style={styles.ContactItem}>
													<View style={styles.ContactIcon}>
														<IonIcons name="ios-mail-open" size={24} color="#FF1493" />
													</View>
													<View style={{ justifyContent: 'center' }}>
														<Text style={[styles.TextDesc, myStyles.FontMedium]}>
															{
																candidate.data_candidate.user ?
																	candidate.data_candidate.user.email
																	: '-'
															}
														</Text>
													</View>
												</View>

												<SocialMedia
													facebook='{profile_header.facebookURL}'
													twitter='{profile_header.twitterURL}'
													linkedin='{profile_header.linkedinURL}'
												/>
											</>
											: null
									}
								</View>

								<TouchableOpacity style={{
									backgroundColor: colors.cyan,
									height: 40,
									flexDirection: 'row',
									justifyContent: 'space-between',
									alignItems: 'center',
									marginHorizontal: 15, borderRadius: 5,
									paddingHorizontal: 20,
									marginVertical: 10
								}}
									onPress={() => this.setState({ toogleDetail: !this.state.toogleDetail })}>
									<Text style={{ color: colors.primary, fontWeight: 'bold' }}>Detail personal informasion</Text>
									<IonIcons name={this.state.toogleDetail ? "ios-arrow-down" : "ios-arrow-back"} size={20} color={colors.primary} />
								</TouchableOpacity>
								{
									this.state.toogleDetail ?
										<DetailPersonal
											Gender={this.props.candidate.data_candidate.gender_display}
											BirthDate={modifyDateFormat(this.props.candidate.data_candidate.date_of_birth)}
											Country={this.props.candidate.data_candidate.domicile.province.country.name}
											Province={this.props.candidate.data_candidate.domicile.province.name}
											City={this.props.candidate.data_candidate.domicile.name}
										/>
										:
										null
								}
								<View style={styles.Button}>
									<Button
										width='40%'
										color={colors.white}
										text={!candidate.data_candidate.cv_unlocked ? "Unlock candidate" : "Download Full CV"}
										borderColor={colors.gray}
										backgroundColor={colors.primary}
										onclick={() => this.props.navigation.navigate('choosecv')}
									/>
								</View>
								<RenderList
									Title="Education"
									Logo={1}
									data={
										candidate.education_list ?
											candidate.education_list
											: null
									}
									date={true}
									onView={() => this.props.navigation.navigate('educationList')}
									clickShow={this.detailEducation}
								/>

								<RenderList
									Title="Working & experience"
									Logo={1}
									data={
										candidate.workexp_list ?
											candidate.workexp_list
											: null
									}
									date={true}
									onView={() => this.props.navigation.navigate('workList')}
									clickShow={this.detailWorking}
								/>

								<RenderList
									Title="Organization"
									Logo={1}
									data={
										candidate.organization_list ?
											candidate.organization_list
											: null
									}
									date={true}
									onView={() => this.props.navigation.navigate('organizationList')}
									clickShow={this.detailOrganization}
								/>

								<RenderList
									Title="Skill & expertise"
									data={
										candidate.skill_list ?
											candidate.skill_list
											: null
									}
									onView={() => this.props.navigation.navigate('skillList')}
									clickShow={this.detailSkill}
								/>

								<RenderList
									Title="Language"
									data={
										candidate.language_list ?
											candidate.language_list
											: null
									}
									onView={() => this.props.navigation.navigate('languageList')}
									clickShow={this.detailLanguage}
								/>

								<RenderList
									Title="Achievements"
									data={
										candidate.achivements_list ?
											candidate.achivements_list
											: null
									}
									date={false}
									onView={() => this.props.navigation.navigate('achiveList')}
									clickShow={this.detailAchievements}
								/>

								<RenderList
									Title="Personality"
									data={[{ id: 1, name: 'nama' },
									{ id: 2, name: 'nama' }, { id: 3, name: 'nama' }]}
									onView={() => this.props.navigation.navigate('personalitiList')}
									clickShow={() => console.log('dte')}
								/>

								{
									!candidate.data_candidate.cv_unlocked ?
										<View style={{ backgroundColor: colors.white, marginHorizontal: 10, marginBottom: 10, borderRadius: 10, borderWidth: 2, borderColor: colors.gray }}>
											<View>
												<View style={{ height: 40, backgroundColor: colors.white, borderTopLeftRadius: 10, borderTopRightRadius: 10, borderBottomWidth: 1, borderBottomColor: colors.gray, paddingLeft: 15, justifyContent: 'center', marginBottom: 10, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
													<Text style={[{ color: colors.primary, fontWeight: 'bold', fontSize: 17 }, myStyles.FontBold]}>
														Portfolio
											      </Text>
													{
														candidate.portfolio_list.length > 3 ?
															<TouchableOpacity style={{ flexDirection: 'row', paddingRight: 15, alignItems: 'center', justifyContent: 'center' }} >
																<Text style={{ color: colors.darkScale, fontFamily: 'DIN-Bold', fontSize: 15, paddingRight: 5, marginTop: -5 }} >
																	View - {candidate.portfolio_list.length - 2} more
													         </Text>
																<IonIcons name="ios-arrow-forward" size={20} color={colors.darkScale} />
															</TouchableOpacity>
															:
															null
													}

												</View>
											</View>
											<LinearGradient start={{ x: 0, y: 1 }} end={{ x: 0, y: 0 }} colors={[colors.cyan, '#fff']}>
												<View style={{ height: 200, alignItems: 'center', justifyContent: 'center' }}>
													<IonIcons name="md-lock" size={80} color={colors.primary} style={{ marginBottom: 17 }} />
													<TextComponent
														color={colors.primary}
														text="Information Locked"
														fontSize={18}
														bold
													/>
													{/* <Button
														width='40%'
														color={colors.white}
														text="Unlock to view"
														borderColor={colors.gray}
														backgroundColor={colors.primary}
													// onclick={() => this.props.navigation.navigate('portfolioList')}
													/> */}

												</View>
											</LinearGradient>
										</View>

										:
										<RenderList
											Title="Portfolio"
											data={
												candidate.portfolio_list ?
													candidate.portfolio_list
													: null
											}
											date={true}
											yearOnly={true}
											onView={() => this.props.navigation.navigate('portfolioList')}
											clickShow={this.detailPortfolio}
										/>
								}
							</View>
						</ScrollView>

						<DetailEducation
							data={this.state.detailEdu}
							isVisible={this.state.modalEducation}
							onClose={() => this.setState({ modalEducation: false })}
						/>
						<DetailWorking
							data={this.state.detailWork}
							isVisible={this.state.modalWorking}
							onClose={() => this.setState({ modalWorking: false })}
						/>
						<DetailOrganization
							data={this.state.detailOrg}
							isVisible={this.state.modalOrganization}
							onClose={() => this.setState({ modalOrganization: false })}
						/>
						<DetailSkill
							isVisible={this.state.modalSkill}
							onClose={() => this.setState({ modalSkill: false })}
						/>
						<DetailLanguage
							isVisible={this.state.modalLanguage}
							onClose={() => this.setState({ modalLanguage: false })}
						/>
						<DetailAchievements
							// data={this.state.detailLanguage}
							data={this.state.detailAchi}
							isVisible={this.state.modalAchievements}
							onClose={() => this.setState({ modalAchievements: false })}
						/>
						<DetailPortfolio
							data={this.state.detailFolio}
							isVisible={this.state.modalPortfolio}
							onClose={() => this.setState({ modalPortfolio: false })}
						/>

					</View>
				</SafeAreaView>
			</LinearGradient>
		)
	}

	detailEducation = async (data) => {
		await this.setState({ detailEdu: data })
		this.setState({ modalEducation: true })
		// console.log(this.state.detailEdu)
	}
	detailWorking = async (data) => {
		await this.setState({ detailWork: data })
		this.setState({ modalWorking: true })
		// console.log(this.state.detailWork)
	}
	detailOrganization = async (data) => {
		await this.setState({ detailOrg: data })
		this.setState({ modalOrganization: true })
		// console.log(this.state.detailOrg)
	}
	detailSkill = async () => {
		console.log('skill')
		// this.setState({ modalSkill: true })
	}
	detailLanguage = async (data) => {
		console.log('language')
		// await this.setState({detailLanguage:data})
		// this.setState({ modalLanguage: true })
		// console.log(this.state.detailLanguage)
	}
	detailAchievements = async (data) => {
		await this.setState({ detailAchi: data })
		this.setState({ modalAchievements: true })
		// console.log(this.state.detailAchi)
	}
	detailPortfolio = async (data) => {
		await this.setState({ detailFolio: data })
		this.setState({ modalPortfolio: true })
		// console.log(this.state.detailFolio)
	}
}

const mapStateToProps = state => {
	return {
		candidate: state.candidate
	}
}

const mapDispatchToProps = dispatch => ({
	getDataCandidate: (data) => dispatch(getDataCandidate(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(Profile);

const styles = StyleSheet.create({
	Section: {
		paddingTop: 50
	},
	Description: {
		paddingTop: 10,
		justifyContent: 'center',
		paddingHorizontal: 20,
	},
	TextDesc: {
		color: colors.grayScale,
		fontSize: 15
	},
	Contact: {
		justifyContent: 'center',
		alignItems: 'center',
		paddingVertical: 10,
	},
	ContactItem: {
		flexDirection: 'row',
		paddingVertical: 5,
		width: '80%'
	},
	ContactIcon: {
		width: 50,
		alignItems: 'center'
	},
	ButtonDetailInformation: {
		backgroundColor: colors.cyan,
		borderRadius: 5,
		marginHorizontal: 15,
		paddingHorizontal: 15,
		paddingVertical: 10,
		marginTop: 10,
		flexDirection: 'row',
	},
	TextButton: {
		fontWeight: 'bold',
		color: colors.primary,
		fontSize: 16
	},
	Button: {
		alignItems: 'center',
		justifyContent: 'center',
		paddingBottom: 20,
		paddingTop: 10,
		flexDirection: 'row',
		justifyContent: 'space-around'
	}
})