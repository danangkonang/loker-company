import React from 'react'
import { Text } from 'react-native'

import colors from '../style/Colors'
import styles from '../style'

const Error = (props) => {
	return (
		<Text style={[{
			zIndex: 0, 
			width: props.width ? props.width : '69%', 
			//marginTop: -8, 
			//elevation: 2.5, 
			marginBottom: 8,
			marginLeft:props.marginLeft ? props.marginLeft : 0, 
			//shadowRadius: 3, 
			//paddingVertical: 6, 
			//fontWeight: 'bold',
			textAlign: 'left', 
			color: colors.red, 
			//paddingHorizontal: 10, 
			backgroundColor: 'transparent', 
			borderBottomLeftRadius: 3, 
			borderBottomRightRadius: 3, 
		}, styles.FontMedium]}>
			{
				props.text ? props.text : 'Error 404'
			}
		</Text>
	)
}

export default Error;