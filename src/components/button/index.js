import React from 'react'
import { TouchableOpacity, Text, StyleSheet, View, ActivityIndicator } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

import LinearGradient from 'react-native-linear-gradient';

import colors from '../style/Colors'

const Button = (props) => {

	const {
		width,
		height,
		borderColor,
		borderWidth,
		backgroundColor,
		color,
		text,
		marginVertical,
		marginHorizontal,
		onclick,
		icon,
		disabled,
		linearBackground,
		isSave = false
	} = props
	return (
		<LinearGradient start={{ x: 0, y: 1 }} end={{ x: 0, y: 0 }}
			colors={linearBackground === false ? ['#ffffff', '#ffffff'] : ['#4782c3', '#155396']}
			style={
				[styles.Button, {
					width: width ? width : '35%',
					//height: height ? height : undefined,
					borderColor: borderColor ? borderColor : colors.white,
					marginVertical: marginVertical ? marginVertical : 2,
					marginHorizontal: marginHorizontal ? marginHorizontal : 2,
					borderWidth: borderWidth ? borderWidth : 1,
				}]
			}>
			<TouchableOpacity disabled={disabled} style={
				[{
					width: '100%',
					height: height ? height : undefined,
					backgroundColor: linearBackground === false ? backgroundColor : null,
					flexDirection: icon ? 'row' : 'column',
					paddingVertical: 10,
					paddingHorizontal: 15,
					alignItems: 'center',
					justifyContent: 'center',
					borderRadius: 6,
				}]
			} onPress={onclick ? onclick : () => alert(`Click ${text}`)}>
				<View>
					{
						isSave == false ?
							<Text style={[styles.ButtonText, { color: color ? color : colors.grayScale }]}>
								{text ? text : 'Button'}
							</Text>
							:
							<ActivityIndicator size="small" color={colors.white} />
					}
					{
						icon &&
						<View style={{ marginLeft: 7 }}>
							<Icon name={icon} size={18} color="#fff" />
						</View>
					}
				</View>
			</TouchableOpacity>
		</LinearGradient>
	)
}

export default Button

const styles = StyleSheet.create({
	Button: {
		borderRadius: 6,
		marginHorizontal: 4,
	},
	ButtonText: {
		fontSize: 14,
		fontWeight: '700',
		fontFamily: 'DIN-Medium'
	}
})