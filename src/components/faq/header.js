import React, { Component } from 'react'
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Platform
} from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome5'
import LinearGradient from "react-native-linear-gradient";
import myStyles from '../../components/style'
import colors from '../../components/style/Colors'

const Header = () => (
  <LinearGradient
    start={{ x: 0, y: 1 }}
    end={{ x: 0, y: 0 }}
    colors={['#4782c3', '#155396']}
    style={[{ paddingHorizontal: '5%' }]}>
    <View>
      <Text style={{ color: colors.white, fontSize: 24 }}>FAQ</Text>
      <View style={[styles.container]}>
        <TextInput
          style={[styles.cardSearchInput, myStyles.FontLight]}
          placeholder={'Search for your problem'}
          onChangeText={() => alert('please define your props value')}
        />
        <Icon name="search" size={16} style={styles.cardSearchIcon} />
      </View>
    </View>
  </LinearGradient>
)

export default Header;

const styles = StyleSheet.create({
  container: {
    height: Platform === 'ios' ? 60 : 65,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: '2%',
    paddingBottom: '25%'
  },
  cardSearchInput: {
    flex: 1,
    backgroundColor: colors.white,
    color: colors.darkSecond,
    borderRadius: 3,
    position: 'relative',
    height: '70%',
    fontSize: 15,
    paddingHorizontal: 40
  },
  cardSearchIcon: {
    color: colors.grayScale,
    position: 'absolute',
    top: '35%',
    marginLeft: '5%'
  },
  cardSearchButton: {
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 12,
    marginHorizontal: 8,
    padding: 10,
    borderRadius: 6
  }
})