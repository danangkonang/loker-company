const initialState = {
  tac_registration: {
    tac: false,
  },
  employer_registration: {},
  country:[],
  profil_info:{},
  analytic_info:{},
  widget_info:{},
  aplied_info:[],
  industry_info:{},
  employer_data: {},
  faq: {
    category: [],
    list_faq: []
  },
  isError: false,
  loading: false
}

const employer = (state = initialState, action) => {
  switch (action.type) {
    case 'CHANGE_TERMS_CONDITIONS':
			return {
				...state,
				tac_registration: {
					...state.tac_registration,
					tac: !state.tac_registration.tac
				}
			}
    case 'REGISTRATION_STEP_ONE':
      return {
        ...state,
        employer_registration: {
          ...state.employer_registration,
          email: action.payload.email,
          phone: action.payload.phone,
          token: action.payload.token,
        }
      }
      break;
    case 'GET_COUNTRY_FULFILLED':
      return {
        ...state,
        country: action.payload.data
      }
      break;
    case 'GET_PROFIL_INFO_FULFILLED':
      return {
        ...state,
        profil_info: action.payload.data
      }
      break;
    case 'GET_ANALYSTIC_INFO_FULFILLED':
      return {
        ...state,
        analytic_info: action.payload.data
      }
      break;
    case 'GET_WIDGET_INFO_FULFILLED':
    return {
      ...state,
      widget_info: action.payload.data
    }
    break;
    case 'GET_APLIED_INFO_FULFILLED':
    return {
      ...state,
      aplied_info: action.payload.data
    }
    break;
    case 'GET_INDUSTRY_INFO_FULFILLED':
    return {
      ...state,
      industry_info: action.payload.data
    }
    break;
    // case 'REFRESH_USER_TOKEN_FULFILLED':
    case 'REFRESH_EMPLOYER_TOKEN_FULFILLED':
      return {
        ...state,
        employer_data: action.payload.data
      }
      break;
    case 'EMPLOYER_AUTHENTICATION_PENDING':
      return {
        ...state,
        loading: true,
        isError: false
      }
      break;
    case 'EMPLOYER_AUTHENTICATION_FULFILLED':
      return {
        ...state,
        loading: false,
        employer_data: action.payload.data,
        isError: false
      };
      break;
    case 'EMPLOYER_AUTHENTICATION_REJECTED':
      return {
        ...state,
        loading: false,
        isError: true,
      }
      break;
    case 'LOGOUT_EMPLOYER':
      return {
        ...initialState
      }
      break;
    case 'SEND_EMAIL_FORGOT_PASSWORD_FULFILLED':
      return {
        ...state
      }
      break;
    case 'GET_CATEGORY_FAQ_PENDING':
      return {
        ...state,
        loading: true
      }
      break;
    case 'GET_CATEGORY_FAQ_FULFILLED':
      return {
        ...state,
        loading: false,
        faq: {
          ...state.faq,
          category: action.payload.data
        }
      }
      break;
    case 'GET_BY_CATEGORY_FAQ_PENDING':
      return {
        ...state,
        loading: true
      }
      break;
    case 'GET_BY_CATEGORY_FAQ_FULFILLED':
      const { category_faqs } = action.payload.data //full response data
      return {
        ...state,
        loading: false,
        faq: {
          ...state.faq,
          list_faq: category_faqs
        }
      }
      break;
    default:
      return state;
      break;
  }
}
export default employer;