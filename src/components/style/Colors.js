const colors = {
	white: '#ffffff',
	whiteScale: '#fdfdfd',
	primary: '#4682c4',
	gray: '#f2f2f2',
	secondary: '#cccccc',
	grayScale: '#aaaaaa',
	dark: '#000000',
	darkScale: '#919191',
	darkSecond: '#636363',
	cyan: '#e3ecf6',
	blue: '#6d9dd1',
	blueScale: '#a7bfd6',
	greenScale: '#bde8a9',
	green: 'green',
	red: '#C1272D',
	redScale: '#ff7d78',
	redSecond: '#E91B68',
	pink: '#ED1966',
	graygelap:'#e6e6e6',
	graymuda:'#f2f2f2',
	bluecyan:'#e3ecf6'
}

export default colors