import colors from './Colors'
/*
	Re-Usable Styles
 */

const styles = {
	Flex1: {
		flex: 1
	},
	FlexColumn: {
		flexDirection: 'column'
	},
	Center: {
		justifyContent: 'center',
		alignItems: 'center'
	},
	textLeft: {
		textAlign: 'left'
	},
	textCenter: {
		textAlign: 'center'
	},
	ImgContain: {
		flex: 1,
		width: undefined,
		height: undefined,
		resizeMode: 'contain'
	},
	ImgCover: {
		flex: 1,
		width: undefined,
		height: undefined,
		resizeMode: 'cover'
	},
	ImgRepeat: {
		flex: 1,
		width: undefined,
		height: undefined,
		resizeMode: 'repeat'
	},
	Mx3: {
		marginHorizontal: 3
	},
	Mx5: {
		marginHorizontal: 5
	},
	// Background
	bgPrimary: {
		backgroundColor: colors.primary
	},
	bgBlue: {
		backgroundColor: colors.blueScale
	},
	bgWhite: {
		backgroundColor: colors.white
	},

	// Bottom Fixed Button
	BottomButton: {
		width: '100%',
		position: 'absolute',
		bottom: 0,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		paddingBottom: 10,
	},

	// Profile Styles
	ProfileSection: {
		flex: 1,
		paddingTop: 50,
		paddingBottom: 10,
		paddingHorizontal: 16,
		backgroundColor: colors.blueScale,
	},
	ProfileCard: {
		flex: 1,
		elevation: 8,
		shadowOpacity: 10,
		marginTop: 15,
		marginBottom: 5,
		overflow: 'hidden',
		borderRadius: 100/15,
		shadowColor: colors.darkScale,
		backgroundColor: colors.white,
	},
	ProfileCardTitle: {
		width: '100%', 
		fontSize: 16, 
		textAlign: 'center', 
		paddingVertical: 8, 
		fontWeight: 'bold', 
		fontFamily: 'DIN-Bold',
		color: colors.darkScale
	},
	FontBold: {
		fontFamily: 'DIN-Bold',
	},
	FontMedium: {
		fontFamily: 'DIN-Medium',
	},
	FontLight: {
		fontFamily: 'DIN-Light',
	}
}

export default styles