import React from 'react'
import {
	View,
	Text,
	TouchableOpacity,
	StyleSheet,
} from 'react-native'
import Modal from "react-native-modal"
import IonIcon from 'react-native-vector-icons/Ionicons'
import colors from '../../components/style/Colors'
// import { modifyDateFormat, modifyMonth } from '../../../components/utils'
import myStyles from '../../components/style'

class ShowWorkExperience extends React.Component {
	render() {
		const {
			isVisible,
			onClose,
			data
		} = this.props
		// console.log(data.name)
		return (
			<Modal
				animationIn='zoomIn'
				animationOut='slideOutDown'
				animationInTiming={1000}
				backdropTransitionOutTiming={800}
				backdropTransitionInTiming={800}
				isVisible={isVisible}
			>
				<View style={styles.Modal}>
					<View style={styles.ModalHeader}>
						<View style={{ flex: 1, paddingLeft: 10 }}>
							<Text style={[{ 
								color: colors.primary, 
								fontWeight: 'bold', 
								fontSize: 16 ,
							},myStyles.FontBold]}>
								Language
							</Text>
						</View>
						
						<View style={{ width: 35, alignItems: 'center' }}>
							<TouchableOpacity
								onPress={onClose}
							>
								<IonIcon name="md-close" size={25} color={colors.darkScale} />
							</TouchableOpacity>
						</View>
					</View>
					<View style={{ paddingHorizontal: 15 }}>
						<View style={{ marginBottom: 10 }}>
							<Text style={[{ fontWeight: 'bold', color: colors.darkScale },myStyles.FontBold]}>Name</Text>
							<Text style={[{ color: '#aaa' },myStyles.FontLight]}>
								{data}
							</Text>
						</View>
						{/* <View style={{ marginBottom: 10 }}>
							<Text style={[{ fontWeight: 'bold', color: colors.darkScale },myStyles.FontBold]}>Link</Text>
							<Text style={[{ color: '#aaa' },myStyles.FontLight]}>
								Academy
							</Text>
						</View> */}
					</View>
				</View>
			</Modal>
		)
	}
}


export default ShowWorkExperience

const styles = StyleSheet.create({
	Modal: {
		backgroundColor: colors.white, 
		borderRadius: 10, 
		paddingTop: 10, 
		paddingBottom: 10,
	},
	ModalHeader: {
		height: 35, 
		flexDirection: 'row', 
		alignItems: 'center', 
		paddingHorizontal: 5,
	}
})