import React, { Component } from 'react'

import {
   View,
   Text,
   ScrollView,
   TouchableOpacity
} from 'react-native'
import HeaderStep from '../../components/header/HeaderStep'
import colors from '../../components/style/Colors'
import Input from '../../components/customTextInput'
import Button from '../../components/button'
import myStyles from '../../components/style'
import ErrorText from '../../components/customTextInput/Error'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
// import { employerAuthentication } from '../../_redux/_actions/employer'

class AdminIdentity extends Component {
   state = {
      token: '',

      first_name: '',
      last_name: '',
      position: '',
      email: '',
      phone: '',

      first_name_err: false,
      last_name_err: false,
      position_err: false,
      email_err: false,
      phone_err: false,

      first_name_err_msg: '',
      last_name_err_msg: '',
      position_err_msg: '',
      email_err_msg: '',
      phone_err_msg: '',

      isSave: false
   }
   render() {
      return (
         <ScrollView>
         <HeaderStep
            colorNonActive={colors.cyan}
            colorActive={colors.primary}
            step={[true, true, true, true, true, true, true]}
         />

         <View style={{ alignItems: 'center', marginTop: 20 }}>
            <Text style={[myStyles.FontBold, { color: colors.primary, fontSize: 25, textAlign: 'center' }]}>Admin identity</Text>
         </View>

         <View style={{ alignItems: 'center', paddingTop: 10 }}>
            <TouchableOpacity activeOpacity={.7} style={{ backgroundColor: colors.primary, height: 120, width: 120, borderRadius: 60, justifyContent: 'center' }}>
               <Text style={{ textAlign: 'center', fontSize: 15, color: colors.white, fontWeight: 'bold' }}>
               Icon admin
                           </Text>
            </TouchableOpacity>
         </View>

         <View style={{ flex: 1, paddingHorizontal: 15, marginVertical: 10 }}>
            <View style={{ flex: 1, backgroundColor: '#FFF', alignItems: 'center' }}>
               <Text style={{ textAlign: 'center', color: colors.grayScale, fontSize: 16 }}>
               your last education
                           </Text>
            </View>
         </View>

         <View style={[{ flex: 1, justifyContent: 'center', alignItems: 'center', marginHorizontal: 40 }]}>
            <Input
               noElevation
               borderWidth={1.5}
               borderColor={colors.secondary}
               width='100%'
               marginBottom={8}
               placeholder="First name"
               borderRadius={3}
               shadowRadius={6}
               zIndex={20}
               onchange={(v) => this.setState({ first_name: v, first_name_err: false })}
            />
         </View>
         <View style={{ marginHorizontal: 40 }}>
            {
               this.state.first_name_err == true ?
               <ErrorText
                  width='100%'
                  text={this.state.first_name_err_msg}
               /> : null
            }
         </View>

         <View style={[{ flex: 1, justifyContent: 'center', alignItems: 'center', marginHorizontal: 40 }]}>
            <Input
               noElevation
               borderWidth={1.5}
               borderColor={colors.secondary}
               width='100%'
               marginBottom={8}
               placeholder="Last name"
               borderRadius={3}
               shadowRadius={6}
               zIndex={20}
               onchange={(v) => this.setState({ last_name: v, last_name_err: false })}
            />
         </View>
         <View style={{ marginHorizontal: 40 }}>
            {
               this.state.last_name_err == true ?
               <ErrorText
                  width='100%'
                  text={this.state.last_name_err_msg}
               /> : null
            }
         </View>

         <View style={[{ flex: 1, justifyContent: 'center', alignItems: 'center', marginHorizontal: 40 }]}>
            <Input
               noElevation
               borderWidth={1.5}
               borderColor={colors.secondary}
               width='100%'
               marginBottom={8}
               placeholder="Position"
               borderRadius={3}
               shadowRadius={6}
               zIndex={20}
               onchange={(v) => this.setState({ position: v, position_err: false })}
            />
         </View>
         <View style={{ marginHorizontal: 40 }}>
            {
               this.state.position_err == true ?
               <ErrorText
                  width='100%'
                  text={this.state.position_err_msg}
               /> : null
            }
         </View>


         <View style={[{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20, marginHorizontal: 40 }]}>
            <View style={{width:'100%',borderRadius:3,marginBottom:8,borderWidth:1.5,borderColor:colors.secondary,minHeight: 50,paddingHorizontal:20,justifyContent:'center'}}>
               <Text style={{color:colors.darkScale}}>{this.props.employer_registration.email}</Text>
            </View>
         </View>
         
         <View style={[{ flex: 1, justifyContent: 'center', alignItems: 'center', marginHorizontal: 40 }]}>
            <View style={{width:'100%',borderRadius:3,marginBottom:8,borderWidth:1.5,borderColor:colors.secondary,minHeight: 50,paddingHorizontal:20,justifyContent:'center'}}>
               <Text style={{color:colors.darkScale}}>{this.props.employer_registration.phone}</Text>
            </View>
         </View>
         

         <View style={{ alignItems: 'center' }}>
            <Button
               marginVertical={15}
               width='40%'
               backgroundColor={colors.primary}
               color={colors.white}
               text="Next"
               onclick={this.validasi}
               isSave={this.state.isSave}
               disabled={this.state.isSave}
            // onclick={()=>this.props.navigation.navigate('home')}
            />
         </View>

         </ScrollView>
      )
   }

   componentDidMount = async () => {
      await this.getToken()
   }

   getToken = async () => {
      let tok = await AsyncStorage.getItem('employerToken')
      await this.setState({ token: tok })
   }

   validasi = () => {
      let { first_name, last_name, position, email, phone } = this.state
      if (first_name == '') {
         this.setState({ first_name_err: true, first_name_err_msg: 'Required' })
      } else {
         if (last_name == '') {
         this.setState({ last_name_err: true, last_name_err_msg: 'Required' })
         } else {
         if (position == '') {
            this.setState({ position_err: true, position_err_msg: 'Required' })
         } else {
            this.save()
         }
         }
      }
   }

   save = async () => {
      this.setState({ isSave: true });
      let res = await fetch(`http://128.199.224.138:8080/api/v1/company/registration/employer_personal/`, {
         method: 'PATCH',
         headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json',
         'Authorization': `JWT ${this.state.token}`
         },
         body: JSON.stringify({
         first_name: this.state.first_name,
         last_name: this.state.last_name,
         position: this.state.position,
         phone: this.props.employer_registration.phone,
         email: this.props.employer_registration.email
         })
      })

      let respon = await res.json()
      console.log(respon)
      this.props.navigation.navigate('home')
      this.setState({ isSave: true });
   }
}

const mapStateToProps = (state) => {
	return {
		employer_registration: state.employer.employer_registration
	}
}

export default connect(mapStateToProps)(AdminIdentity)
