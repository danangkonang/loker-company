import React, { Component } from 'react'
import { View, ScrollView, Text, StyleSheet, Platform, StatusBar, SafeAreaView } from 'react-native'
import Header from '../../components/header'
import Tac from '../../components/TaC'
import myStyles from '../../components/style'
import colors from '../../components/style/Colors'
import Button from '../../components/button'
import { connect } from 'react-redux'
import { changeTermsCondition } from '../../_redux/_actions/employer'
import LinearGradient from 'react-native-linear-gradient'

class TermCondition extends Component {
	
	render() {
		const { tac } = this.props.tac_registration
		const { goBack } = this.props.navigation
		return (
			<LinearGradient
				start={{ x: 0, y: 0 }}
				end={{ x: 1, y: 0 }}
				colors={['#4782c3', '#155396']}
				style={[myStyles.Flex1, { paddingTop: Platform.OS === 'ios' ? 20 : 0 }]}
			>
				<SafeAreaView style={myStyles.Flex1}>
					<StatusBar hidden={true} />
					<Header
						iconSize={25}
						logo={true}
						onpress={() => goBack()}
					/>
					<View style={[{ paddingBottom: 25 }]}>
						<ScrollView contentContainerStyle={styles.Container} showsVerticalScrollIndicator={false}>
							<View style={styles.Card}>
								<Text style={styles.CardTitle}>
									Career Support Terms & Conditions
								</Text>
								<Tac />
							</View>
						</ScrollView>
					</View>
					<View style={myStyles.BottomButton}>
						<Button
							width='30%'
							backgroundColor={tac ? colors.redScale : colors.primary}
							color={colors.white}
							text={tac ? "Disagree" : "Agree"}
							onclick={() => this._handleButton()}
						/>
					</View>
				</SafeAreaView>
			</LinearGradient>
		)
	}

	/**
	 * @function _handleButton
	 */
	_handleButton = () => {
		this.props.dispatch(changeTermsCondition())
		this.props.navigation.goBack()
	}
}

const mapStateToProps = (state) => {
	return {
		tac_registration: state.employer.tac_registration
	}
}

export default connect(mapStateToProps)(TermCondition)

const styles = StyleSheet.create({
	Container: {
		paddingBottom: 15,
		paddingTop: 10,
		paddingHorizontal: 15,
	},
	Card: {
		marginBottom:40,
		borderRadius:8,
		borderWidth: .6,
		borderColor: colors.secondary,
		elevation: 5
	},
	CardTitle: {
		paddingVertical: 10,
		paddingHorizontal: 15,
		fontSize: 16,
		color: colors.primary,
		backgroundColor: colors.white,
		fontWeight: 'bold',
		borderTopRightRadius:8,
		borderTopLeftRadius:8
	},
})