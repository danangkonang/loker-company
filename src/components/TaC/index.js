import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
// Styles
import myStyles from '../style'
import colors from '../style/Colors'

const TaC = () => {
	return (
		<View style={styles.Container}>
			<Text style={styles.TextChild}>
				Last updated on 26 September 2019
			</Text>
			<View style={{marginTop: 4, marginBottom: '5%'}}>
				<Text style={styles.TextLead}>
					Understanding your rights and responsibilities as a Career.Support user
				</Text>
				<Text style={styles.TextHead}>
					Privacy Reminder
				</Text>
				<Text style={styles.TextLead}>
					Protect your personal information by never providing credit card or bank account numbers to prospective employers.
				</Text>
				<Text style={styles.TextHead}>
					Terms of Use
				</Text>
				<Text style={styles.TextLead}>
					This page states the Terms of Use ("Terms") under which you ("You") may use the Career.Support Sites and the Services (each as defined below). These Terms include the Career.Support Privacy Policy, which is incorporated into these Terms by reference.
				</Text>
				<Text style={styles.TextLead}>
					These Terms constitute a binding agreement between You and Career.Support or the Career.Support company operating the Web site for the country in which You live or in which business is headquartered (“Career.Support”), and are deemed accepted by You each time that You use or access any Career.Support Site or Career.Support Services. If You do not accept the Terms stated here, do not use the Career.Support Sites and the Career.Support Services.
				</Text>
				<Text style={styles.TextLead}>
					The Career.Support Sites are defined as any Web sites under Career.Support’s control, whether partial or otherwise (including, without limitation, Career.Support.com, and the Web site from which these terms of use were accessed from) and include the Career.Support Services. The Career.Support Services are defined as the applications and services offered by Career.Support, including an on-line service to post and search employment opportunities and including any application accessed through the Facebook Platform and/or any mobile or other interface that allows You to access such application (collectively, “Career.Support Services”). The Career.Support Sites also allow Users to create individual profiles, which may include personal information (”Profiles”), and to make these Profiles, or aspects thereof, public.  In addition, Career.Support may collect information about You from publicly-available websites and may use this information to create a Profile or append it to an existing Profile.
				</Text>
				<Text style={styles.TextLead}>
					Career.Support may revise these Terms at any time by posting an updated version to this Web page. You should visit this page periodically to review the most current Terms because they are binding on You.
					{"\n\n"}
					Users who violate these Terms may have their access and use of the Career.Support Sites suspended or terminated, at Career.Support’s discretion.
					{"\n\n"}
					You must be 13 years of age or older to visit or use any Career.Support Site in any manner, and, if under the age of 18 or the age of majority as that is defined in Your jurisdiction, must use any Career.Support Site under the supervision of a parent, legal guardian, or other responsible adult.
				</Text>
				<Text style={styles.TextLead}>
					You may not use the Career.Support Content or Profiles to determine a consumer's eligibility for:{"\n"}
					(a)	credit or insurance for personal, family, or household purposes; {"\n"}
					(b)	employment;{"\n"}
					(c)	a government license or benefit.
				</Text>
				<Text style={styles.TextLeadHead}>
					1. Use of Career.Support Content
				</Text>
				<Text style={styles.TextLead}>
					Career.Support authorizes You, subject to these Terms, to access and use the Career.Support Sites and the Career.Support Content (as defined below) and to download and print a single copy of the content available on or from the Career.Support Sites solely for Your personal, non-commercial use. The contents of the Career.Support Sites, such as designs, text, graphics, images, video, information, logos, button icons, software, audio files and other Career.Support content (collectively, "Career.Support Content"), are protected under copyright, trademark and other laws.
					All Career.Support Content is the property of Career.Support or its licensors. The compilation (meaning the collection, arrangement and assembly) of all content on the Career.Support Sites is the exclusive property of Career.Support and is protected by copyright, trademark, and other laws. Unauthorized use of the Career.Support Content may violate these laws and/or applicable communications regulations and statutes, and is strictly prohibited. You must preserve all copyright, trademarks, service mark and other proprietary notices contained in the original Career.Support Content on any authorized copy You make of the Career.Support Content.{"\n\n"}

					Any code that Career.Support creates to generate or display any Career.Support Content or the pages making up any Career.Support Site is also protected by Career.Support's copyright and You may not copy or adapt such code.{"\n\n"}

					You agree not to sell or modify the Career.Support Content or reproduce, display, publicly perform, distribute, or otherwise use the Career.Support Content in any way for any public or commercial purpose, in connection with products or services that are not those of the Career.Support Sites, in any other manner that is likely to cause confusion among consumers, that disparages or discredits Career.Support or its licensors, that dilutes the strength of Career.Support's or its licensor’s property, or that otherwise infringes Career. Support's or its licensor’s intellectual property rights. You further agree to in no other way misuse Career.Support Content. The use of the Career.Support Content on any other application, web site or in a networked computer environment for any purpose is prohibited. Any code that Career.Support creates to generate or display any Career.Support Content or the pages making up any Application or Service is also protected by Career.Support's copyright and you may not copy or adapt such code.{"\n"}
				</Text>
				<Text style={styles.TextLeadHead}>
					2.	Use of the Career.Support Services
				</Text>
				<Text style={styles.TextLead}>
					The job posting, resume database (“Career.Support Resume Database”) and other features of the Career.Support Sites may be used only by individuals seeking employment and/or career information and by employers seeking employees. In addition, Career.Support Networking and Profiles may be used by individuals for permitted professional and personal networking purposes. Your use of the Career.Support Services is also subject to any other contracts You may have with Career.Support. In the case of any conflict between these Terms and any contract you have with Career.Support, the terms of your contract will prevail. The term “post” as used herein shall mean information that You submit, publish or display on a Career.Support Site.{"\n\n"}

					You are responsible for maintaining the confidentiality of Your account, Profile and passwords, as applicable. You may not share Your password or other account access information with any other party, temporarily or permanently, and You shall be responsible for all uses of Your Career.Support account, whether or not authorized by You. You agree to immediately notify Career.Support of any unauthorized use of Your account, Profile, or passwords.
				</Text>
				<Text style={styles.TextLeadHead}>
					All Career.Support Users agree to not :
				</Text>
				<Text style={styles.TextLead}>
					(a)	transmit, post, distribute, store or destroy material, including without limitation Career.Support Content, in violation of any applicable law or regulation, including but not limited to laws or regulations governing the collection, processing, or transfer of personal information, or in breach of Career.Support’s privacy policy;
				</Text>
				<Text style={styles.TextLead}>
					(b) take any action that imposes an unreasonable or disproportionately large load on any Career.Support Site's infrastructure;
				</Text>
				<Text style={styles.TextLead}>
					(c)	use any device to navigate or search any Career.Support Site other than the tools available on the Site, generally available third party web browsers, or other tools approved by Career.Support;
				</Text>
				<Text style={styles.TextLead}>
					(d)	use any data mining, robots or similar data gathering or extraction methods;
				</Text>
				<Text style={styles.TextLead}>
					(e)	violate or attempt to violate the security of any Career.Support Site including attempting to probe, scan or test the vulnerability of a system or network or to breach security or authentication measures without proper authorization;
				</Text>
				<Text style={styles.TextLead}>
					(f)	forge any TCP/IP packet header or any part of the header information in any e-mail or newsgroup posting;
				</Text>
				<Text style={styles.TextLead}>
					(g)	reverse engineer or decompile any parts of any Career.Support Site;
				</Text>
				<Text style={styles.TextLead}>
					(h)	aggregate, copy or duplicate in any manner any of the Career.Support Content or information available from any Career.Support Site, including expired job postings, other than as permitted by these Terms;
				</Text>
				<Text style={styles.TextLead}>
					(i)	frame or link to any Career.Support Content or information available from any Career.Support Site, unless permitted by these Terms;
				</Text>
				<Text style={styles.TextLead}>
					(j)	post any content or material that promotes or endorses false or misleading information or illegal activities, or endorses or provides instructional information about illegal activities or other activities prohibited by these Terms, such as making or buying illegal weapons, violating someone's privacy, providing or creating computer viruses or pirating media;
				</Text>
				<Text style={styles.TextLead}>
					(k)	post any resume or Profile or apply for any job on behalf of another party;
				</Text>
				<Text style={styles.TextLead}>
					(l)	defer any contact from an employer to any agent, agency, or other third party;
				</Text>
				<Text style={styles.TextLead}>
					(m)	set more than one copy of the same resume to public at any one time;
				</Text>
				<Text style={styles.TextLead}>
					(n)	share with a third party any login credentials to any Career.Support Site;
				</Text>
				<Text style={styles.TextLead}>
					(o)	access data not intended for You or logging into a server or account which You are not authorized to access;
				</Text>
				<Text style={styles.TextLead}>
					(p)	post or submit to any Career.Support Site any incomplete, false or inaccurate biographical information or information which is not Your own;
				</Text>
				<Text style={styles.TextLead}>
					(q)	post content that contains restricted or password-only access pages, or hidden pages or images;
				</Text>
				<Text style={styles.TextLead}>
					(r)	solicit passwords or personally identifiable information from other Users;
				</Text>
				<Text style={styles.TextLead}>
					(s)	delete or alter any material posted by any other person or entity;
				</Text>
				<Text style={styles.TextLead}>
					(t)	harass, incite harassment or advocate harassment of any group, company, or individual;
				</Text>
				<Text style={styles.TextLead}>
					(u)	send unsolicited mail or email, make unsolicited phone calls or send unsolicited faxes promoting and/or advertising products or services to any User, or contact any users that have specifically requested not to be contacted by You;
				</Text>
				<Text style={styles.TextLead}>
					(v)	attempt to interfere with service to any User, host or network, including, without limitation, via means of submitting a virus to any Career.Support Site, overloading, "flooding", "spamming", "mailbombing" or "crashing";
				</Text>
				<Text style={styles.TextLead}>
					(w)	promote or endorse an illegal or unauthorized copy of another person's copyrighted work, such by as providing or making available pirated computer programs or links to them, providing or making available information to circumvent manufacture-installed copy-protect devices, or providing or making available pirated music or other media or links to pirated music or other media files; or
				</Text>
				<Text style={styles.TextLead}>
					(x)	use the Career.Support Services for any unlawful purpose or any illegal activity, or post or submit any content, resume, or job posting that is defamatory, libelous, implicitly or explicitly offensive, vulgar, obscene, threatening, abusive, hateful, racist, discriminatory, of a menacing character or likely to cause annoyance, inconvenience, embarrassment, anxiety or could cause harassment to any person or include any links to pornographic, indecent or sexually explicit material of any kind, as determined by Career.Support’s discretion.{"\n"}
				</Text>
				<Text style={styles.TextLead}>
					Violations of system or network security may result in civil and/or criminal liability. Career.Support will investigate occurrences which may involve such violations and may involve, and cooperate with, law enforcement authorities in prosecuting Users who are involved in such violations.{"\n"}
				</Text>
				<Text style={styles.TextLeadHead}>
					3.	Additional Terms Applicable to Employers
				</Text>
				<Text style={styles.TextLead}>
					Employers are solely responsible for their postings on Career.Support Sites. Career.Support is not to be considered to be an employer with respect to Your use of any Career.Support Site and Career.Support shall not be responsible for any employment decisions, for whatever reason, made by any entity posting jobs on any Career.Support Site.{"\n\n"}

					You understand and acknowledge that if You cancel Your employer account or Your employer account is terminated, all Your account information from Career.Support, including saved resumes, network contacts, and email mailing lists, will be marked as deleted in and may be deleted from Career.Support's databases. Information may continue to be available for some period of time because of delays in propagating such deletion through Career.Support’s web servers.{"\n\n"}

					In order to protect our Career.Support Users from commercial advertising or solicitation, Career.Support reserves the right to restrict the number of e-mails which an employer may send to Users to a number which Career.Support deems appropriate in its sole discretion. You shall use the Career.Support Networking and Profiles in accordance with all applicable privacy and data protection laws.{"\n"}
				</Text>
				<Text style={styles.TextHead}>
					Job postings
				</Text>
				<Text style={styles.TextLead}>
					A Job posting may not contain:
				</Text>
				<Text style={styles.TextLead}>
					(a)	any hyperlinks, other than those specifically authorized by Career.Support;
				</Text>
				<Text style={styles.TextLead}>
					(b)	misleading, unreadable, or "hidden" keywords, repeated keywords or keywords that are irrelevant to the job opportunity being presented, as determined in Career.Support’s reasonable discretion;
				</Text>
				<Text style={styles.TextLead}>
					(c)	the names, logos or trademarks of unaffiliated companies other than those of your customer save where expressly agreed by Career.Support;
				</Text>
				<Text style={styles.TextLead}>
					(d)	the names of colleges, cities, states, towns or countries that are unrelated to the posting;
				</Text>
				<Text style={styles.TextLead}>
					(e)	inaccurate, false, or misleading information; and
				</Text>
				<Text style={styles.TextLead}>
					(f)	material or links to material that exploits people in a sexual, violent or other manner, or solicits personal information from anyone under 18.
				</Text>
				<Text style={styles.TextLead}>
					You may not use Your Career.Support job posting, {"\n"}Career.Support Networking, or Profiles to:
				</Text>
				<Text style={styles.TextLead}>
					(a)	post jobs in a manner that does not comply with applicable local, national and international laws, including but not limited to laws relating to labor and employment, equal employment opportunity and employment eligibility requirements, data privacy, data access and use, and intellectual property;
				</Text>
				<Text style={styles.TextLead}>
					(b)	post jobs that require citizenship of any particular country or lawful permanent residence in a country as a condition of employment, unless otherwise required in order to comply with law, regulations, executive order, or federal, state or local government contract;
				</Text>
				<Text style={styles.TextLead}>
					(c)	post jobs that include any screening requirement or criterion in connection with a job posting where such requirement or criterion is not an actual and legal requirement of the posted job;
				</Text>
				<Text style={styles.TextLead}>
					(d)	with respect to Profiles, determine a consumer's eligibility for: (i) credit or insurance for person, family, or household purposes; (ii) employment; or (iii) a government license of benefit.
				</Text>
				<Text style={styles.TextLead}>
					(e)	post jobs or other advertisements for competitors of Career.Support or post jobs or other content that contains links to any site competitive with Career.Support;
				</Text>
				<Text style={styles.TextLead}>
					(f)	sell, promote or advertise products or services;
				</Text>
				<Text style={styles.TextLead}>
					(g)	post any franchise, pyramid scheme, "club membership", distributorship, multi-level marketing opportunity, or sales representative agency arrangement;
				</Text>
				<Text style={styles.TextLead}>
					(h)	post any business opportunity that requires an upfront or periodic payment or requires recruitment of other members, sub-distributors or sub-agents;
				</Text>
				<Text style={styles.TextLead}>
					(i)	post any business opportunity that pays commission only unless the posting clearly states that the available job pays commission only and clearly describes the product or service that the job seeker would be selling;
				</Text>
				<Text style={styles.TextLead}>
					(j)	promote any opportunity that does not represent bona fide employment which is generally indicated by the employer’s use of IRS forms W-2 or 1099;
				</Text>
				<Text style={styles.TextLead}>
					(k)	post jobs on any Career.Support Site for modeling, acting, talent or entertainment agencies or talent scouting positions;
				</Text>
				<Text style={styles.TextLead}>
					(l)	advertise sexual services or seek employees for jobs of a sexual nature;
				</Text>
				<Text style={styles.TextLead}>
					(m)	request the use of human body parts or the donation of human parts, including, without limitation, reproductive services such as egg donation and surrogacy;
				</Text>
				<Text style={styles.TextLead}>
					(n)	endorse a particular political party, political agenda, political position or issue;
				</Text>
				<Text style={styles.TextLead}>
					(o)	promote a particular religion;
				</Text>
				<Text style={styles.TextLead}>
					(p)	post jobs located in countries subject to economic sanctions of the United States Government; and
				</Text>
				<Text style={styles.TextLead}>
					(q)	Except where allowed by applicable law, post jobs which require the applicant to provide information relating to his/her (i) racial or ethnic origin (ii) political beliefs (iii) philosophical or religious beliefs (iv) membership of a trade union (v) physical or mental health (vi) sexual life (vii) the commission of criminal offences or proceedings or (vii) age.{"\n"}
				</Text>
				<Text style={styles.TextLead}>
					Career.Support reserves the right to remove any job posting or content from any Career.Support Site, which in the reasonable exercise of Career.Support’s discretion, does not comply with the above Terms, or if any content is posted that Career.Support believes is not in the best interest of Career.Support.{"\n"}
				</Text>
				<Text style={styles.TextLead}>
					If at any time during Your use of the Career.Support Services, You made a misrepresentation of fact to Career.Support or otherwise misled Career.Support in regards to the nature of Your business activities, Career.Support will have grounds to terminate Your use of the Career.Support Services.{"\n"}
				</Text>
				<Text style={styles.TextHead}>
					Resume Database
				</Text>
				<Text style={styles.TextLead}>
					Use of the Career.Support Resume Database by Employers{"\n"}
				</Text>
				<Text style={styles.TextLead}>
					You shall use the Career.Support Resume Database as provided in these Terms and in any contract You have with Career.Support. You shall use the Career.Support Resume Database in accordance with all applicable privacy and data protection laws, and You agree You shall not further disclose any of the data from Career.Support Resume Database to any third party, unless You are an authorized recruitment agency, staffing agency, advertising or other agency or using the resume explicitly for employment purposes.{"\n\n"}
					You shall take appropriate physical, technical, and administrative measures to protect the data You have obtained from Career.Support Resume Database from loss, misuse, unauthorized access, disclosure, alteration or destruction. You shall not share Resume Database seat-based license login credentials with any other party, nor share Resume Database pay-per-view license login credentials with any party.{"\n\n"}
					The Career.Support Resume Database shall not be used:
				</Text>
				<Text style={styles.TextLead}>
					(a)	for any purpose other than as an employer seeking employees, including but not limited to advertising promotions, products, or services to any resume holders;
				</Text>
				<Text style={styles.TextLead}>
					(b)	to make unsolicited phone calls or faxes or send unsolicited mail, email, or newsletters to resume holders or to contact any individual unless they have agreed to be contacted (where consent is required or, if express consent is not required, who has not informed you that they do not want to be contacted); or
				</Text>
				<Text style={styles.TextLead}>
					(c)	to source candidates or to contact job seekers or resume holders in regards to career fairs and business opportunities prohibited by Section 3.{"\n"}
				</Text>
				<Text style={styles.TextLead}>
					In order to ensure a safe and effective experience for all of our customers, Career.Support reserves the right to limit the amount of data (including resume views) that may be accessed by You in any given time period. These limits may be amended in Career.Support’s sole discretion from time to time.{"\n"}
				</Text>
				<Text style={styles.TextLeadHead}>
					4.	Additional terms applicable to Job Seekers
				</Text>
				<Text style={styles.TextLead}>
					When You register with any Career.Support Site, You will be asked to create an account and provide Career.Support with certain information including, without limitation, a valid email address (Your "Information").
				</Text>
				<Text style={styles.TextLead}>
					Any Profile You submit must be accurate and describe You, an individual person. The Profile requires standard fields to be completed and you may not include in these fields any telephone numbers, street addresses, email addresses or other means of contacting You, other than Your last name and URLs.
				</Text>
				<Text style={styles.TextLead}>
					You acknowledge and agree that You are solely responsible for the form, content and accuracy of any resume or material contained therein placed by You on the Career.Support Sites.
				</Text>
				<Text style={styles.TextLead}>
					Career.Support reserves the right to offer third party services and products to You based on the preferences that You identify in Your registration and at any time thereafter or you have agreed to receive, such offers may be made by Career.Support or by third parties. Please see Career.Support's Privacy Policy, for further details regarding Your Information.
				</Text>
				<Text style={styles.TextLead}>
					You understand and acknowledge that You have no ownership rights in Your account and that if You cancel Your Career.Support account or Your Career.Support account is terminated, all Your account information from Career.Support, including resumes and Profiles will be marked as deleted in and may be deleted from Career.Support's databases and will be removed from any public area of the Career.Support Sites. In addition, third parties may retain saved copies of Your Information.
				</Text>
				<Text style={styles.TextLead}>
					Career.Support reserves the right to delete Your account and all of Your Information after a significant duration of inactivity.{"\n"}
				</Text>
				<Text style={styles.TextLeadHead}>
					5.	User Content and Submissions
				</Text>
				<Text style={styles.TextLead}>
					You understand that all information, data, text, software, music, sound, photographs, graphics, video, advertisements, messages or other materials submitted, posted or displayed by You on or through a Career.Support Site ("User Content") is the sole responsibility of the person from which such User Content originated. Career.Support claims no ownership or control over any User Content. You or a third party licensor, as appropriate, retain all patent, trademark and copyright to any User Content you submit, post or display on or through Career.Support and you are responsible for protecting those rights, as appropriate. By submitting, posting or displaying User Content on or through Career.Support, you grant Career.Support a worldwide, non-exclusive, royalty-free, transferable, sub-licensable license to use, reproduce, adapt, distribute and publish such User Content through Career.Support. In addition, by submitting, posting or displaying User Content which is intended to be available to the general public, you grant Career.Support a worldwide, non-exclusive, royalty-free license to reproduce, adapt, distribute and publish such User Content for the purpose of promoting Career.Support and its services. Career.Support will discontinue this licensed use within a commercially reasonable period after such User Content is removed from Career.Support. Career.Support reserves the right to refuse to accept, post, display or transmit any User Content in its sole discretion
				</Text>
				<Text style={styles.TextLead}>
					You also represent and warrant that You have the right to grant, or that the holder of any rights, including moral rights in such content has completely and effectively waived all such rights and validly and irrevocably granted to You the right to grant, the license stated above. If You post User Content in any public area of any Career.Support Site, You also permit any User to access, display, view, store and reproduce such User Content for personal use. Subject to the foregoing, the owner of such User Content placed on any Career.Support Site retains any and all rights that may exist in such User Content. Career.Support may review and remove any User Content that, in its sole judgment, violates these Terms, violates applicable laws, rules or regulations, is abusive, disruptive, offensive or illegal, or violates the rights of, or harms or threatens the safety of, Users of any Career.Support Site. Career.Support reserves the right to expel Users and prevent their further access to the Career.Support Sites and/or use of Career.Support Services for violating the Terms or applicable laws, rules or regulations. Career.Support may take any action with respect to User Content that it deems necessary or appropriate in its sole discretion if it believes that such User Content could create liability for Career.Support, damage Career.Support’s brand or public image, or cause Career.Support to lose Users or (in whole or in part) the services of its ISPs or other suppliers.
				</Text>
				<Text style={styles.TextLead}>
					Career.Support does not represent or guarantee the truthfulness, accuracy, or reliability of User Content, derivative works from User Content, or any other communications posted by Users nor does Career.Support endorse any opinions expressed by Users. You acknowledge that any reliance on material posted by other Users will be at Your own risk.
				</Text>
				<Text style={styles.TextLead}>
					The following is a partial list of User Content that is prohibited on the Application. The list below is for illustration only and is not a complete list of all prohibited User Content.{"\n"}
				</Text>
				<Text style={styles.TextLeadHead}>
					Content that:
				</Text>
				<Text style={styles.TextLead}>
					is implicitly or explicitly offensive, such as User Content that engages in, endorses or promotes racism, bigotry, discrimination, hatred or physical harm of any kind against any group or individual; harasses, incites harassment or advocates harassment of any group or individual; involves the transmission of “junk mail”, “chain letters,” or unsolicited mass mailing, “spamming” or “phishing”; promotes or endorses false or misleading information or illegal activities or conduct that is abusive, threatening, obscene, defamatory or libelous; promotes or endorses an illegal or unauthorized copy of another person's copyrighted work, such as providing or making available pirated computer programs or links to them, providing or making available information to circumvent manufacture-installed copy-protect devices, or providing or making available pirated music or other media or links to pirated music or other media files; contains restricted or password only access pages, or hidden pages or images; displays or links to pornographic, indecent or sexually explicit material of any kind; provides or links to material that exploits people under the age of 18 in a sexual, violent or other manner, or solicits personal information from anyone under 18; or provides instructional information about illegal activities or other activities prohibited by these Terms, including without limitation, making or buying illegal weapons, violating someone's privacy, providing or creating computer viruses or pirating any media; and solicits passwords or personal identifying information from other Users.
				</Text>
				<Text style={styles.TextLead}>
					Any Profile You submit must describe You, an individual person. Examples of inappropriate and prohibited Profiles include, but are not limited to, Profiles that purport to represent an animal, place, inanimate object, fictional character, or real individual that is not You.
				</Text>
				<Text style={styles.TextLead}>
					You may not include, in any User Content submitted to Career.Support Networking, information that may be interpreted as a direct solicitation, advertisement or recruitment for an available job position directed to individuals seeking employment on either a full time or part time basis. In order to protect our Career.Support Community Users from commercial advertising or solicitation, Career.Support reserves the right to restrict the number of e-mails or other messages which a User may send to other Users to a number which Career.Support deems appropriate in its sole discretion.
				</Text>
				<Text style={styles.TextLead}>
					Profiles derived from User Content may also be made available through the Career.Support Sites. Career.Support does not make any representations regarding the accuracy or validity of such derived works or their appropriateness for evaluation by employers. Derived Profiles may differ significantly from User Content.
				</Text>
				<Text style={styles.TextLead}>
					We appreciate hearing from our Users and welcome Your comments regarding our services and the Career.Support Sites. Please be advised, however, that our policy does not permit us to accept or consider creative ideas, suggestions, inventions or materials other than those which we have specifically requested. While we do value Your feedback on our services, please be specific in Your comments regarding our services and do not submit creative ideas, inventions, suggestions, or materials. If, despite our request, You send us creative suggestions, ideas, drawings, concepts, inventions, or other information (collectively the "Submission"), the Submission shall be the property of Career.Support. None of the Submission shall be subject to any obligation of confidentiality on our part and we shall not be liable for any use or disclosure of any Submission. Career.Support shall own exclusively all now known or later discovered rights to the Submission and shall be entitled to unrestricted use of the Submission for any purpose whatsoever, commercial or otherwise, without compensation to You or any other person.{"\n"}
				</Text>
				<Text style={styles.TextLeadHead}>
					6.	Policy Regarding Termination Of Users Who Infringe The Copyright Or Other Intellectual Property Rights Of Others
				</Text>
				<Text style={styles.TextLead}>
					Career.Support respects the intellectual property of others, and we ask our Users and content partners to do the same. The unauthorized posting, reproduction, copying, distribution, modification, public display or public performance of copyrighted works constitutes infringement of the copyright owners rights. As a condition to Your use of the Career.Support Sites, You agree not to use any Career.Support Site to infringe the intellectual property rights of others in any way. Career.Support reserves the right to terminate the accounts of any Users, and block access to the Career.Support Sites of any Users who are repeat infringers of the copyrights, or other intellectual property rights, of others. Career.Support reserves the right, in its sole discretion, to take these actions to limit access to the Site and/or terminate the accounts of any time, in our sole discretion Users who infringe any intellectual property rights of others, whether or not there is any repeat infringement, with or without notice, and without any liability to the User who is terminated or to the User whose access is blocked. Notwithstanding the foregoing, in the event that You believe in good faith that a notice of copyright infringement has been wrongly filed against You, please contact Career.Support as set forth in Section 6 above.{"\n"}
				</Text>
				<Text style={styles.TextLeadHead}>
					7.	Career.Support's Liability
				</Text>
				<Text style={styles.TextLead}>
					The Career.Support Sites act as, among other things, venues for (i) employers to post job opportunities and search for and evaluate job candidates and (ii) candidates to post resumes and Profiles and search for and evaluate job opportunities. Career.Support does not screen or censor the listings, including Profiles offered. Career.Support is not involved in, and does not control, the actual transaction between employers and candidates. As a result, Career.Support is not responsible for User Content, the quality, safety or legality of the jobs or resumes posted, the truth or accuracy of the listings, the ability of employers to offer job opportunities to candidates or the ability of candidates to fill job openings and Career.Support makes no representations about any jobs, resumes or User Content on the Career.Support Sites. While Career.Support reserves the right in its sole discretion to remove User Content, job postings, resumes or other material from the Career.Support Sites from time to time, Career.Support does not assume any obligation to do so and to the extent permitted by law, disclaims any liability for failing to take any such action.
				</Text>
				<Text style={styles.TextLead}>
					Because User authentication on the Internet is difficult, Career.Support cannot and does not confirm that each User is who they claim to be. Because we do not and cannot be involved in User-to-User dealings or control the behavior of participants on any Career.Support Site, in the event that You have a dispute with one or more Users, You release Career.Support (and our agents and employees) from claims, demands and damages (actual and consequential and direct and indirect) of every kind and nature, known and unknown, suspected and unsuspected, disclosed and undisclosed, arising out of or in any way connected with such disputes to the fullest extent permitted by law.
				</Text>
				<Text style={styles.TextLead}>
					The Career.Support Sites and the Career.Support Content may contain inaccuracies or typographical errors. Career.Support makes no representations about the accuracy, reliability, completeness, or timeliness of any Career.Support Site or the Career.Support Content. The use of all Career.Support Sites and the Career.Support Content is at Your own risk. Changes are periodically made to Career.Support Sites and may be made at any time. Career.Support cannot guarantee and does not promise any specific results from use of any Career.Support Site. No advice or information, whether oral or written, obtained by a User from Career.Support or through or from any Career.Support Site shall create any warranty not expressly stated herein.
				</Text>
				<Text style={styles.TextLead}>
					Career.Support encourages You to keep a back-up copy of any of Your User Content. To the extent permitted by law, in no event shall Career.Support be liable for the deletion, loss, or unauthorized modification of any User Content.
				</Text>
				<Text style={styles.TextLead}>
					Career.Support does not provide or make any representation as to the quality or nature of any of the third party products or services purchased through any Career.Support Site, or any other representation, warranty or guaranty. Any such undertaking, representation, warranty or guaranty would be furnished solely by the provider of such third party products or services, under the terms agreed to by the provider.
				</Text>
				<Text style={styles.TextLead}>
					If notified of any content or other materials which allegedly do not conform to these Terms, Career.Support may in its sole discretion investigate the allegation and determine whether to remove or request the removal of the content. Career.Support has no liability or responsibility to Users for performance or nonperformance of such activities.{"\n"}
				</Text>
				<Text style={styles.TextLeadHead}>
					8.	Disclaimer of Warranty
				</Text>
				<Text style={styles.TextLead}>
					TO THE FULLEST EXTENT POSSIBLE BY LAW, CAREER.SUPPORT DOES NOT WARRANT THAT ANY CAREER.SUPPORT SITE OR ANY CAREER.SUPPORT SERVICES WILL OPERATE ERROR-FREE OR THAT ANY CAREER.SUPPORT SITE AND ITS SERVERS ARE FREE OF COMPUTER VIRUSES OR OTHER HARMFUL MECHANISMS. IF YOUR USE OF ANY CAREER.SUPPORT SITE OR THE CAREER.SUPPORT CONTENT RESULTS IN THE NEED FOR SERVICING OR REPLACING EQUIPMENT OR DATA OR ANY OTHER COSTS, CAREER.SUPPORT IS NOT RESPONSIBLE FOR THOSE COSTS. THE CAREER.SUPPORT SITES AND CAREER.SUPPORT CONTENT ARE PROVIDED ON AN "AS IS" BASIS WITHOUT ANY WARRANTIES OF ANY KIND. CAREER.SUPPORT, TO THE FULLEST EXTENT PERMITTED BY LAW, DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS OR IMPLIED, INCLUDING THE WARRANTY OF MERCHANTABILITY, FITNESS FOR PARTICULAR PURPOSE AND NON-INFRINGEMENT. CAREER.SUPPORT MAKES NO WARRANTIES ABOUT THE ACCURACY, RELIABILITY, COMPLETENESS, OR TIMELINESS OF THE CAREER.SUPPORT CONTENT, SERVICES, SOFTWARE, TEXT, GRAPHICS, AND LINKS.{"\n"}
				</Text>
				<Text style={styles.TextLeadHead}>
					9.	Disclaimer Of Consequential Damages
				</Text>
				<Text style={styles.TextLead}>
					TO THE FULLEST EXTENT POSSIBLE BY LAW, IN NO EVENT SHALL CAREER.SUPPORT, ITS SUPPLIERS, OR ANY THIRD PARTIES MENTIONED ON ANY CAREER.SUPPORT SITE BE LIABLE FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT LIMITATION, INCIDENTAL AND CONSEQUENTIAL DAMAGES, LOST PROFITS, OR DAMAGES RESULTING FROM LOST DATA, LOST EMPLOYMENT OPPORTUNITY OR BUSINESS INTERRUPTION) RESULTING FROM THE USE OR INABILITY TO USE ANY CAREER.SUPPORT SITE AND THE CAREER.SUPPORT CONTENT, WHETHER BASED ON WARRANTY, CONTRACT, TORT, OR ANY OTHER LEGAL THEORY, AND WHETHER OR NOT CAREER.SUPPORT IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.{"\n"}
				</Text>
				<Text style={styles.TextLeadHead}>
					10.	Limitation of Liability
				</Text>
				<Text style={styles.TextLead}>
					TO THE FULLEST EXTENT POSSIBLE BY LAW, CAREER.SUPPORT'S MAXIMUM LIABILITY ARISING OUT OF OR IN CONNECTION WITH ANY CAREER.SUPPORT SITE OR YOUR USE OF THE CAREER.SUPPORT CONTENT, REGARDLESS OF THE CAUSE OF ACTION (WHETHER IN CONTRACT, TORT, BREACH OF WARRANTY OR OTHERWISE), WILL NOT EXCEED RP 1,000,000.{"\n"}
				</Text>
				<Text style={styles.TextLeadHead}>
					11.	Links to Other Sites
				</Text>
				<Text style={styles.TextLead}>
					The Career.Support Sites contain links to third party Web sites. These links are provided solely as a convenience to You and not as an endorsement by Career.Support of the contents on such third-party Web sites. Career.Support is not responsible for the content of linked third-party sites and does not make any representations regarding the content or accuracy of materials on such third party Web sites. If You decide to access linked third-party Web sites, You do so at Your own risk.{"\n"}
				</Text>
				<Text style={styles.TextLeadHead}>
					12.	No Resale or Unauthorized Commercial Use
				</Text>
				<Text style={styles.TextLead}>
					You agree not to resell or assign Your rights or obligations under these Terms. You also agree not to make any unauthorized commercial use of any Career.Support Site.{"\n"}
				</Text>
				<Text style={styles.TextLeadHead}>
					13.	Indemnity
				</Text>
				<Text style={styles.TextLead}>
					You agree to defend, indemnify, and hold harmless Career.Support, its affiliates, and their respective officers, directors, employees and agents, from and against any claims, actions or demands, including without limitation reasonable legal and accounting fees, alleging or resulting from (i) any User Content or other material You provide to any Career.Support Site, (ii) Your use of any Career.Support Content, or (iii) Your breach of these Terms. Career.Support shall provide notice to You promptly of any such claim, suit, or proceeding.{"\n"}
				</Text>
				<Text style={styles.TextLeadHead}>
					14.	Additional Terms
				</Text>
				<Text style={styles.TextLead}>
					Certain areas of the Career.Support Sites are subject to additional Terms. By using such areas, or any part thereof, You agree to be bound by the additional Terms applicable to such areas. By using any areas of this website or the other {"\n"}
				</Text>
				<Text style={styles.TextLeadHead}>
					15.	Mobile Services
				</Text>
				<Text style={styles.TextLead}>
					If You use the Career.Support Sites through a mobile device, You agree that information about Your use of the Career.Support Sites through Your mobile device and carrier may be communicated to us, including but not limited to Your mobile carrier, Your mobile device, or Your physical location. In addition, use of the Career.Support Sites through a mobile device may cause data to be displayed on and through Your mobile device. By accessing the Career.Support Sites using a mobile device, You represent that to the extent You import any of Your Career.Support data to Your mobile device that You have authority to share the transferred data with Your mobile carrier or other access provider. In the event You change or deactivate Your mobile account, You must promptly update Your Career.Support account information to ensure that Your messages are not sent to the person that acquires Your old number and failure to do so is Your responsibility. You acknowledge You are responsible for all charges and necessary permissions related to accessing the Career.Support Sites through Your mobile access provider. Therefore, You should check with Your provider to find out if the Career.Support Sites are available and the terms for these services for Your specific mobile devices.{"\n"}
					By using any downloadable application to enable Your use of the Career.Support Sites, You are explicitly confirming Your acceptance of the terms of the End User License Agreement associated with the Application provided at download or installation, or as may be 5updated from time to time.{"\n"}
				</Text>
				<Text style={styles.TextLeadHead}>
					16.	Term and Termination
				</Text>
				<Text style={styles.TextLead}>
					These Terms will remain in full force and effect while You are a User of any Career.Support Site. Career.Support reserves the right, at its sole discretion, to pursue all of its legal remedies, including but not limited to removal of Your User Content from the Career.Support Sites and immediate termination of Your registration with or ability to access the Career.Support Sites and/or any other services provided to You by Career.Support, upon any breach by You of these Terms or if Career.Support is unable to verify or authenticate any information You submit to a Career.Support Site registration. Even after You are no longer a User of the Career.Support Sites, certain provisions of these Terms will remain in effect, including Sections 1, 2, 5, 7 through 15, inclusive.
				</Text>
			</View>
		</View>
	)
}

export default TaC;

const styles = StyleSheet.create({
	Container: {
		paddingHorizontal: 15,
		backgroundColor: colors.white,
		paddingBottom: 10,
	},
	TextChild: {
		paddingVertical: 2,
		fontWeight: 'bold',
		fontFamily: 'DIN-Bold',
		fontSize: 12,
		color: colors.darkScale
	},
	TextHead: {
		paddingVertical: 2,
		fontSize: 16,
		fontFamily: 'DIN-Bold',
		fontWeight: 'bold',
		color: colors.dark
	},
	TextLead: {
		paddingVertical: 2,
		fontSize: 14,
		fontWeight: '800',
		fontFamily: 'DIN-Light',
		color: colors.darkSecond
	},
	TextLeadHead: {
		fontFamily: 'DIN-Medium',
		fontWeight: 'bold', 
		fontSize: 14, 
		color: colors.darkSecond
	}
})