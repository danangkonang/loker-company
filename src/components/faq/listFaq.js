import React, { useState } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native'

import colors from '../style/Colors'
import Ionicons from 'react-native-vector-icons/Ionicons'

const ListFaq = (props) => {
  const [state, setState] = useState({
    active:false,
    id:null
  });
  
  return (
    <View>
      <TouchableOpacity
        activeOpacity={.5}
        onPress={() => {
          onSetActive(props.id, state, setState);
        }}
        style={[styles.tabList, {
          backgroundColor: state.active && state.id && props.id ? colors.bluecyan : colors.white,
          borderBottomWidth: state.active && state.id && props.id ? 0 : 2, //true is value active
          borderBottomLeftRadius: state.active && state.id && props.id ? 0 : 6,
          borderBottomRightRadius: state.active && state.id && props.id ? 0 : 6
        }]}
      >
        <Text style={[styles.textMenu, { textAlign: 'left' }]}>{props.title}</Text>
        <View style={{ justifyContent: 'center', alignItems: 'center', }}>
          <Ionicons name={state.active && state.id && props.id ? 'ios-arrow-up' : 'ios-arrow-down'} color={colors.primary} size={20} />
        </View>
      </TouchableOpacity>
      {
        state.active && state.id && props.id &&
        <View style={styles.contentfaq}>
          <Text style={styles.textContent}>{props.description}</Text>
        </View>
      }
    </View>
  )
}

const onSetActive = (id, state, setState) => {
  setState({
    ...state,
    active:!state.active,
    id:id
  })
}

export default ListFaq;

const styles = StyleSheet.create({
  tabMenu: {
    backgroundColor: colors.gray,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 120,
    paddingVertical: 10,
    paddingHorizontal: 10
  },
  textMenu: {
    fontSize: 16,
    fontWeight: 'bold',
    color: colors.darkScale,
    width: '95%',
    textAlign: 'center'
  },
  tabList: {
    width: '100%',
    backgroundColor: colors.white,
    borderWidth: 2,
    borderColor: colors.gray,
    borderRadius: 6,
    marginTop: 10,
    paddingVertical: 9,
    flexDirection: 'row',
    paddingHorizontal: 14,
  },
  contentfaq: {
    borderTopWidth: 0,
    borderWidth: 2,
    borderColor: colors.gray,
    backgroundColor: colors.white,
    paddingHorizontal: 14,
    paddingVertical: 10,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6
  },
  textContent: {
    color: colors.grayScale,
    fontSize: 16,
    letterSpacing: 1,
    lineHeight: 22
  }
})