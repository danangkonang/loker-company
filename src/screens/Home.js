import React, { Component } from 'react'
import {
	View,
	Text,
	ScrollView,
	SafeAreaView,
	TouchableOpacity,
	StatusBar,
	Image
} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Fa from 'react-native-vector-icons/FontAwesome'
import MaterialIcn from 'react-native-vector-icons/MaterialCommunityIcons'
import Header from '../components/header'
import LinearGradient from 'react-native-linear-gradient'
import Menu from '../components/navigation/Menu'
import myStyles from '../components/style'
import colors from '../components/style/Colors'
import SwitchButton from 'switch-button-react-native'
import PureChart from 'react-native-pure-chart'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { getProfilInfo, getAnalytic, getWidget, getApplied, getIndustry } from '../_redux/_actions/employer'

var jwtDecode = require('jwt-decode');

class Profile extends Component {
	constructor() {
		super()
		this._isMounted = false
		this.state = {
			token: '',
			activeSwitch: 1,
			data: [],
			imageProfile: null,
			companyName: null,
			fullName: null,
			translation: null
		}
	}

	componentDidMount = async () => {
		// console.log('mounted')
		await this.getToken();

		let l = this.props.analystics.analytic_info.series[0].map((e, i) => {
			switch (i) {
				case 0: return { x: 'Friday', y: e }
				case 1: return { x: 'Saturday', y: e }
				case 2: return { x: 'Sunday', y: e }
				case 3: return { x: 'Monday', y: e }
				case 4: return { x: 'Tuesday', y: e }
				case 5: return { x: 'Wednesday', y: e }
				default: return { x: 'Thursday', y: e }
			}
		})
		let r = this.props.analystics.analytic_info.series[1].map((e, i) => {
			switch (i) {
				case 0: return { x: 'Friday', y: e }
				case 1: return { x: 'Saturday', y: e }
				case 2: return { x: 'Sunday', y: e }
				case 3: return { x: 'Monday', y: e }
				case 4: return { x: 'Tuesday', y: e }
				case 5: return { x: 'Wednesday', y: e }
				default: return { x: 'Thursday', y: e }
			}
		})

		this.setState({
			data: [
				{
					seriesName: 'series1',
					data: l,
					color: '#297AB1'
				},
				{
					seriesName: 'series2',
					data: r,
					color: 'red'
				}
			]
		})

	}

	getToken = async () => {
		let tok = await AsyncStorage.getItem('employerToken')
		await this.setState({ token: tok })
		await this.getData(tok)
	}

	getData = async (token) => {
		let decod = await jwtDecode(token)
		let id = decod.user_id
		await this.props.dispatch(getProfilInfo(token, id))
		await this.props.dispatch(getApplied(token))
		await this.props.dispatch(getWidget(token))
		await this.props.dispatch(getIndustry(token, this.props.profil.profil_info.user_employer_personals.employer.industry))
		await this.props.dispatch(getAnalytic(token))

		this.setState({
			imageProfile: this.props.profil.profil_info.user_employer_personals.employer.company_logo,
			companyName: this.props.profil.profil_info.user_employer_personals.employer.company_name,
			fullName: this.props.profil.profil_info.full_name,
			translation: this.props.profil.industry_info.translation
		})
	}

	render() {
		return (
			<LinearGradient
				start={
					{ x: 0, y: 0 }
				}
				end={
					{ x: 0, y: 1 }
				}
				colors={['#155396', '#155396', colors.white, colors.white]}
				style={{
					flex: 1
				}}
			>
				<StatusBar hidden={false} backgroundColor="#155396" barStyle="light-content" />
				<SafeAreaView style={myStyles.Flex1}>
					<View style={[myStyles.Flex1, { backgroundColor: colors.white }]}>
						<Header logo={true} icon="settings" iconSize={25} onpress={() => this.props.navigation.navigate('settings')} />
						<ScrollView style={myStyles.Flex1}>
							<LinearGradient start={{ x: 0, y: 1 }} end={{ x: 0, y: 0 }} colors={['#4782c3', '#155396']}>
								<View style={{ paddingTop: 10, height: 90, flexDirection: 'row', paddingHorizontal: 10, }}>
									<View style={{ paddingLeft: 10, width: 90, height: 80, }}>
										<TouchableOpacity onPress={() => navigate('account')}>
											<View style={{ width: 80, height: 80, borderRadius: 50, borderColor: '#fff', borderWidth: 4, backgroundColor: '#fff' }}>
												<Image
													style={{ width: '100%', height: '100%', borderRadius: 50, resizeMode: 'cover' }}
													source={{ uri: this.state.imageProfile ? this.state.imageProfile : '' }}
												/>
											</View>
										</TouchableOpacity>
									</View>
									<View style={{ flex: 1, paddingLeft: 10, justifyContent: 'center' }}>
										<Text numberOfLines={1} style={[myStyles.FontBold, { color: colors.white, fontSize: 18, marginBottom: 3, }]}>
											{this.state.companyName}
										</Text>
										<Text numberOfLines={2} style={[myStyles.FontMedium, { color: colors.white, marginBottom: 3, fontSize: 13, }]}>
											{this.state.translation ? this.state.translation : '-'}
										</Text>
										<Text numberOfLines={2} style={[myStyles.FontMedium, { color: colors.white, marginBottom: 3, fontSize: 13, }]}>
											{this.state.fullName}
										</Text>
									</View>
								</View>
							</LinearGradient>

							<View style={myStyles.Flex1}>
								<LinearGradient
									start={{ x: 0, y: 1 }}
									end={{ x: 0, y: 0 }}
									colors={['#4782c3', '#4782c3']}
									style={{ height: 140 }}>
								</LinearGradient>
							</View>

							<View style={{ backgroundColor: colors.white, marginTop: -120, marginHorizontal: 10, borderRadius: 7, borderWidth: 2, borderColor: colors.gray, paddingHorizontal: 10, paddingBottom: 5, marginBottom: 10, flexDirection: 'row', flexWrap: 'wrap', paddingTop: 10 }}>

								<View style={{ width: '50%', paddingVertical: 5, paddingHorizontal: 5, }}>
									<View style={{ backgroundColor: '#fff', height: 60, borderRadius: 5, borderWidth: 1.5, borderColor: colors.secondary, flexDirection: 'row' }}>
										<View style={{ flex: 1, justifyContent: 'center', paddingLeft: 10 }}>
											<Text style={{ color: colors.primary, fontWeight: 'bold' }}>Profil visit</Text>
											<Text style={{ color: colors.primary, fontWeight: 'bold', fontSize: 17 }}>{this.props.profil.widget_info.candidateView}</Text>
										</View>
										<View style={{ width: 50}}>
                                 <View style={{position:'absolute',bottom:-3,right:1}}>
											   <Ionicons name="md-home" color={colors.blueScale} size={50} />
                                 </View>
										</View>
									</View>
								</View>

								<View style={{ width: '50%', paddingVertical: 5, paddingHorizontal: 5, }}>
									<View style={{ backgroundColor: '#fff', height: 60, borderRadius: 5, borderWidth: 1.5, borderColor: colors.secondary, flexDirection: 'row' }}>
										<View style={{ flex: 1, justifyContent: 'center', paddingLeft: 10 }}>
											<Text style={{ color: colors.primary, fontWeight: 'bold' }}>Job aplicans</Text>
											<Text style={{ color: colors.primary, fontWeight: 'bold', fontSize: 17 }}>{this.props.profil.widget_info.candidateApply}</Text>
										</View>
										<View style={{ width: 50}}>
                                 <View style={{position:'absolute',bottom:0,right:1}}>
											   <Fa name="user" color={colors.blueScale} size={50} />
                                 </View>
										</View>
									</View>
								</View>

								<View style={{ width: '50%', paddingVertical: 5, paddingHorizontal: 5, }}>
									<View style={{ backgroundColor: '#fff', height: 60, borderRadius: 5, borderWidth: 1.5, borderColor: colors.secondary, flexDirection: 'row' }}>
										<View style={{ flex: 1, justifyContent: 'center', paddingLeft: 10 }}>
											<Text style={{ color: colors.primary, fontWeight: 'bold' }}>Ad clicks</Text>
											<Text style={{ color: colors.primary, fontWeight: 'bold', fontSize: 17 }}>{this.props.profil.widget_info.advertisementClick}</Text>
										</View>
										<View style={{ width: 50}}>
                                 <View style={{position:'absolute',bottom:-9,right:-7}}>
											   <MaterialIcn name="arrow-top-left-thick" color={colors.blueScale} size={55} />
                                 </View>
										</View>
									</View>
								</View>

								<View style={{ width: '50%', paddingVertical: 5, paddingHorizontal: 5, }}>
									<View style={{ backgroundColor: '#fff', height: 60, borderRadius: 5, borderWidth: 1.5, borderColor: colors.secondary, flexDirection: 'row' }}>
										<View style={{ flex: 1, justifyContent: 'center', paddingLeft: 10 }}>
											<Text style={{ color: colors.primary, fontWeight: 'bold' }}>Vacanci views</Text>
											<Text style={{ color: colors.primary, fontWeight: 'bold', fontSize: 17 }}>{this.props.profil.widget_info.jobVacancyClick}</Text>
										</View>
										<View style={{ width: 50}}>
                                 <View  style={{position:'absolute',bottom:-7,right:1}}>
											   <Ionicons name="md-eye" color={colors.blueScale} size={50} />
                                 </View>
										</View>
									</View>
								</View>
							</View>

							<View style={{ marginLeft: 20, paddingVertical: 10 }}>
								<Text style={{ color: colors.primary, fontWeight: 'bold' }}>Daily Statistics</Text>
							</View>

							<View style={{ width: '100%', paddingHorizontal: 15, marginVertical: 10 }}>
								<PureChart
									data={this.state.data}
									type='line'
								/>
							</View>

							<View style={{ alignItems: 'center', marginVertical: 30 }}>
								<SwitchButton
									onValueChange={(val) => this.setState({ activeSwitch: val })}
									text2='Ad click'
									text1='Vacancy views'
									switchWidth={250}
									switchHeight={44}
									switchdirection='rtl'
									switchBorderRadius={10}
									switchSpeedChange={500}
									switchBorderColor='#d4d4d4'
									switchBackgroundColor='#fff'
									btnBorderColor='#00a4b9'
									btnBackgroundColor={colors.primary}
									fontColor='#b1b1b1'
									activeFontColor='#fff'
								/>
							</View>

							<View style={{ marginBottom: 5 }}>
								<Text style={{ marginLeft: 10, fontWeight: 'bold', color: colors.primary, marginBottom: 10, paddingTop: 10 }}>Your top {this.props.profil.aplied_info.length} Vacancy</Text>
								{
									this.props.profil.aplied_info.map((item, idx) => (
										<View style={{ borderColor: colors.gray, borderRadius: 5, borderWidth: 1.5, marginHorizontal: 10, height: 70, flexDirection: 'row', marginBottom: 10 }} key={idx}>
											{/* <View style={{ width: 70, borderBottomLeftRadius: 5, borderTopLeftRadius: 5, alignItems: 'center', justifyContent: 'center' }}>
												<View style={{ backgroundColor: 'pink', width: 50, height: 50, borderRadius: 25 }}>

												</View>
											</View> */}
											<View style={{ flex: 1, justifyContent: 'center',marginLeft:10 }}>
												<Text style={{ fontWeight: 'bold', color: colors.darkScale }}>{item.jobSpecialization}</Text>
												<Text style={{ color: colors.darkScale }}>best magazine</Text>
											</View>
											<View style={{ width: 100, borderTopRightRadius: 5, borderBottomRightRadius: 5 }}>
												<View style={{ flex: 1, backgroundColor: colors.primary, borderTopRightRadius: 5, flexDirection: 'row', alignItems: 'center' }}>
													<Text style={{ paddingRight: 5, paddingLeft: 5, fontWeight: 'bold', color: colors.white }}>{item.totalApplied}</Text>
													<Text style={{ color: colors.white }}>Applicants</Text>
												</View>
												<View style={{ flex: 1, borderBottomRightRadius: 5, backgroundColor: colors.blueScale, flexDirection: 'row', alignItems: 'center' }}>
													<Text style={{ paddingRight: 5, paddingLeft: 5, fontWeight: 'bold', color: colors.primary }}>{item.totalApplied}</Text>
													<Text style={{ color: colors.primary }}>View</Text>
												</View>
											</View>
										</View>
									))
								}
							</View>

						</ScrollView>
						<Menu navigation={this.props.navigation} />

					</View>
				</SafeAreaView>
			</LinearGradient>
		)
	}

	detailEducation = () => {
		this.setState({
			modalEducation: true
		})
	}
	detailWorking = () => {
		this.setState({
			modalWorking: true
		})
	}
	detailOrganization = () => {
		this.setState({
			modalOrganization: true
		})
	}

	detailAchievements = () => {
		this.setState({
			modalAchievements: true
		})
	}
	detailPortfolio = () => {
		this.setState({
			modalPortfolio: true
		})
	}
}

const mapStateToProps = (state) => {
	return {
		profil: state.employer,
		analystics: state.employer
	}
}

export default connect(mapStateToProps)(Profile)

