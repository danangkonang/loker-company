import React, { useState, useEffect } from 'react'
import { View, SafeAreaView, StatusBar, TouchableWithoutFeedback, StyleSheet, FlatList } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { connect } from 'react-redux'
// Components
import Text from '../../components/Text'
import colors from '../../components/style/Colors'
import Menu from '../../components/navigation/Menu'
import myStyles from '../../components/style'
import Search from '../../components/header/Search'
import LayoutJobActive from './LayoutJobActive'
import LayoutJobInActive from './LayoutJobInActive'
import { getListJobActive, getListJobInActive, getDetailJob, getLockedCV } from '../../_redux/_actions/jobs'
import ModalLoading from '../../components/loading/ModalLoading'

const AppJobs = (props) => {

	const [keyword, setKeyword] = useState(null)
	const [keyTab, setKey] = useState(1)
	const [mounted, setMounted] = useState(true)

	useEffect(() => {
		if (mounted) {
			getJobs(props.employer.employer_data.token, props)
			// console.log(props)
		}

		return function cleanup() {
			// console.log('will mount')
			setMounted(false)
		}
	}, [])

	// Use reducer
	const { activeJob, inActiveJob, isLoading } = props.job

	return (
		<LinearGradient
			start={
				{ x: 0, y: 0 }
			}
			end={
				{ x: 0, y: 1 }
			}
			colors={['#155396', '#155396', colors.white, colors.white]}
			style={myStyles.Flex1}
		>
			<StatusBar barStyle="light-content" />
			<SafeAreaView style={myStyles.Flex1}>
				<View style={[myStyles.Flex1, { backgroundColor: colors.white }]}>
					<Search
						placeholder="Search job title or keyword"
						onChangeText={(text) => setKeyword(text)}
						value={keyword}
						onFilter={() => alert('Not ready')}
					/>

					<View style={{ height: 50 }}>
						<View style={myStyles.Flex1}>
							<View style={{
								backgroundColor: colors.cyan,
								flexDirection: 'row'
							}}>
								<TouchableWithoutFeedback onPress={() => setKey(1)}>
									<View style={[styles.TabButton, {
										backgroundColor: keyTab === 1 ? colors.cyan : colors.white,
									}]}>
										<View style={[styles.TabView, {
											backgroundColor: keyTab === 1 ? colors.white : colors.cyan,
											borderTopRightRadius: keyTab === 1 ? 15 : 0,
											borderBottomRightRadius: keyTab === 1 ? 0 : 15,
										}]}>
											<Text
												fontSize={16}
												color={colors.primary}
												text="Active"
												light={keyTab === 1 ? false : true}
											/>
										</View>
									</View>
								</TouchableWithoutFeedback>

								<TouchableWithoutFeedback onPress={() => setKey(2)}>
									<View style={[styles.TabButton, {
										backgroundColor: keyTab === 1 ? colors.white : colors.cyan,
									}]}>
										<View style={[styles.TabView, {
											backgroundColor: keyTab === 1 ? colors.cyan : colors.white,
											borderBottomLeftRadius: keyTab === 1 ? 15 : 0,
											borderTopLeftRadius: keyTab === 1 ? 0 : 15,
										}]}>
											<Text
												fontSize={16}
												color={colors.primary}
												text="Inactive"
												light={keyTab === 2 ? false : true}
											/>
										</View>
									</View>
								</TouchableWithoutFeedback>
							</View>
						</View>
					</View>

					{
						keyTab === 1 &&
						<FlatList
							data={activeJob.results}
							renderItem={(item) => renderActiveJobs(item, props)}
							contentContainerStyle={{ paddingHorizontal: 20, paddingTop: 15 }}
							keyExtractor={item => item.id.toString()}
						/>
					}

					{
						keyTab === 2 &&
						<FlatList
							data={inActiveJob.results}
							renderItem={(item) => renderInActiveJobs(item, props)}
							contentContainerStyle={{ paddingHorizontal: 20, paddingTop: 15 }}
							keyExtractor={item => item.id.toString()}
						/>
					}
				</View>
				<ModalLoading isVisible={isLoading} text="Please wait, on getting job detail ..." />
				<Menu
					navigation={props.navigation}
				/>
			</SafeAreaView>
		</LinearGradient>
	)
}

/**
 * @function getJobs
 * @param is token
 */
const getJobs = async (token, props) => {
	try {
		await props.dispatch(getListJobActive(token))
		await props.dispatch(getListJobInActive(token))
	} catch (error) {
		console.log(error)
	}
}

/**
 * @function getDetail
 * @param is id & props
 */
const getDetail = async (id, props) => {
	const { token } = props.employer.employer_data
	try {
		await props.dispatch(getDetailJob(id, token))
		await props.dispatch(getLockedCV(id, token))
		props.navigation.navigate('JobDetail', { jobsId: id })
	} catch (error) {
		console.log(error)
	}
}

/**
 * @function renderActiveJobs
 * @params is Data Item
 */
const renderActiveJobs = ({ item, index }, props) => {

	return (
		<LayoutJobActive
			title={item.detail.job_specialization.name}
			startDate={item.detail.period_from}
			endDate={item.detail.period_to}
			status={item.publishStatus}
			total_views={item.totalViewed}
			total_applicants={item.totalApplied}
			onViewMore={() => getDetail(item.id, props)}
			onEdit={() => props.navigation.navigate('EditJob')}
		/>
	)

}

/**
 * @function renderInActiveJobs
 * @params is Data Item
 */
const renderInActiveJobs = ({ item, index }, props) => {

	return (
		<LayoutJobInActive
			title={item.detail.job_specialization.name}
			status={item.publishStatus}
			total_views={item.totalViewed}
			total_applicants={item.totalApplied}
			onViewMore={() => getDetail(item.id, props)}
		/>
	)

}

// mapStateToProps
const mapStateToProps = (state) => {
	return {
		employer: state.employer,
		job: state.job
	}
}

export default connect(mapStateToProps)(AppJobs);

const styles = StyleSheet.create({
	TabButton: {
		width: '50%',
		alignItems: 'center',
		justifyContent: 'center',
	},
	TabView: {
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		paddingTop: 10,
		paddingBottom: 15,
		overflow: 'hidden',
	}
})