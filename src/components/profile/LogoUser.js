import React from 'react'
import { View, Image, StyleSheet } from 'react-native'

import myStyle from '../style'
import colors from '../style/Colors'

const LogoUser = (props) => {
	return (
		<View style={styles.userLogo}>
			<View style={styles.Logo}>
				<Image source={{
					uri: props.image != null ? props.image : 'https://career.support/candidate/img/photo_placeholder-candidate.8ef32882.jpg'
					}} 
					style={myStyle.ImgCover} />
			</View>
		</View>
	)
}

export default LogoUser

const styles = StyleSheet.create({
	userLogo: {
		alignItems: 'center',
		position: 'absolute',
		width: '100%',
		height: 100,
		top: 145,
		zIndex: 99,
	},
	Logo: {
		flex: 1,
		width: 100,
		backgroundColor: colors.white,
		borderRadius: 100/2,
		elevation: 1,
		borderColor: colors.white,
		borderWidth: 6,
		overflow: 'hidden'
	},
})