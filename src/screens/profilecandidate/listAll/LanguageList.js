import React, { Component } from 'react'
import {
	View,
	Text,
	ScrollView,
	Dimensions,
	SafeAreaView,
	StyleSheet,

} from 'react-native'
import Header from '../../../components/header/index'
import HeaderImage from '../../../components/profile/HeaderImage'
import Logo from '../../../components/profile/LogoUser'
import myStyles from '../../../components/style'
import colors from '../../../components/style/Colors'
import List from './List'
import { connect } from 'react-redux'
let AndroidHeight = Dimensions.get('window').height

class Education extends Component {

	state = {
		isDelete: false,
		scroll: true,
		showModal: false,
		achiveAddModal: false,
		achiveDetailModal: false,
		achiveEditModal: false,
		userToken: '',
		typeDelete: null,
		data: ['Indonesia', 'Sunda', 'Betawi', 'Ngapak', 'Melayu']
	}

	render() {
		return (
			<SafeAreaView style={myStyles.Flex1}>
				<View style={myStyles.Flex1}>
					<Header
						logo={true}
						onpress={() => this.props.navigation.goBack()}
					/>
					<ScrollView
						showsVerticalScrollIndicator={false}
						scrollEnabled={this.state.scroll}
					>
						<HeaderImage
							fullName="Nama penuh"
							major='tes'
							education="institution"
							isVerified={true}
						/>
						<Logo
							image='https://marein-re.com/public/uploads/photo/foto_pak_yan_copy.png'
						/>
						<View style={[myStyles.ProfileSection, { minHeight: AndroidHeight - 281 }]}>
							<View style={{ flex: 1, marginTop: 15, marginBottom: 5, borderRadius: 10, backgroundColor: colors.white, }}>
								<View style={styles.Header}>
									<Text style={[styles.Title, myStyles.FontBold]}>
										Language
									</Text>
								</View>
								<View style={[{ flex: 1, paddingVertical: 10, paddingHorizontal: 20, borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }]}>
									{
										this.props.candidate.language_list.map((item, idx) => (
											<List
												key={idx}
												Title={item.name}
											/>
										))
									}
								</View>
							</View>
						</View>
					</ScrollView>
				</View>
			</SafeAreaView>
		)
	}
}

const mapStateToProps = state => ({
	candidate: state.candidate
});

export default connect(mapStateToProps)(Education)

const styles = StyleSheet.create({
	Header: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		width: '100%',
		paddingVertical: 15,
		paddingHorizontal: 20,
		borderBottomWidth: .7,
		borderBottomColor: colors.gray,
	},
	Title: {
		fontSize: 18,
		color: colors.primary,
		fontWeight: 'bold',
	}
})
