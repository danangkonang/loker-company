import React from 'react'
import { View, TextInput, TouchableOpacity, StyleSheet } from 'react-native'
import colors from '../style/Colors'
import myStyles from '../style'
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'

const Password = (props) => {
	const {
		value,
		isError,
		password,
		placeholder,
		width,
		center,
		paddingHorizontal,
		borderColor,
		borderWidth,
		borderRadius,
		onClick,
		onChangeText,
		zIndex,
	} = props
	return (
		<View style={
			{
				width: width ? width : '100%',
				backgroundColor: colors.white,
				borderColor: borderColor ? borderColor : colors.gray,
				borderWidth: borderWidth ? borderWidth : 0,
				borderRadius: borderRadius ? borderRadius : 3,
				marginVertical: 5,
				elevation: 2,
				shadowColor: '#545454', 
				shadowOffset: { width: 1, height: 1 }, 
				shadowOpacity: .5,
			}
		}>
			<TextInput
				placeholder={placeholder ? placeholder : 'Career Support'}
				style={
					[{
						width: '100%',
						textAlign: center ? 'center' : 'left',
						paddingHorizontal: paddingHorizontal ? paddingHorizontal : 20,
						borderBottomLeftRadius: isError ? 1 : borderRadius,
						minHeight: 48,
						borderRadius: borderRadius ? borderRadius : 3,
						borderBottomRightRadius: isError ? 1 : borderRadius,
						zIndex: zIndex ? zIndex : 0,
						color: isError ? colors.red : colors.darkScale,
						backgroundColor: colors.white
					}, myStyles.FontMedium]
				}
				secureTextEntry={password ? true : false}
				onChangeText={onChangeText ? onChangeText : null}
				placeholderTextColor={colors.grayScale}
				value={value ? value : null}
				autoCapitalize='none'
			/>
			<TouchableOpacity style={styles.ShowButton}
			onPress={onClick ? onClick : () => alert('Define your props onClick!')}
			>
				<MdIcon name={password ? 'eye-off' : 'eye'} size={25} color={colors.darkScale} />
			</TouchableOpacity>
		</View>
	)
}

export default Password;

const styles = StyleSheet.create({
	ShowButton: {
		width: 50,
		right: 0,
		height: '100%',
		position: 'absolute',
		justifyContent: 'center',
		alignItems: 'center',
		padding: 13,
	},
	ShowIcon: {
		flex: 1,
		resizeMode: 'contain'
	}
})