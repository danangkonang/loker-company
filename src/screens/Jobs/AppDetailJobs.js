/**
 * SCREEN APPLOCKCV
 * CREATED BY RISMAN ABDILAH
 * DATE -> 13 DECEMBER 2019
 */

import React, { useState } from 'react'
import { View, SafeAreaView, StatusBar, Dimensions, StyleSheet, ScrollView, TextInput, TouchableWithoutFeedback, FlatList } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import { connect } from 'react-redux'
// Components
import AppHeader from '../../components/header'
import colors from '../../components/style/Colors'
import myStyles from '../../components/style'
import Text from '../../components/Text'
import LayoutCandidateList from './LayoutCandidateList'
import LayoutDetail from './LayoutDetail'
import Banner from '../../components/Layout/Banner'
import Logo from '../../components/Layout/Logo'
import { getUnLockedCV, getListAllCountry, getListAllEmployment, getListAllCategory, getListAllPosition, getProvinceById, getCityById, getJobSpecialization } from '../../_redux/_actions/jobs'
import { ActivityIndicator } from 'react-native'
import { getDataCandidate } from '../../_redux/_actions/candidate'
import ModalLoading from '../../components/loading/ModalLoading'
// Dimensions
const screenHeight = Dimensions.get('window').height

const AppDetailJobs = (props) => {
	const { goBack, navigate } = props.navigation

	const [keyTab, setKey] = useState(1)
	const [isLoading, setLoading] = useState(false)

	const { jobDetail, Locked, UnLocked } = props.job

	return (
		<LinearGradient
			start={
				{ x: 0, y: 0 }
			}
			end={
				{ x: 0, y: 1 }
			}
			colors={['#155396', '#155396', colors.white, colors.white]}
			style={myStyles.Flex1}
		>
			<SafeAreaView style={myStyles.Flex1}>
				<StatusBar barStyle="light-content" />
				<AppHeader
					logo={true}
					onpress={() => goBack()}
				/>
				<View style={[myStyles.Flex1, myStyles.bgWhite]}>
					<ScrollView showsVerticalScrollIndicator={false}>
						<Banner
							url={jobDetail.detail.company.company_banner}
							isEdit
							onPress={() => editJobVacancy(props, setLoading)}
						/>

						<Logo
							image={jobDetail.detail.company.company_logo}
						/>

						<LayoutDetail />

						<LinearGradient
							colors={[colors.white, colors.cyan, colors.cyan]}
							style={{ height: 180 }}
						>
							<View style={{ paddingLeft: 20, paddingRight: 15 }}>
								<Text
									fontSize={22}
									color={colors.primary}
									text="Candidates"
									bold
								/>
							</View>

							<View style={styles.Search}>
								<View style={[myStyles.Center, { paddingLeft: 10, height: 50 }]}>
									<MdIcon name="magnify" size={24} color={colors.darkScale} />
								</View>
								<TextInput
									placeholder="Search for applied candidates"
									placeholderTextColor={colors.secondary}
									style={styles.SearchInput}
								/>
							</View>

							<View style={[myStyles.Flex1, { justifyContent: 'flex-end' }]}>
								<View style={{ backgroundColor: colors.cyan, flexDirection: 'row' }}>
									<TouchableWithoutFeedback onPress={() => setKey(1)}>
										<View style={[styles.TabButton, {
											backgroundColor: keyTab === 1 ? colors.cyan : colors.white,
										}]}>
											<View style={[styles.TabView, {
												backgroundColor: keyTab === 1 ? colors.white : colors.cyan,
												borderTopRightRadius: keyTab === 1 ? 100 / 8 : 0,
												borderBottomRightRadius: keyTab === 1 ? 0 : 100 / 8,
											}]}>
												<Text
													fontSize={16}
													color={colors.primary}
													text="Locked"
													light={keyTab === 1 ? false : true}
												/>
											</View>
										</View>
									</TouchableWithoutFeedback>

									<TouchableWithoutFeedback onPress={async () => {
										await props.getUnLockedCV(jobDetail.id, props.employer.employer_data.token)
										setKey(2)
									}}>
										<View style={[styles.TabButton, {
											backgroundColor: keyTab === 1 ? colors.white : colors.cyan,
										}]}>
											<View style={[styles.TabView, {
												backgroundColor: keyTab === 1 ? colors.cyan : colors.white,
												borderBottomLeftRadius: keyTab === 1 ? 100 / 8 : 0,
												borderTopLeftRadius: keyTab === 1 ? 0 : 100 / 8,
											}]}>
												<Text
													fontSize={16}
													color={colors.primary}
													text="Unlocked"
													light={keyTab === 2 ? false : true}
												/>
											</View>
										</View>
									</TouchableWithoutFeedback>
								</View>
							</View>

						</LinearGradient>

						<View style={styles.List}>

							{
								keyTab === 1 &&
								<View style={{
									marginBottom: 15,
									justifyContent: 'center',
									alignItems: 'center'
								}}>
									{
										Locked.cv.count !== 0 &&
										<Text
											text={`There are ${Locked.cv.count ? Locked.cv.count : 0} new candidates!`}
											color={colors.primary}
											fontSize={16}
											light
										/>
									}
								</View>
							}

							{
								keyTab === 1 ?
									<FlatList
										data={Locked.cv.results}
										renderItem={(item) => renderListCandidate(item, keyTab, props, setLoading)}
										keyExtractor={item => item.id.toString()}
									/>
									:
									<FlatList
										data={UnLocked.cv.results}
										renderItem={(item) => renderListCandidate(item, keyTab, props, setLoading)}
										keyExtractor={item => item.id.toString()}
									/>
							}

						</View>

					</ScrollView>
				</View>
				<ModalLoading isVisible={isLoading} text="Please wait..." />
				<ModalLoading isVisible={props.candidate.isLoading} text="Please wait, on getting profile candidate ..." />
			</SafeAreaView>
		</LinearGradient>
	)
}

/**
 * @function editJobVacancy
 * @param is props
 */
const editJobVacancy = async (props, setLoading) => {
	const { detail } = props.job.jobDetail
	const { employer_data } = props.employer
	setLoading(true)
	try {
		// console.log(typeof employer_data.token)
		await props.getListAllCountry(employer_data.token)
		await props.getListAllEmployment(employer_data.token)
		await props.getListAllCategory(employer_data.token)
		await props.getListAllPosition(employer_data.token)
		await props.getProvinceById(detail.city[0].province.country.id, employer_data.token)
		await props.getCityById(detail.city[0].province.id, employer_data.token)
		await props.getJobSpecialization(detail.job_specialization.category.id, employer_data.token)
		setLoading(false)
		props.navigation.navigate('EditJob')
	} catch (error) {
		setLoading(false)
		console.log(error)
	}
	// console.log(props)
}

/**
 * @function renderListCandidate
 * @param is Data Candidate
 */
const renderListCandidate = ({ item }, keyTab, props, setLoading) => {
	const jobsId = props.navigation.state.params.jobsId
	const candidateId = item.cv.personalInfo.id
	const token = props.employer.employer_data.token
	
	return (
		<LayoutCandidateList
			keyTab={keyTab}
			name={
				`${item.cv.user.first_name} ${item.cv.user.last_name}`
			}
			graduateYear={item.cv.personalInfo.lastEducation ? item.cv.personalInfo.lastEducation.graduated_at : ''}
			appliedOn={item.appliedOn}
			gpa={item.cv.personalInfo.lastEducation ? item.cv.personalInfo.lastEducation.gpa : ''}
			major={item.cv.personalInfo.lastEducation ? item.cv.personalInfo.lastEducation.major : ''}
			image={item.cv.personalInfo.avatar}
			verified={item.cv.personalInfo.isVerified}
			onPressed={
				async () => {
					await getProfilecandidate(props, token, jobsId, candidateId)
				}
			}
		/>
	)
}

/**
 * @function getProfilecandidate for get data profile candidate
 * @param {*} props for props
 * @param {*} token for token employer
 * @param {*} jobsId fot id jobs
 * @param {*} candidateId for candidate id
 */
const getProfilecandidate = async (props, token, jobsId, candidateId) => {
	try {
		await props.getDataCandidate({ token, jobsId, candidateId })
		props.navigation.navigate('profil', { candidateId, jobsId })
	} catch (error) {
		console.log(error)
	}
}

// mapStateToProps
const mapStateToProps = (state) => {
	return {
		employer: state.employer,
		job: state.job,
		candidate: state.candidate
	}
}

// mapDispatchToProps
const mapDispatchToProps = dispatch => ({
	getDataCandidate: (data) => dispatch(getDataCandidate(data)),
	getUnLockedCV: (jobsId, employerToken) => dispatch(getUnLockedCV(jobsId, employerToken)),
	getListAllCountry: (employerToken) => dispatch(getListAllCountry(employerToken)),
	getListAllEmployment: (employerToken) => dispatch(getListAllEmployment(employerToken)),
	getListAllCategory: (employerToken) => dispatch(getListAllCategory(employerToken)),
	getListAllPosition: (employerToken) => dispatch(getListAllPosition(employerToken)),
	getProvinceById: (countryId, employerToken) => dispatch(getProvinceById(countryId, employerToken)),
	getCityById: (provinceId, employerToken) => dispatch(getCityById(provinceId, employerToken)),
	getJobSpecialization: (categoryId, employerToken) => dispatch(getJobSpecialization(categoryId, employerToken))
})

// Export 
export default connect(mapStateToProps, mapDispatchToProps)(AppDetailJobs);

/**
 * @StyleSheet
 */
const styles = StyleSheet.create({
	Search: {
		flexDirection: 'row',
		height: 50,
		borderRadius: 5,
		shadowColor: '#000',
		shadowOffset: {
			width: 1,
			height: 1,
		},
		shadowOpacity: .11255,
		marginTop: 20,
		marginBottom: 10,
		marginHorizontal: 15,
		backgroundColor: colors.white,
		overflow: 'hidden'
	},
	SearchInput: {
		color: colors.grayScale,
		fontFamily: 'DIN-Medium',
		fontSize: 15,
		paddingLeft: 10,
		flex: 1,
	},
	TabButton: {
		width: '50%',
		alignItems: 'center',
		justifyContent: 'center',
	},
	TabView: {
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		paddingTop: 10,
		paddingBottom: 15,
		overflow: 'hidden',
	},

	List: {
		minHeight: screenHeight * .6,
		backgroundColor: colors.white,
		paddingHorizontal: 18,
		paddingTop: 15
	}
})