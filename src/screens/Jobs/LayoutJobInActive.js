import React from 'react'
import { View, TouchableOpacity, StyleSheet } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome5'
// Components
import colors from '../../components/style/Colors'
import Text from '../../components/Text'
import myStyles from '../../components/style'

// FUNCTIONAL COMPONENTS
const LayoutJobInActive = (props) => {
	// Define variables props
	const { title, status, total_views, total_applicants, onViewMore } = props
	return (
		<View style={styles.Container}>
			<TouchableOpacity
				onPress={onViewMore}
				style={styles.SectionLeft}
			>
				<View style={{ flex: 1, paddingHorizontal: 5 }}>
					<Text 
						text={title}
						fontSize={15}
						color={colors.darkSecond}
						bold
						numberOfLines={2}
					/>
					<View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginTop: 20}}>
						<View style={myStyles.Flex1}>
							<Text 
								text="Status"
								color={colors.darkScale}
								fontSize={14}
								bold
							/>
							<Text 
								text={status}
								color={colors.darkScale}
								fontSize={13}
								light
							/>
						</View>

						<View style={myStyles.Flex1}>
							<Text 
								text="Views"
								color={colors.darkScale}
								fontSize={14}
								bold
							/>
							<Text 
								text={`${total_views}`}
								color={colors.darkScale}
								fontSize={13}
								light
							/>
						</View>

						<View style={{ width: '40%' }}>
							<Text 
								text="Total Applicants"
								color={colors.darkScale}
								fontSize={14}
								bold
							/>
							<Text 
								text={`${total_applicants}`}
								color={colors.darkScale}
								fontSize={13}
								light
							/>
						</View>
					</View>
				</View>
			</TouchableOpacity>

			<View style={styles.SectionRight}>
				<LinearGradient
					start={{ x: 0, y: 1 }}
					colors={['#4782c3', '#155396']}
					style={[myStyles.Flex1, myStyles.Center, { borderTopRightRadius: 10 }]}
				>
					<TouchableOpacity
						onPress={() => alert('Reply Jobs')}
						style={[myStyles.Flex1, myStyles.Center]}
					>
						<FontAwesome name="reply" size={25} color={colors.white} style={{ transform : [{
							rotate: '75deg'
						}]}} />
					</TouchableOpacity>
				</LinearGradient>

				<TouchableOpacity onPress={onViewMore} style={[myStyles.Flex1, myStyles.Center, { borderBottomRightRadius: 10}]}>
					<MdIcon name="chevron-right" size={25} color={colors.primary} />
				</TouchableOpacity>
			</View>
		</View>
	)
}

// Exporting Component
export default LayoutJobInActive;

/**
 * @StyleSheet
 */
const styles = StyleSheet.create({
	Container: {
		flexDirection: 'row',
		marginBottom: 15,
	},
	SectionLeft: {
		flex: 1,
		borderWidth: 1.5,
		borderTopLeftRadius: 10,
		borderBottomLeftRadius: 10,
		borderColor: colors.gray,
		paddingHorizontal: 10,
		paddingTop: 10,
		paddingBottom: 15,
		justifyContent: 'flex-start'
	},
	SectionRight: {
		width: '10%',
		backgroundColor: colors.cyan,
		borderTopRightRadius: 10,
		borderBottomRightRadius: 10,
	}
})
