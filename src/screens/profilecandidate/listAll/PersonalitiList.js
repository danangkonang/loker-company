import React, { Component } from 'react'
import {
	View,
	Text,
	ScrollView,
	Dimensions,
    SafeAreaView,
    StyleSheet,
    
} from 'react-native'
import Header from '../../../components/header/index'
import HeaderImage from '../../../components/profile/HeaderImage'
import Logo from '../../../components/profile/LogoUser'
import myStyles from '../../../components/style'
import colors from '../../../components/style/Colors'
import List from './List'
let AndroidHeight = Dimensions.get('window').height

class Education extends Component {

	state = {
		isDelete: false,
		scroll: true,
		showModal: false,
		achiveAddModal: false,
		achiveDetailModal: false,
		achiveEditModal: false,
		userToken: '',
        typeDelete: null,
        data:['php','html','java script','melukir','menari']
	}

	render() {
		return (
			<SafeAreaView style={myStyles.Flex1}>
				<View style={myStyles.Flex1}>
					<Header
						logo={true}
						onpress={() => this.props.navigation.goBack()}
					/>
					<ScrollView
						showsVerticalScrollIndicator={false}
						scrollEnabled={this.state.scroll}
					>
						<HeaderImage
							fullName="Nama penuh"
							major='tes'
                            education="institution"
							isVerified={true}
						/>
						<Logo
							image='https://marein-re.com/public/uploads/photo/foto_pak_yan_copy.png'
						/>
                        <View style={[myStyles.ProfileSection,{minHeight: AndroidHeight - 281}]}>
                            <View style={{flex: 1,marginTop: 15,marginBottom: 5,borderRadius: 10,backgroundColor: colors.white,}}>
                                <View style={styles.Header}>
                                    <Text style={[styles.Title, myStyles.FontBold]}>
                                        Personaliti
                                    </Text>
                                </View>

                                <View style={[{flex: 1,paddingVertical: 10,paddingHorizontal: 20,borderBottomLeftRadius:10,borderBottomRightRadius:10,backgroundColor:'#fff',flexDirection:'row',flexWrap:'wrap'}]}>
                                    {
                                        this.state.data.map((item,idx)=>(
                                            <View style={{backgroundColor:colors.secondary,margin:5,height:35,paddingHorizontal:10,alignItems:'center',justifyContent:'center',borderRadius:5}} key={idx}>
                                                <Text style={{color:colors.primary}}>{item}</Text>
                                            </View>
                                        ))
                                    }
                                </View>
                            </View>
                        </View>
					</ScrollView>
				</View>
			</SafeAreaView>
		)
	}
}

export default Education

const styles = StyleSheet.create({
    Header: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		width: '100%',
		paddingVertical: 15,
		paddingHorizontal: 20,
		borderBottomWidth: .7,
		borderBottomColor: colors.gray,
	},
	Title: {
		fontSize: 18,
		color: colors.primary,
		fontWeight: 'bold',
	}
})
