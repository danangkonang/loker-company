import React, { Component } from 'react'
import {
	View,
	Text,
	ScrollView,
	Dimensions,
	SafeAreaView,
	StyleSheet,
} from 'react-native'
import Header from '../../../components/header/index'
import HeaderImage from '../../../components/profile/HeaderImage'
import myStyles from '../../../components/style'
import colors from '../../../components/style/Colors'
import List from './List'
import { connect } from 'react-redux'
import { modifyMonth } from '../../../components/utils'
import LogoUser from '../../../components/profile/LogoUser'
let AndroidHeight = Dimensions.get('window').height
import DetailOrganization from '../../profilecandidate/OrganizationDetail'
class Education extends Component {

	state = {
		isDelete: false,
		scroll: true,
		showModal: false,
		achiveAddModal: false,
		achiveDetailModal: false,
		achiveEditModal: false,
		userToken: '',
		typeDelete: null,
		data: [1, 2, 3, 4, 5, 6],
		modalOrganization: false,
		detailOrg:{},
	}
	detailOrganization =async (data) => {
		await this.setState({detailOrg:data})
		this.setState({ modalOrganization: true })
		// console.log(this.state.detailOrg)
	}

	render() {
		return (
			<SafeAreaView style={myStyles.Flex1}>
				<View style={myStyles.Flex1}>
					<Header
						logo={true}
						onpress={() => this.props.navigation.goBack()}
					/>
					<ScrollView
						showsVerticalScrollIndicator={false}
						scrollEnabled={this.state.scroll}
					>
						<HeaderImage
							fullName={
								this.props.candidate.data_candidate.cv_unlocked ? this.props.candidate.data_candidate.user.full_name : "[Name Hidden]"
							}
							major={
								this.props.candidate.data_candidate.last_education.major.label
							}
							education={
								this.props.candidate.data_candidate.last_education.school.label
							}
						/>
						<LogoUser image={this.props.candidate.data_candidate.avatar} />
						<View style={[myStyles.ProfileSection, { minHeight: AndroidHeight - 281 }]}>
							<View style={{ flex: 1, marginTop: 15, marginBottom: 5, borderRadius: 10, backgroundColor: colors.white, }}>
								<View style={styles.Header}>
									<Text style={[styles.Title, myStyles.FontBold]}>
										Organization
									</Text>
								</View>
								<View style={[{ flex: 1, paddingVertical: 10, paddingHorizontal: 20, borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }]}>
									{
										this.props.candidate.organization_list.map((item, idx) => {
											let started = new Date(item.start_date).getFullYear()
											let ended = new Date(item.end_date).getFullYear()
											return (
												<List
													key={idx}
													click={()=>this.detailOrganization(item)}
													Logo={true}
													Title={item.name}
													TextDesc={item.title}
													Years={`${modifyMonth(item.start_date)} ${started}  -  ${item.end_date ? `${modifyMonth(item.start_date)} ${ended}` : 'Now'}`}
												/>
											)
										})
									}
								</View>
							</View>
						</View>
					</ScrollView>

					<DetailOrganization
						data={this.state.detailOrg}
						isVisible={this.state.modalOrganization}
						onClose={() => this.setState({ modalOrganization: false })}
					/>
				</View>
			</SafeAreaView>
		)
	}
}

const mapStateToProps = state => ({
	candidate: state.candidate
})

export default connect(mapStateToProps)(Education);

const styles = StyleSheet.create({
	Header: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		width: '100%',
		paddingVertical: 15,
		paddingHorizontal: 20,
		borderBottomWidth: .7,
		borderBottomColor: colors.gray,
	},
	Title: {
		fontSize: 18,
		color: colors.primary,
		fontWeight: 'bold',
	}
})
