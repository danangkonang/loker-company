import { createStore, combineReducers, applyMiddleware } from 'redux'
import { logger, promise } from './Middleware'
import employer from '../_reducers/employer'
import candidate from '../_reducers/candidate'
import job from '../_reducers/jobs'

const reducers = combineReducers({
	employer,
	candidate,
	job
})

const store = createStore(
	reducers,
	applyMiddleware(promise, logger)
)

export default store;