import React from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native'

import Ionicons from 'react-native-vector-icons/Ionicons'
import myStyles from '../style/index'
import colors from '../style/Colors'

const SectionFaq = (props) => (
  <TouchableOpacity
    activeOpacity={0.6}
    onPress={props.onPressed}
    style={{ width: '33.3%' }}
  >
    <View style={styles.cardCat}>
      <View style={styles.circleIcon}>
        <Ionicons name="ios-paper-plane" color={colors.white} size={30} />
      </View>
      <Text style={[myStyles.FontBold, styles.textCat]}>{props.name}</Text>
    </View>
  </TouchableOpacity>
)

export default SectionFaq;

const styles = StyleSheet.create({
  cardCat: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
    borderWidth: 2,
    borderColor: colors.gray,
    borderRadius: 6,
    margin: 2,
    paddingVertical: 15,
    paddingHorizontal: 10,
  },
  textCat: {
    marginTop: 5,
    color: colors.darkScale,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  circleIcon: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#4782c3',
    borderRadius: 100,
    width: 50,
    height: 50,
    paddingVertical: 7,
    paddingHorizontal: 10
  },
})