
// Reducer State
const initialState = {
	activeJob: {},
	inActiveJob: {},
	jobDetail: {},
	Locked: {},
	UnLocked: {},
	ListCountry: [],
	ListEmployment: [],
	ListCategory: [],
	ListPosition: [],
	province: [],
	city: [],
	JobSpecialization: [],
	isLoading: false
}

// Job Reducer
const jobs = (state = initialState, action) => {
	switch (action.type) {
		// JOB ACTIVE
		case 'GET_LIST_JOB_ACTIVE_FULFILLED':
			return {
				...state,
				activeJob: action.payload.data
			}
		// JOB INACTIVE
		case 'GET_LIST_JOB_INACTIVE_FULFILLED':
			return {
				...state,
				inActiveJob: action.payload.data
			}
		// JOB DETAIL
		case 'GET_DETAIL_JOB_PENDING':
			return {
				...state,
				isLoading: true
			}
			break;
		case 'GET_DETAIL_JOB_FULFILLED':
			return {
				...state,
				jobDetail: action.payload.data,
				isLoading: false
			}
		// LOCKED CV
		case 'GET_LOCKED_CV_FULFILLED':
			return {
				...state,
				Locked: action.payload.data
			}
		// UNLOCKED CV
		case 'GET_UNLOCKED_CV_FULFILLED':
			return {
				...state,
				UnLocked: action.payload.data
			}
		// COUNTRY
		case 'GET_LIST_COUNTRY_FULFILLED':
			return {
				...state,
				ListCountry: action.payload.data
			}
		// EMPLOYMENT
		case 'GET_LIST_EMPLOYMENT_FULFILLED':
			return {
				...state,
				ListEmployment: action.payload.data
			}
		// CATEGORY
		case 'GET_LIST_CATEGORY_FULFILLED':
			return {
				...state,
				ListCategory: action.payload.data
			}
		// POSITION
		case 'GET_LIST_POSITION_FULFILLED':
			return {
				...state,
				ListPosition: action.payload.data
			}
		// PROVINCE BY COUNTRYID
		case 'GET_PROVINCE_BYID_FULFILLED':
			return {
				...state,
				province: action.payload.data
			}
		// CITY BY PROVINCEID
		case 'GET_CITY_BYID_FULFILLED':
			return {
				...state,
				city: action.payload.data
			}
		// JOB_SPECIALIZATION BY CATEGORYID
		case 'GET_JOB_SPECIALIZATION_FULFILLED':
			return {
				...state,
				JobSpecialization: action.payload.data
			}
		// UPDATE JOB DETAIL
		case 'UPDATE_DETAIL_JOB_FULFILLED':
			console.log(action.payload)
			return {
				...state,
				isLoading: false
			}
		/** LOUGOUT */
		case 'LOGOUT_EMPLOYER':
			return {
				...initialState
			}
			break;
		default:
			return state;
			break;
	}
}

// Export reducer
export default jobs