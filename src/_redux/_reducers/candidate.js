const initialState = {
  isLoading: false,
  isError: false,
  data_candidate: {},
  education_list: [],
  workexp_list: [],
  organization_list: [],
  skill_list: [],
  language_list: [],
  achivements_list: [],
  portfolio_list: [],
  personality_list: [],
  education_detail: {}
}

const candidate = (state = initialState, action) => {
  switch (action.type) {
    case "GET_DATA_CANDIDATE_PENDING":
      return {
        ...state,
        isLoading: true,
        isError: false,
      }
      break;
    case "GET_DATA_CANDIDATE_FULFILLED":
      let education_list = action.payload.data.user_educations.map(data => {
        // console.log(data)
        return {
          id: data.id,
          name: data.school.label,
          title: data.major.label,
          logo: data.school.logo,
          nim:data.gpa,
          major:data.major.major,
          degree:data.degree.degree,
          start_date: data.start_year,
          end_date: data.end_year,
          verified: data.schoolVerified
        }
      })

      let workexp_list = action.payload.data.work_experiences.map(data => {
        // console.log(data)
        return {
          id: data.id,
          name: data.job_name,
          title: data.company_name,
          desc:data.desc,
          logo: data.company.company_logo,
          start_date: data.start_work,
          end_date: data.end_work
        }
      })

      let organization_list = action.payload.data.associations.map(data => {
        // console.log(data)
        return {
          id: data.id,
          name: data.organization,
          title: data.role,
          description:data.description,
          start_date: data.since,
          end_date: data.until
        }
      })

      let skill_list = action.payload.data.skills.map(data => {
        return {
          id: data.id,
          name: data.skill.label,
        }
      })

      let language_list = action.payload.data.languages.map(data => {
        return {
          id: data.id,
          name: data.language.label,
        }
      })

      let achivements_list = action.payload.data.achievements.map(data => {
        // console.log(data)
        return {
          id: data.id,
          name: data.achievement,
          title: data.associate,
          role_description:data.role_description,
          start_date: data.received,
          end_date: null
        }
      })

      let portfolio_list = action.payload.data.portfolios.map(data => {
        // console.log(data)
        return {
          id: data.id,
          name: data.portfolio_name,
          title: data.portfolio_url,
          portfolio_description:data.portfolio_description,
          start_date: data.portfolio_date,
          end_date: null
        }
      })


      // let personality_list = action.payload.data.map(data => {
      //   return {

      //   }
      // })

      return {
        ...state,
        isLoading: false,
        isError: false,
        data_candidate: action.payload.data,
        education_list,
        workexp_list,
        organization_list,
        skill_list,
        language_list,
        achivements_list,
        portfolio_list,
        //personality_list
      }
      break;
    case "GET_DATA_CANDIDATE_REJECTED":
      return {
        ...state,
        isLoading: false,
        isError: true,
        data_candidate: action.payload.data
      }
      break;
    case 'LOGOUT_EMPLOYER':
      return {
        ...initialState
      }
      break;
    case "DETAIL_EDUCATION":
      return {
        ...state,
        education_detail: {
          ...state.education_detail,
          end_date: action.payload.end_date,
          id: action.payload.id,
          logo: action.payload.logo,
          name: action.payload.name,
          start_date: action.payload.start_date,
          title: action.payload.title
        }
      }
      break;
    default:
      return state;
      break;
  }
}

export default candidate;