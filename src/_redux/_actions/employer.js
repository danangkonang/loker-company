import axios from 'axios';
import { CS_PRODUCTION, CS_KEY } from 'react-native-dotenv'

export const changeTermsCondition = () => {
	return {
		type: 'CHANGE_TERMS_CONDITIONS'
	}
}

export const registrasion = (data) => {
	return {
		type: 'REGISTRATION_STEP_ONE',
		payload: data
	}
}

export const getCountry = (token) => {
	return {
		type: 'GET_COUNTRY',
		payload: axios.get(`${CS_KEY}region/country/`, {
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json',
        Authorization: `JWT ${token}`
      }
    })
	}
}

export const getProfilInfo = (token,id) => {
	return {
		type: 'GET_PROFIL_INFO',
		payload: axios.get(`${CS_KEY}company/user/profile/${id}/`, {
      headers: {
        Authorization: `JWT ${token}`
      }
    })
	}
}

export const getAnalytic = (token) => {
	return {
		type: 'GET_ANALYSTIC_INFO',
		payload: axios.get(`${CS_KEY}company/dashboard/analytic/`, {
      headers: {
        Authorization: `JWT ${token}`
      }
    })
	}
}

export const getWidget = (token) => {
	return {
		type: 'GET_WIDGET_INFO',
		payload: axios.get(`${CS_KEY}company/dashboard/widget/`, {
      headers: {
        Authorization: `JWT ${token}`
      }
    })
	}
}

export const getApplied = (token) => {
	return {
		type: 'GET_APLIED_INFO',
		payload: axios.get(`${CS_KEY}company/dashboard/applied/`, {
      headers: {
        Authorization: `JWT ${token}`
      }
    })
	}
}

export const getIndustry = (token,id) => {
	return {
		type: 'GET_INDUSTRY_INFO',
		payload: axios.get(`${CS_KEY}company/profile/industry/${id}/`, {
      headers: {
        Authorization: `JWT ${token}`
      }
    })
	}
}

/**
 * 
 * @function employerAuthentication
 * @param {email, password} for dataLogin 
 */
export const employerAuthentication = (email, password) => {
  const dataLogin = JSON.stringify({
    username: email,
    password: password
  })
  return {
    type: 'EMPLOYER_AUTHENTICATION',
    payload: axios.post(`${CS_KEY}/authentication/employer/`, dataLogin, {
      headers: {
        'Content-Type': 'application/json'
      }
    })
  }
}

/**
 * 
 * @param {token} is token now
 */
export const refreshTokenEmployer = token => {
  return {
    type: 'REFRESH_EMPLOYER_TOKEN',
    payload: axios.post(`${CS_KEY}/authentication/employer/refresh/`, {
      token: token
    })
  }
}

/** @func logoutEmployer */
export const logoutEmployer = () => ({
  type: 'LOGOUT_EMPLOYER'
})

/**
 * @func forgotPassword for action handleclick sendLink in screen forgotpassword
 * @param {email} email for data email
 */
export const forgotPassword = (email) => {
  const data = JSON.stringify({
    username: email
  })
  return {
    type: 'SEND_EMAIL_FORGOT_PASSWORD',
    payload: axios.post(`${CS_KEY}/authentication/employer/password/reset/`, data, {
      headers: {
        'Content-Type': 'application/json'
      }
    })
  }
}

/** 
 * @func changePassword for action change password
 * @param {oldPassword, newPassword, token}  for pass data to endpoint 
*/
export const changePassword = data => {
  const { oldPassword, newPassword, token } = data

  const body = JSON.stringify({
    old_password: oldPassword,
    new_password: newPassword,
  });

  return {
    type: 'CHANGE_PASSWORD_EMPLOYER',
    payload: axios.post(`${CS_KEY}/authentication/employer/password/change/`, body, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        Authorization: `JWT ${token}`
      }
    })
  }
}

/**
 * @func getCategoryFaq for get data category FAQ
 * @param token for authorization endpoint
 */
export const getCategoryFaq = token => ({
  type: 'GET_CATEGORY_FAQ',
  payload: axios.get(`${CS_KEY}/company/faq/category/`, {
    headers: {
      Accept: 'application/json',
      'Content-type': 'application/json',
      Authorization: `JWT ${token}`
    }
  })
})
/**
 * @func getCategoryFaq for get data category FAQ
 * @param {id, token} for get spesific cat faq and authorization endpoint
 */
export const getFaqByCategory = (data) => ({
  type: 'GET_BY_CATEGORY_FAQ',
  payload: axios.get(`${CS_KEY}/company/faq/bycategory/${data.dataId}`, {
    headers: {
      Accept: 'application/json',
      'Content-type': 'application/json',
      Authorization: `JWT ${data.employerToken}`
    }
  })
})
