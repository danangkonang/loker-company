/**
		@function valid_email with Regular Expression to Validate Email
	*/
export const valid_email = (value) => {
	var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	return regex.test(value)
}

/** 
		@function valid_numberPhone with Regular Expression to Validate Phone Number
	*/
export const valid_numberPhone = (value) => {
	var RegEx = /^(^\+62\s?|^0)(\d{3,4}-?){2}\d{3,4}$/g;

	return RegEx.test(value)
}