/**
 * @function formatDate
 * @param is dateString
 * @param {"2019-06-10"} prevDate 
 * @param {1 / 2 / null} type 
 * @returns FORMAT " DATE MONTH YEAR / MONTH YEAR / YEAR "
 */
export const formatDate = (prevDate, type) => {
	
	// SET FORMAT MONTH
	var month = [
		'Jan',
		'Feb',
		'Mar',
		'Apr',
		'May',
		'Jun',
		'Jul',
		'Agu',
		'Sep',
		'Oct',
		'Nov',
		'Dec'
	]

	// SET VALUE
	var date = new Date(prevDate).getDate()
	var _month = new Date(prevDate).getMonth()
	var _year = new Date(prevDate).getFullYear()

	var fullDate = date + ' ' + month[_month] + ' ' + _year
	var mediumDate = month[_month] + ' ' + _year

	if (type === 1) {
		return _year
	} else if (type === 2) {
		return mediumDate
	} else {
		return fullDate
	}

}

/**
 * @function setDate
 * @param is date
 * @returns example 2019-09-02
 */
export const setDate = (date) => {
	// Set To Date Format
	var currentDate = new Date(date)
	// GET MONTH
	const getMonth = currentDate.getMonth() + 1
	const JumStringMonth = getMonth.toString().length
	const month = JumStringMonth == 1 ? `0${getMonth}` : `${getMonth}`
	// GET DATE
	const getDate = currentDate.getDate()
	const JumStringDate = getDate.toString().length
	const getdate = JumStringDate == 1 ? `0${getDate}` : `${getDate}`
	// GET YEAR
	const getYear = currentDate.getFullYear().toString()
	// GET FULL DATE
	let i = getYear + '-' + (month) + '-' + getdate
	// RETURN FULLDATE
	return i
}