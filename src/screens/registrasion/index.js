import React, { Component } from 'react'
import {
	View,
	Text,
	StatusBar,
	SafeAreaView,
	ScrollView,
	StyleSheet,
	Dimensions,
	Image,
	TouchableOpacity
} from 'react-native'
import HeaderStep from '../../components/header/HeaderStep'
import colors from '../../components/style/Colors'
import myStyles from '../../components/style'
const ScreenWidth = Dimensions.get('window').width
const ScreenHeight = Dimensions.get('window').height
import CustomTextInput from '../../components/customTextInput'
import DefaultText from '../../components/customTextInput/DefaulText'
import ErrorText from '../../components/customTextInput/Error'
import CheckBox from '../../components/CheckBox'
import Button from '../../components/button'
import LinearGradient from 'react-native-linear-gradient'
import { connect } from 'react-redux'
import { registrasion, changeTermsCondition } from '../../_redux/_actions/employer'
class Registration extends Component {
	state = {
		email: '',
		phone: '',
		errorEmail: false,
		msgErrEmail: '',
		errorPhone: false,
		msgErrPhone: '',
		isSave: false
	}
	
	componentDidMount(){
		this.props.dispatch(changeTermsCondition())
	}
	render() {
		const {
			email,
			errorEmail,
			msgErrEmail,
			phone,
			errorPhone,
			msgErrPhone,
		} = this.state
		const {
			tac
		} = this.props.tac_registration
		return (
			<LinearGradient
				start={
					{ x: 0, y: 0 }
				}
				end={
					{ x: 0, y: 1 }
				}
				colors={[colors.white, colors.white, '#155396', '#155396']}
				style={myStyles.Flex1}
			>
				<SafeAreaView style={myStyles.Flex1}>
						<StatusBar backgroundColor={colors.white} barStyle="dark-content" />
						<ScrollView>
							<View style={{ backgroundColor: '#fff', height: ScreenHeight * 0.96 }}>
								<HeaderStep
									colorNonActive={colors.cyan}
									colorActive={colors.primary}
									step={[true, false, false, false, false, false, false]}
								/>
								<Text style={[styles.TextHeader, myStyles.textCenter, myStyles.FontBold]}>
									Find the right talent in
 			 				</Text>
								<View style={styles.Logo}>
									<Image source={require('../../assets/img/logo_cs.png')} style={myStyles.ImgContain} />
								</View>
								<View style={[styles.Form, { flex: 1 }]}>
									<CustomTextInput
										width='70%'
										marginBottom={8}
										placeholder="Email Address"
										type='emailAddress'
										keyboardType='email-address'
										borderRadius={3}
										shadowRadius={6}
										onchange={(email) => {
											let data = email.toLowerCase()
											this.setState({
												email: data,
												errorEmail: false
											})
										}}
										value={email}
										isError={errorEmail}
										zIndex={20}
									/>
									{
										errorEmail &&
										<ErrorText
											text={msgErrEmail}
										/>
									}
									<View style={{ flexDirection: 'row' }}>
										<DefaultText
											width='13.5%'
											placeholder="Phone Number xxxx"
											keyboardType='phone-pad'
											borderBottomRightRadius={0}
											shadowRadius={6}
											borderRadius={3}
											value='+62'
											disabled={true}
											paddingHorizontal={6}
											center
											zIndex={20}
										/>
										<CustomTextInput
											width='54%'
											placeholder="Phone Number xxxx"
											keyboardType='phone-pad'
											borderRadius={3}
											maxLength={12}
											shadowRadius={6}
											value={phone}
											isError={errorPhone}
											zIndex={20}
											onchange={(phone) => this.setState({ phone, errorPhone: false })}
											marginLeft={10}
										/>

									</View>
									{
										errorPhone &&
										<ErrorText
											text={msgErrPhone}
											width="50%"
											marginLeft={-70}
										/>
									}
									<View style={styles.TermsCondition}>
										<CheckBox
											value={tac}
											onValueChange={() => this.props.dispatch(changeTermsCondition())}
										/>
										<View style={myStyles.FlexColumn}>
											<Text style={[myStyles.FontLight, { color: colors.primary }]}>I have read and agree to the</Text>
											<TouchableOpacity onPress={() => this.props.navigation.navigate('tac')}>
												<Text style={[myStyles.FontBold, {
													color: colors.primary,
													textDecorationLine: 'underline',
													textDecorationColor: colors.primary,
													fontSize: 16,
												}]}>Terms & Condition</Text>
											</TouchableOpacity>
										</View>
									</View>
									<Button
										isSave={this.state.isSave}
										disabled={this.state.isSave}
										marginVertical={15}
										width='40%'
										backgroundColor={colors.primary}
										color={colors.white}
										text="Register"
										onclick={() => this.registrasi()}
									/>
								</View>
								<LinearGradient start={{ x: 0, y: 1 }} end={{ x: 0, y: 0 }} colors={['#155396', '#4782c3']} style={{ backgroundColor: colors.primary, height: '15%', position: 'relative', bottom: 0 }}>
									<TouchableOpacity onPress={() => this.props.navigation.navigate('login')} style={styles.Link}>
										<Text style={[myStyles.FontBold, styles.LinkText]}>
											Already have an account?
 			 						</Text>
									</TouchableOpacity>
								</LinearGradient>
							</View>
						</ScrollView>
				</SafeAreaView>
			</LinearGradient>
		)
	}

	registrasi = () => {
		if (this.state.email == '') {
			this.setState({
				errorEmail: true,
				msgErrEmail: 'Please enter your email'
			})
		} else {
			if (this.state.phone == '') {
				this.setState({
					errorPhone: true,
					msgErrPhone: 'Please enter your phone number'
				})
			} else {
				this.save()
			}
		}
	}

	save = async () => {
		this.setState({ isSave: true })
		const { email, phone } = this.state
		const data = JSON.stringify({
			email: email,
			phone: '+62' + phone
		})
		try {
			const res = await fetch(`http://128.199.224.138:8080/api/v1/authentication/employer/register/`, {
				method: 'POST',
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				body: data
			})
			const response = await res.json()
			console.log(response)
			if (response.en == `User with email ${email} already exists.`) {
				this.setState({ errorEmail: true, msgErrEmail: `Email already exists.` })
			} else if (response.en == `User with phone ${phone} already exists.`) {
				this.setState({ errorPhone: true, msgErrPhone: `Phone already exists.` })
			} else {
				this.props.dispatch(registrasion({
					email: response.email,
					phone: response.phone,
					token: response.token,
				}))
				this.props.navigation.navigate('otp')
			}
			this.setState({ isSave: false })
		} catch (e) {
			alert('Error')
			console.log(e)
			this.setState({ isSave: false })
		}
	}
}

const mapStateToProps = (state) => {
	return {
		tac_registration: state.employer.tac_registration
	}
}

export default connect(mapStateToProps)(Registration)

const styles = StyleSheet.create({
	Registration: {
		width: ScreenWidth,
		backgroundColor: colors.white,
		justifyContent: 'space-between',
		height: '100%'
	},
	TextHeader: {
		width: '100%',
		color: colors.primary,
		fontSize: 24,
		marginTop: '10%'
	},
	Logo: {
		width: '100%',
		height: 50,
		paddingHorizontal: '15%',
		marginBottom: 10,
		marginTop: 2
	},
	Form: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		paddingTop: '5%',
		width: '100%',
	},
	TermsCondition: {
		flexDirection: 'row',
		width: '80%',
		height: 50,
		justifyContent: 'center',
		paddingHorizontal: 10,
		alignItems: 'center',
		marginVertical: '2%'
	},
	Footer: {
		height: '15%',
		backgroundColor: colors.primary,
		width: '100%',
	},
	Link: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	LinkText: {
		color: colors.white,
		fontSize: 15,
	}
})