import React from 'react'
import {
	View,
	Text,
	Image,
	StyleSheet,
	TouchableOpacity
} from 'react-native'
import IonIcon from 'react-native-vector-icons/Ionicons'
import colors from '../../../components/style/Colors'
import myStyles from '../../../components/style'
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'

const List = (props) => {
	const {
		Logo,
		SourceImage,
		Title,
		TextDesc,
		Years,
		YearsOnly,
		click,
		verified
	} = props
	return (
		<TouchableOpacity activeOpacity={0.8} style={{ borderRadius: 10, borderColor: colors.gray, borderWidth: 2, flexDirection: 'row', marginBottom: 8 }} onPress={click}>
			{
				Logo &&
				<View style={{ width: 60, padding: 7 }}>
					<Image
						source={{ uri: SourceImage ? SourceImage : 'https://career.support/candidate/img/photo_placeholder-school.df476433.jpg' }}
						style={{
							flex: 1,
							resizeMode: 'contain',
							borderRadius: 5
						}}
					/>
				</View>
			}
			<View style={{ flex: 1, paddingVertical: 5, paddingLeft: 15, paddingRight: 10 }}>
				<Text style={[styles.Title, myStyles.FontBold]}>
					{
						Title ? `${Title}` : 'Teknik Informatika'
					}
					{
						verified === true &&
						<>&nbsp;
							<MdIcon name="check-decagram" size={15} color={colors.pink} />
						</>
					}
				</Text>
				<Text numberOfLines={5} style={[styles.TextDesc, myStyles.FontMedium]}>
					{
						TextDesc ? TextDesc : 'level'
					}
				</Text>
				{
					Years &&
					<Text style={[styles.Date, myStyles.FontLight]}>
						{YearsOnly ? Years.split('-')[0] : Years}
					</Text>
				}
			</View>
			<View style={{ width: 30, backgroundColor: colors.cyan, borderTopRightRadius: 5, borderBottomRightRadius: 5, alignItems: 'center', justifyContent: 'center' }}>
				<IonIcon name="ios-arrow-forward" size={17} color={colors.primary} />
			</View>
		</TouchableOpacity>
	)
}

export default List

const styles = StyleSheet.create({
	Container: {
		marginVertical: 6,
		marginHorizontal: 5,
		borderRadius: 6,
		elevation: 2,
		shadowColor: colors.dark,
		shadowOpacity: .5,
		shadowOffset: {
			width: 1,
			height: 1,
		},
		shadowRadius: 5,
		flexDirection: 'row',
		overflow: 'hidden',
		backgroundColor: colors.white,
	},
	Checkbox: {
		width: '15%',
		height: '100%',
		justifyContent: 'center',
		alignItems: 'center',
	},
	Logo: {
		width: '25%',
		height: 50,
		borderRadius: 5,
		backgroundColor: 'white',
		marginVertical: 1,
		paddingTop: 10,
	},
	Desc: {
		flex: 1,
		paddingLeft: 15,
		paddingRight: 5,
		paddingVertical: 5,
		justifyContent: 'space-around',
		marginVertical: 1,
	},
	Title: {
		fontWeight: 'bold',
		fontSize: 16,
		color: colors.darkScale,
	},
	TextDesc: {
		fontWeight: '900',
		color: colors.darkScale,
		marginTop: 2
	},
	Date: {
		color: colors.darkScale,
		marginTop: 2
	},
	EditButton: {
		width: 40,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: colors.cyan,
	}
})