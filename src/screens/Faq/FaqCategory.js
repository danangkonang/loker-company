import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  SafeAreaView,
  Dimensions,
  ActivityIndicator,
  Image,
} from 'react-native'
import { connect } from 'react-redux'
// Components
import Header from '../../components/header'
import HeaderSearch from '../../components/faq/header'
import myStyles from '../../components/style'
import colors from '../../components/style/Colors'
import { getFaqByCategory, getCategoryFaq } from '../../_redux/_actions/employer'
import ListFaq from '../../components/faq/listFaq'
import TextComponent from "../../components/Text/index";

const ScreenWidth = Dimensions.get('window').width;

class FaqCategory extends Component {

  constructor(props) {
    super(props)
    this.state = {
      dataId: null,
      employerToken: null
    }
  }

  async componentDidMount() {
    const { dataId, employerToken } = this.props.navigation.state.params
    await this.setState({ dataId: dataId, employerToken: employerToken })
    await this.getListFaq();
  }

  getListFaq = async () => {
    try {
      const { dataId, employerToken } = this.state
      await this.props.getFaqByCategory({ dataId, employerToken })
    } catch (error) {
      console.log(error)
    }
  }

  render() {
    const { isActiveTablist } = this.state
    return (
      <SafeAreaView style={myStyles.Flex1}>
        <View style={myStyles.Flex1}>
          <Header
            logo={true}
            onpress={() => this.props.navigation.goBack()}
          />
          <ScrollView>
            <HeaderSearch />
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <View style={{ flexDirection: 'row' }}>
                {
                  this.props.employer.faq.category.map((data, i) => {
                    const { dataId } = this.state
                    return (
                      <TouchableOpacity
                        key={i}
                        style={[styles.tabMenu,
                        { backgroundColor: data.id === dataId ? colors.graygelap : colors.graymuda } // condition of active background
                        ]}
                        onPress={async () => {
                          await this.setState({ dataId: data.id })
                          await this.getListFaq()
                        }}
                      >
                        <Text style={styles.textMenu} numberOfLines={1}>{data.name}</Text>
                      </TouchableOpacity>
                    )
                  })
                }
              </View>
            </ScrollView>
            {
              !this.props.employer.loading &&
                this.props.employer.faq.list_faq.length === 0 ?
                <>
                  <View style={[myStyles.Center, { height: 200, backgroundColor:'white', paddingTop: 50 }]}>
                    <Image
                      style={{
                        width: "50%",
                        resizeMode: 'contain',
                      }}
                      source={require('../../components/icons/icon_error_connection.png')} />
                  </View>
                  <View style={[myStyles.Center, { marginTop: 20 }]}>
                    <TextComponent
                      fontSize={22}
                      color={colors.primary}
                      text="Im sorry :("
                      bold
                    />
                    <TextComponent
                      fontSize={22}
                      color={colors.primary}
                      text="Faq not found"
                      bold
                    />
                  </View>
                </>
                :
                <View style={{ padding: '5%', backgroundColor: 'white', flex: 1 }}>
                  {
                    this.props.employer.loading ?
                      <ActivityIndicator size="large" color={colors.primary} />
                      :
                      this.props.employer.faq.list_faq.map((data, i) => {
                        return (
                          <ListFaq
                            key={i}
                            isActiveTablist={isActiveTablist}
                            title={data.title}
                            description={data.description}
                            id={data.id}
                          />
                        )
                      })
                  }
                </View>
            }

          </ScrollView>
        </View>
      </SafeAreaView>
    )
  }
}

const mapStateToProps = state => ({
  employer: state.employer
})

const mapDispatchToProps = dispatch => ({
  getFaqByCategory: (data) => dispatch(getFaqByCategory(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(FaqCategory);

const styles = StyleSheet.create({
  tabMenu: {
    backgroundColor: colors.gray,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 120,
    paddingVertical: 10,
    paddingHorizontal: 10
  },
  textMenu: {
    fontSize: 16,
    fontWeight: 'bold',
    color: colors.darkScale,
    width: '95%',
    textAlign: 'center'
  },
  tabList: {
    width: '100%',
    backgroundColor: colors.white,
    borderWidth: 2,
    borderColor: colors.gray,
    borderRadius: 6,
    marginTop: 10,
    paddingVertical: 9,
    flexDirection: 'row',
    paddingHorizontal: 14,
  },
  contentfaq: {
    borderTopWidth: 0,
    borderWidth: 2,
    borderColor: colors.gray,
    backgroundColor: colors.white,
    paddingHorizontal: 14,
    paddingVertical: 10,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6
  },
  textContent: {
    color: colors.grayScale,
    fontSize: 16,
    letterSpacing: 1,
    lineHeight: 22
  }
})