import colors from '../style/Colors'

const styles = {
	Image: {
		width: '50%',
		height: '30%',
	},
	Title: {
		width: '100%',
		textAlign: 'center',
		color: colors.darkScale,
		fontSize: 20,
		fontWeight: 'bold',
		fontFamily: 'DIN-Bold'
	},
	Text: {
		width: '100%',
		textAlign: 'center',
		color: colors.grayScale,
		fontSize: 17,
		fontFamily: 'DIN-Medium'
	}
}

export default styles