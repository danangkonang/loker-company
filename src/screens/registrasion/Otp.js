import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, SafeAreaView, StatusBar } from 'react-native'
import { connect } from 'react-redux'
import { registrasion } from '../../_redux/_actions/employer'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import color from '../../components/style/Colors'
import myStyles from '../../components/style'
import HeaderStep from '../../components/header/HeaderStep'
import CountDown from 'react-native-countdown-component'
import Button from '../../components/button'
import colors from '../../components/style/Colors'

class Otp extends React.Component {
	state = {
		code: "",
		errOtp: false,
		btnTimer: true,
		token: '',
		token2: '',
		dataOtp: '123456',
		minut: 60 * 1,
		timer: 10
	}

	validasi = async (code) => {
		if (code.length == 6) {
			try {
				const res = await fetch(`http://128.199.224.138:8080/api/v1/authentication/employer/register/token/`, {
					method: 'POST',
					headers: {
						'Accept': 'application/json',
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						token: this.props.employer_registration.token,
						otp: code
					})
				})
				const response = await res.json()
				// console.log('awal',response)
				let respon = JSON.parse(response)
				// console.log('ahir',respon)
				this.props.dispatch(registrasion({
					email: respon.email,
					phone: respon.phone,
					token: respon.token,
				}))
				this.props.navigation.navigate('password')
			} catch (e) {
				console.log(e)
			}
		}
	}

	resend = async () => {
		try {
			const response = await fetch(`http://128.199.224.138:8080/api/v1/authentication/employer/register/`, {
				method: 'POST',
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: this.props.employer_registration.email,
					phone: this.props.employer_registration.phone
				})
			})
			let data = await response.json()
			// console.log(data)
			// 
			this.props.dispatch(registrasion({
				email: data.email,
				phone: data.phone,
				token: data.token,
			}))
			this.setState({
				minut: 60 * 1, btnTimer: true,
				errOtp: false
			})
		} catch (e) {
			console.log(e)
		}
	}

	render() {
		return (
			<SafeAreaView>
				<StatusBar hidden={true} />
				<ScrollView style={{}} contentContainerStyle={{ paddingTop: 5 }}>
					<HeaderStep
						colorNonActive={color.cyan}
						colorActive={color.primary}
						step={[true, true, false, false, false, false, false]}
					/>
					<View style={{ flex: 1, paddingVertical: 35, paddingHorizontal: 15 }}>
						<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
							<Text style={[myStyles.FontBold, styles.Title]}>We have sent an OTP {"\n"} to your email</Text>
						</View>
					</View>
					<View style={{ flex: 1, paddingVertical: 35, paddingHorizontal: 15 }}>
						<View style={{ flex: 1, backgroundColor: '#FFF', alignItems: 'center', justifyContent: 'center' }}>
							<Text style={[myStyles.FontLight, styles.Label]}>
								Enter your OTP to continue. {"\n"}
								Click resend if you don't get the OTP in 1 minute,{"\n"}
								but don't forget to also check your spam folder
                            </Text>
						</View>
					</View>
					{/* <Text style={{fontSize: 18, color: '#000'}}>
						{this.state.timer}
 					</Text> */}
					<View style={styles.container}>
						<OTPInputView
							style={{ width: '80%', height: 100 }}
							pinCount={6}
							codeInputFieldStyle={styles.underlineStyleBase}
							codeInputHighlightStyle={styles.underlineStyleHighLighted}
							onCodeFilled={(code) => this.validasi(code)}
						/>
					</View>
					{
						this.state.errOtp == true ?
							<View style={{ alignItems: 'center' }}>
								<Text style={{ color: 'red', fontWeight: '400' }}>Your OTP is wrong please check your email</Text>
							</View>
							:
							<View />
					}
					<View style={{ flex: 1, paddingVertical: 35, paddingHorizontal: 15 }}>
						<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
							{
								this.state.btnTimer ?
									<TouchableOpacity
										activeOpacity={.5}
										disabled={true}
										style={styles.btnTrue}>
										{
											<View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
												<View style={{ flex: 1, justifyContent: 'center' }}>
													<Text style={[myStyles.FontMedium, { color: '#aaa', fontWeight: '700', alignItems: 'center', paddingLeft: 8 }]}>Resend in</Text>
												</View>
												<View style={{ justifyContent: 'flex-end' }}>
													<CountDown
														size={14}
														until={this.state.minut} //detik
														onFinish={() => this.setState({ btnTimer: false })}
														digitStyle={{ backgroundColor: 'tranparant' }}
														digitTxtStyle={{ color: color.darkScale }}
														timeLabelStyle={{ color: 'red' }}
														separatorStyle={{ color: '#aaa' }}
														timeToShow={['M', 'S']}
														timeLabels={{ m: null, s: null }}
														showSeparator
													/>
												</View>
											</View>
										}
									</TouchableOpacity>
									:
									<Button
										marginVertical={15}
										width='50%'
										height={45}
										backgroundColor={colors.primary}
										color={colors.white}
										text="Resend"
										onclick={() => this.resend()}
									/>
							}
						</View>
					</View>
				</ScrollView>
			</SafeAreaView>
		);
	}
}


const mapStateToProps = (state) => {
	return {
		employer_registration: state.employer.employer_registration
	}
}

export default connect(mapStateToProps)(Otp)

const styles = StyleSheet.create({
	btnTrue: { backgroundColor: "#fff", borderRadius: 7, height: 45, borderColor: '#aaa', borderWidth: 2, width: '50%', alignItems: 'center', justifyContent: 'center' },
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
	borderStyleBase: {
		width: 30,
		height: 45
	},
	borderStyleHighLighted: {
		borderColor: "#03DAC6",
	},
	underlineStyleBase: {
		width: 33,
		height: 45,
		borderWidth: 2,
		borderBottomWidth: 1,
		borderRadius: 5
	},
	underlineStyleHighLighted: {
		borderColor: color.primary
	},
	Title: {
		fontSize: 20,
		fontWeight: 'bold',
		color: color.primary,
		textAlign: 'center'
	},
	Label: {
		fontSize: 15,
		color: color.darkScale,
		textAlign: 'center'
	}
});
