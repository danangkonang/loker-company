import React from 'react'
import { Text } from 'react-native'
import colors from '../style/Colors'

const TextComponent = (props) => {
	const { color, fontSize, bold, light, text, bottom, left, right, numberOfLines, align } = props
	return (
		<Text style={{
			marginBottom: bottom ? 3 : 0,
			marginLeft: left ? left : .5,
			marginRight: right ? right : .5,
			color: color ? color : colors.darkScale,
			fontSize: fontSize ? fontSize : 14,
			textAlign: align ? align : 'left',
			fontFamily: bold ?
				'DIN-Bold'
				:
				light ?
					'DIN-Light'
					:
					'DIN-Medium'
		}}
			numberOfLines={numberOfLines ? numberOfLines : 200}
		>
			{
				text ? text : 'Text'
			}
		</Text>
	)
}

export default TextComponent;