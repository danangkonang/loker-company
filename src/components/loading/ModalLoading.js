import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import Text from '../Text';
import colors from '../style/Colors';
import myStyles from '../style';

const ModalLoading = (props) => (
  props.isVisible &&
  <View style={[myStyles.Center, {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: 'rgba(0,0,0,0.7)',
    zIndex:100000
  }]}>
    <ActivityIndicator size="large" color={colors.white} style={{ marginBottom: 10 }} />
    <Text
      text={props.text}
      color={colors.white}
      bold
      fontSize={18}
    />
  </View>
)

export default ModalLoading;