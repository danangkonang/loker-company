/**
 * @function formatRupiah
 * @param is number & prefix
 */
export const formatRupiah = (number, prefix) => {
	var number_string = number.replace(/[^,\d]/g, '').toString(),
		split = number_string.split(','),
		sisa = (split[0].length - 2) % 3,
		rupiah = split[0].substr(0, sisa),
		ribuan = split[0].substr(sisa).match(/\d{3}/gi);

	// tambahkan titik jika yang di input sudah menjadi angka ribuan
	if (ribuan) {
		separator = sisa ? '.' : '';
		rupiah += separator + ribuan.join('.');
	}

	rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
	// console.log(rupiah)
	return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

/**
 * @function modifyMonth
 * @param is Date
 * @returns Month 3 Characters
 */
export const modifyMonth = (currentDate) => {
	let month = [
		'Jan',
		'Feb',
		'Mar',
		'Apr',
		'May',
		'Jun',
		'Jul',
		'Agu',
		'Sep',
		'Oct',
		'Nov',
		'Dec'
	]
	// Value of Date
	let _month = new Date(currentDate).getMonth()

	// SET NEW VALUE
	let newMonth = month[_month]
	return newMonth
}

/**
 * @function modifyDateFormat
 * @param is Date 
 */
export const modifyDateFormat = (currentDate) => {

	// SET FORMAT
	var month = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'
	]

	// VALUE DATE
	var date = new Date(currentDate).getDate()
	var _month = new Date(currentDate).getMonth()
	var _year = new Date(currentDate).getFullYear()

	var newDate = date + ' ' + month[_month] + ' ' + _year
	return newDate

}

export const validURL = (link) => {
	var regex = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
	if (regex.test(link)) {
		return true;
	}
	return false
}

export const elevationShadowStyle = (elevation) => {
	return {
		elevation,
		shadowColor: 'black',
		shadowOffset: { width: 0, height: 0.5 * elevation },
		shadowOpacity: 0.3,
		shadowRadius: 0.8 * elevation
	}
}