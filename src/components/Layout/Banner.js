import React from 'react'
import { View, TouchableOpacity, Image, StyleSheet } from 'react-native'
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import IonIcon from 'react-native-vector-icons/Ionicons'
// Components
import colors from '../style/Colors'
import myStyles from '../style'

const Banner = (props) => {

	const { isEdit, onPress, url } = props

	return (
		<View style={{ height: 120, backgroundColor: 'rgba(0,0,0,0.9)' }}>
			<TouchableOpacity 
				onPress={onPress}
				style={[styles.Button, { backgroundColor : isEdit ? colors.primary : colors.redSecond}]}>
					{
						isEdit ?
						<MdIcon name="pencil" size={23} color={colors.white} />
						:
						<IonIcon name="md-trash" size={23} color={colors.white} />
					}
			</TouchableOpacity>
			<Image 
				source={url ? { uri: url } : require('../../assets/img/banner-image.png')} 
				style={[myStyles.ImgCover, {opacity: .5}]} />
		</View>
	)
}

export default Banner;

const styles = StyleSheet.create({
	Button: {
		position: 'absolute',
		right: 15,
		top: 15,
		width: 40,
		height: 40,
		borderRadius: 100/2,
		justifyContent: 'center',
		alignItems: 'center',
		zIndex: 999
	}
})