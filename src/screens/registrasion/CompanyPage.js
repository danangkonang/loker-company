import React, { Component } from 'react'
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator
} from 'react-native'
import HeaderStep from '../../components/header/HeaderStep'
import colors from '../../components/style/Colors'
import Button from '../../components/button'
import myStyles from '../../components/style'
import ImagePicker from 'react-native-image-crop-picker'
import AsyncStorage from '@react-native-community/async-storage'
import ErrorText from '../../components/customTextInput/Error'
let{height} = Dimensions.get('window')
export default class CompanyPage extends Component {
    state = {
        image: null,
        dataImage:null,
        token:'',
        loader:false,
        errImage:false,
        errImageMsg:''
    }

    componentDidMount = async () => {
       this.getToken()
    }

    getToken = async () => {
        let tok = await AsyncStorage.getItem('employerToken')
        await this.setState({ token: tok })
        console.log(this.state.token)
    }

    saveImage=()=>{
        let image = this.state.dataImage
        if(image==null){
            this.setState({errImage:true,errImageMsg:'Please select image'})
        }else{
            this.setState({loader:true})
            let i = image.path
            let imageName =i.split('/')
            let name = imageName[7]
            const avatar = {
                uri:image.path,
                type:'image/jpeg',
                name:name
            }
            // console.log(avatar)
            this.uploadFile(avatar)
        }
    }

    modalLoading=()=>{
        if(this.state.loader==true){
            return(
                <View style={{backgroundColor:'#000',opacity:.5,position:'absolute',height:height,width:'100%',zIndex:99,alignItems:'center',justifyContent:'center'}}>
                    <ActivityIndicator size="large" color="#fff" />
                </View>
            )
        }
    }

    uploadFile = async (image)=>{
        try{
            const formData = new FormData()
            formData.append('company_banner', image)
            const up = await fetch(`http://128.199.224.138:8080/api/v1/company/registration/employer_image/`,{
                method: 'PATCH',
                headers:{ 
                    'Content-Type': 'multipart/form-data',
                    'Authorization':`JWT ${this.state.token}` 
                },
                body:formData
            })
            let u = await up.json()
            this.props.navigation.navigate('companyAddress')
            this.setState({loader:false})
            console.log(u)
        }catch(err){
            console.log(err)
        }
    }

    pickSingleWhiteGalery(cropit, circular = false, mediaType) {
    ImagePicker.openPicker({
        width: 300,
        height: 300,
        // cropping: true,
        includeBase64: true,
    }).then(image => {
        this.setState({
            image: image.path,
            dataImage:image
        })
    })
    .catch(e => {
        if (e) {
            this.upload()
            this.setState({
                errFile: true,
                msgErrFile: 'format (jpg, jpeg, and png)',
            })
        }
    })
}


render() {
    return (
        <ScrollView>
            <View style={{height:height-25}}>
                <HeaderStep
                    colorNonActive={colors.cyan}
                    colorActive={colors.primary}
                    step={[true, true, true, true, true, false, false]}
                />
                {this.modalLoading()}

                <View style={{ alignItems: 'center', marginTop: 20 }}>
                    <Text style={[myStyles.FontBold, { color: colors.primary, fontSize: 25, textAlign: 'center' }]}>Personalize {"\n"} company page</Text>
                </View>

                <View style={{ alignItems: 'center', paddingTop: 10,backgroundColor:'#fff' }}>
                    <Image source={require('../../components/icons/image_registration_banner.png')} style={{height:120,resizeMode:'contain'}}/>
                </View>
                <View style={{ alignItems: 'center', marginVertical: 25 }}>
                    <Text style={[myStyles.FontLight, { color: colors.darkScale }]}>Upload a banner for your company to </Text>
                    <Text style={[myStyles.FontMedium, { color: colors.darkScale, fontWeight: 'bold' }]}>display in your profile & vacancies</Text>
                </View>

                <Text style={{ marginLeft: 50, marginRight: 50,color:colors.secondary }}>
                    {this.state.image}
                </Text>

                <View style={{ alignItems: 'center' }}>
                    <Button
                        marginVertical={15}
                        width='40%'
                        backgroundColor={colors.primary}
                        color={colors.white}
                        text="Upload image"
                        onclick={() => this.pickSingleWhiteGalery(true)}
                    />
                </View>
                <View style={{alignItems:'center',marginTop:-10}}>
                {
                    this.state.errImage == true ?
                        <ErrorText
                            width='40%'
                            text={this.state.errImageMsg}
                        /> : null
                }
                </View>

                <View style={{ alignItems: 'center' }}>
                    <Button
                        marginVertical={15}
                        width='40%'
                        backgroundColor={colors.primary}
                        color={colors.white}
                        text="Next"
                        onclick={this.saveImage}
                        // onclick={() => this.props.navigation.navigate('companyAddress')}
                    />
                </View>
                
                
                <View style={{ alignItems: 'center', paddingTop: 30 }}>
                    <TouchableOpacity>
                        <Text style={{ fontWeight: 'bold', color: colors.primary }}>Cancel registrasion</Text>
                    </TouchableOpacity>
                </View>
            </View>

        </ScrollView>
        )
    }
}


