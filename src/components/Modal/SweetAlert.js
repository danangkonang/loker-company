import React, { Component } from 'react'
import { View, Text, Image, StyleSheet, Dimensions } from 'react-native'
// Components
import Button from '../button'
// Styles
import myStyles from '../style'
import colors from '../style/Colors'
// Dimensions
let AndroidWidth = Dimensions.get('window').width
let AndroidHeight = Dimensions.get('window').height

class SweetAlert extends Component {
	render() {
		let { type, text, onYes, onCancel, visible, isAlert, onOke, textProceed, textCancel, textOke } = this.props
		let icon;
		if (type === 1) {
			// Delele Account
			icon = require('../icons/icon_warning_delete_account.png')
		} else if (type === 2) {
			// Remove Item
			icon = require('../icons/icon_warning_item.png')
		} else if (type === 3) {
			icon = require('../icons/icon_warning_logout.png')
		} else if (type === 4) {
			icon = require('../icons/icon_pop-up-notifications.png')
		} else if (type === 5) {
			icon = require('../icons/icon_pop-up_report.png')
		} else if (type === 6) {
			icon = require('../icons/icon_pop-up_email-sent.png')
		} else if (type === 7) {
			icon = require('../icons/icon_wrong.png')
		} else if (type === 8) {
			icon = require('../icons/icon_checklist.png')
		} else if (type === 9) [
			icon = require('../icons/icon_error_data.png')
		]

		if (visible) {

			return (
				<View style={styles.Container}>
					<View style={styles.Card}>
						<View style={styles.Image}>
							<Image
								source={icon}
								style={myStyles.ImgContain}
							/>
						</View>
						<View style={styles.Text}>
							<Text style={[styles.WarningText, myStyles.FontMedium]}>
								{
									text ? text : 'Warning'
								}
							</Text>
						</View>
						{
							isAlert ?
								<View style={styles.Button}>
									<Button
										width='50%'
										backgroundColor={colors.primary}
										color={colors.white}
										marginVertical={6}
										text={textOke ? textOke : "Oke"}
										onclick={onOke}
									/>
								</View>
								:
								<View style={styles.Button}>
									<Button
										width='50%'
										backgroundColor={colors.primary}
										color={colors.white}
										marginVertical={6}
										text={textProceed ? textProceed : "Yes, proceed"}
										onclick={onYes}
									/>
									<Button
										width='50%'
										backgroundColor={colors.white}
										color={colors.primary}
										borderColor={colors.primary}
										marginVertical={6}
										text={textCancel ? textCancel : "No, cancel"}
										onclick={onCancel}
										linearBackground={false}
									/>
								</View>
						}
					</View>
				</View>
			)

		}

		return false
	}
}

export default SweetAlert;

const styles = StyleSheet.create({
	Container: {
		position: 'absolute',
		width: AndroidWidth,
		height: AndroidHeight,
		backgroundColor: "rgba(0,0,0,0.4)",
		justifyContent: 'center',
		alignItems: 'center',
		paddingHorizontal: '10%',
	},
	Card: {
		width: '100%',
		maxHeight: AndroidHeight * .55,
		backgroundColor: colors.white,
		borderRadius: 10,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 3,
		},
		shadowOpacity: 0.29,
		shadowRadius: 4.65,
		elevation: 7,
		alignItems: 'center',
		overflow: 'hidden'
	},
	Image: {
		paddingVertical: 5,
		width: '90%',
		height: AndroidHeight * 1 / 5,
		marginTop: 15
	},
	Text: {
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		paddingVertical: 20,
	},
	WarningText: {
		textAlign: 'center',
		fontSize: 15,
		color: colors.grayScale
	},
	Button: {
		width: '100%',
		alignItems: 'center',
		justifyContent: 'center',
		paddingVertical: 10,
	}
})