import React, { useState, useEffect } from 'react'
import { View, ScrollView, Text, Image, TouchableOpacity, StyleSheet, BackHandler, Dimensions } from 'react-native'
import { connect } from 'react-redux'
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'

// Components
import CustomTextInput from '../../components/customTextInput'
import ErrorText from '../../components/customTextInput/Error'
import Button from '../../components/button'
import SweetAlert from '../../components/Modal/SweetAlert'
// Styles
import myStyles from '../../components/style'
import colors from '../../components/style/Colors'
import LinearGradient from 'react-native-linear-gradient'
import { forgotPassword } from '../../_redux/_actions/employer'
import { valid_email } from '../../components/validation'

const ScreenHeight = Dimensions.get('window').height

const ForgotPassword = (props) => {

  const [state, setState] = useState({
    email: '',
    errorEmail: false,
    errorEmailText: '',
    showModal: false,
    typeIconAlert: null
  })

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', () => {
      props.navigation.goBack()
      return true
    })
  }, [])

  const {
    email,
    errorEmail,
    errorEmailText,
    showModal,
    typeIconAlert
  } = state

  return (
    <View style={myStyles.Flex1}>
      <ScrollView contentContainerStyle={styles.Container} showsVerticalScrollIndicator={false}>
        <Text style={[myStyles.textCenter, myStyles.FontBold, styles.TextHeader]}>
          Welcome back to
        </Text>
        <View style={styles.Logo}>
          <Image
            source={require('../../assets/img/logo_cs.png')}
            style={myStyles.ImgContain}
          />
        </View>
        <View style={styles.Form}>
          <Text style={[styles.FormDesc, myStyles.FontLight]}>
            Enter your email below and {"\n"} we will send a link to your email {"\n"} to reset your password
          </Text>
          <CustomTextInput
            width='70%'
            marginBottom={8}
            placeholder="rismandev@gmail.com"
            type='emailAddress'
            keyboardType='email-address'
            borderRadius={3}
            shadowRadius={6}
            onchange={(email) => {
              let data = email.toLowerCase()
              setState({ ...state, email: data, errorEmail: false })
            }}
            value={email}
            zIndex={20}
          />
          {
            errorEmail &&
            <ErrorText
              text={errorEmailText}
            />
          }
          <View style={styles.BottomButton}>
            <Button
              marginVertical={20}
              width='30%'
              backgroundColor={colors.primary}
              color={colors.white}
              text="Send link"
              onclick={() => validationEmail({ email, setState, state }) && sendEmail({ props, state, setState })}
            />
            <TouchableOpacity
              onPress={() => props.navigation.goBack()}
            >
              <View style={styles.LinkBack}>
                <MdIcon name="chevron-left" size={25} color={colors.primary} />
                <Text style={[styles.LinkBackText, myStyles.FontBold]}>
                  Back to login
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <LinearGradient start={{ x: 0, y: 1 }} end={{ x: 0, y: 0 }} colors={['#155396', '#4782c3']} style={styles.Footer} />
      </ScrollView>
      <SweetAlert
        visible={showModal}
        type={typeIconAlert}
        text={`Please check your email \n to create new password`}
        isAlert={true}
        onOke={() => _onClickOke({ setState, state, props })}
      />
      <SweetAlert
        visible={showModal}
        type={typeIconAlert}
        text={`Error send email \n please check connection or try again latter!`}
        isAlert={true}
        onOke={() => _onClickOke({ setState, state, props })}
      />
    </View>
  )
}

/**
 * @func sendEmail() for sendlink forgot password
 * @param data state, setState, & props
 */
const sendEmail = async (data) => {
  const { props, state, setState } = data
  try {
    const send = await props.sendEmailForgotPass(state.email)
    if (send) {
      setState({
        ...state,
        showModal: true,
        typeIconAlert: 6
      })
    }
  } catch (error) {
    if (error.response.data.en) {
      setState({
        ...state,
        errorEmail: true,
        errorEmailText: error.response.data.en
      })
    } else {
      setState({
        ...state,
        showModal: true,
        typeIconAlert: 7
      })
    }
  }
}

/** @function validationEmail for validation email input */
const validationEmail = (dataValidEmail) => {
  const { email, setState, state } = dataValidEmail
  const valid = valid_email(email);
  if (email === '') {
    setState({
      ...state,
      errorEmail: true,
      errorEmailText: 'Please enter email address!'
    });
  } else if (!valid) {
    setState({
      ...state,
      errorEmail: true,
      errorEmailText: 'Please enter a valid email!'
    });
  } else {
    setState({
      ...state,
      errorEmail: false,
      errorEmailText: ''
    });
    return true
  }
  return false;
}

/**
 * @func _onClickOke for handle sweetalert click
 * @param for this state
 */
const _onClickOke = (data) => {
  const { setState, state, props } = data
  setState({
    ...state,
    showModal: false,
  })
}

const mapDispatchToProps = dispatch => ({
  sendEmailForgotPass: (email) => dispatch(forgotPassword(email))
})

export default connect(null, mapDispatchToProps)(ForgotPassword);


const styles = StyleSheet.create({
  Container: {
    paddingTop: ScreenHeight * .12,
    height: ScreenHeight - 24,
    backgroundColor: colors.white,
  },
  TextHeader: {
    width: '100%',
    height: 40,
    color: colors.primary,
    fontSize: 24,
  },
  Logo: {
    width: '100%',
    height: 40,
    paddingHorizontal: '15%',
  },
  Form: {
    flex: 1,
    width: '100%',
    height: '100%',
    paddingVertical: '10%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  FormDesc: {
    width: '90%',
    textAlign: 'center',
    marginBottom: '8%',
    color: colors.darkScale,
    fontSize: 16
  },
  BottomButton: {
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    marginTop: 10,
    minHeight: 100,
  },
  LinkBack: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 5,
    marginLeft: -10
  },
  LinkBackText: {
    color: colors.primary,
    fontSize: 14,
  },
  Footer: {
    height: '15%',
    backgroundColor: colors.primary,
    width: '100%',
  },
})