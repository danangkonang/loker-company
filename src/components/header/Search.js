import React from 'react'
import { View, TextInput, StyleSheet, Platform, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome5'
import LinearGradient from 'react-native-linear-gradient'
// Components
import colors from '../style/Colors'
import myStyles from '../style'

const Search = (props) => {
	
	return (
		<LinearGradient start={{ x: 0, y: 1 }} end={{ x: 0, y: 0 }} colors={['#4782c3', '#155396']} style={{ paddingVertical: 8 }}>
      <View style={[styles.container]}>
        <TextInput
          style={[styles.searchInput, myStyles.FontMedium]}
          placeholder={props.placeholder ? props.placeholder : ''}
          placeholderTextColor={colors.grayScale}
          onChangeText={props.onChangeText}
          value={props.value}
        />
        <FontAwesome name="search" size={16} style={styles.searchIcon} />
				{/* <TouchableOpacity 
					style={styles.filterAdvance} 
					onPress={props.onFilter ? props.onFilter : () => alert('please define your props onFilter')}>
						<Icon name="filter" color={colors.primary} size={20} />
				</TouchableOpacity> */}
      </View>
    </LinearGradient>
	)

}

export default Search;

/**
 * @StyleSheet
 */
const styles = StyleSheet.create({
	container: {
    height: Platform === 'ios' ? 60 : 65,
    paddingHorizontal: 10,
    paddingVertical: 3,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  searchInput: {
    flex: 1,
    backgroundColor: colors.white,
    color: colors.darkSecond,
    borderRadius: 4,
    paddingLeft: 50,
    paddingRight: 15,
    position: 'relative',
    height: '80%',
    fontSize: 15,
    marginVertical: 6,
    marginHorizontal: 4
  },
  searchIcon: {
    color: colors.grayScale,
    position: 'absolute',
    left: 30,
    top: '43%'
	},
	filterAdvance: {
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 12,
    marginHorizontal: 8,
    padding: 6,
    borderRadius: 6
  }
})