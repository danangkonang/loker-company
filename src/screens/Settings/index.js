import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Linking, BackHandler, SafeAreaView } from 'react-native'

import { connect } from 'react-redux'

import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'
// Components
import Header from '../../components/header'
import myStyles from '../../components/style'
import colors from '../../components/style/Colors'
import ChangePassword from '../../components/Modal/ChangePassword'
import SweetAlert from '../../components/Modal/SweetAlert'
//import AsyncStorage from '@react-native-community/async-storage'
import LinearGradient from 'react-native-linear-gradient'
import AsyncStorage from '@react-native-community/async-storage'
import { logoutEmployer } from '../../_redux/_actions/employer'

class Settings extends Component {

  constructor(props) {
    super(props);
    this.state = {
      push_notification: false,
      modalPassword: false,
      modalNotification: false,
      modalLogout: false,
      modalDelete: false
    }
  }

  componentDidMount() {
    this.androidBack = BackHandler.addEventListener('hardwareBackPress', () => {
      return true
    })
  }

  componentWillUnmount() {
    this.androidBack.remove()
  }

  render() {
    const {
      push_notification,
      modalPassword,
      modalNotification,
      modalLogout,
      modalDelete
    } = this.state
    return (
      <LinearGradient
        start={
          { x: 0, y: 0 }
        }
        end={
          { x: 0, y: 1 }
        }
        colors={['#155396', '#155396', colors.white, colors.white]}
        style={{
          flex: 1
        }}
      >
        <SafeAreaView style={myStyles.Flex1}>
          <View style={[myStyles.Flex1, { backgroundColor: colors.white }]}>
            <Header
              logo={true}
              onpress={() => this.props.navigation.goBack()}
            />
            <LinearGradient start={{ x: 0, y: 1 }} end={{ x: 0, y: 0 }} colors={['#4782c3', '#155396']} style={[myStyles.Flex1, myStyles.bgPrimary, { paddingHorizontal: '5%' }]}>
              <Text style={[myStyles.FontBold, styles.TextHeader]}>
                Options & Settings
						</Text>
              <TouchableOpacity style={styles.Menu} onPress={() => {
                this.props.navigation.navigate('faq');
              }}>
                <Text style={[myStyles.FontBold, styles.MenuText]}>
                  FAQ
							</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.Menu} onPress={this._changePassword.bind(this)}>
                <Text style={[myStyles.FontBold, styles.MenuText]}>
                  Change password
							</Text>
              </TouchableOpacity>
              <View style={styles.Menu}>
                <View style={styles.MenuToggle}>
                  <Text style={[myStyles.FontBold, styles.MenuText]}>
                    Push notifications
								</Text>
                  <TouchableOpacity style={styles.ToggleIcon} onPress={this._changePushNotification.bind(this)}>
                    <MdIcon name={push_notification ? "toggle-switch" : "toggle-switch-off"} color={push_notification ? colors.primary : colors.grayScale} size={35} />
                  </TouchableOpacity>
                </View>
              </View>

              <TouchableOpacity style={styles.Menu} onPress={() => this.setState({ modalLogout: true })}>
                <Text style={[myStyles.FontBold, styles.MenuText]}>
                  Log out
							</Text>
              </TouchableOpacity>
              {/* <TouchableOpacity style={styles.Menu} onPress={() => {
                alert('Language')
              }}>
                <Text style={[myStyles.FontBold, styles.MenuText]}>
                  Language
							</Text>
              </TouchableOpacity> */}
            </LinearGradient>
            <ChangePassword
              visible={modalPassword}
              hideModal={() => this.hideModal()}
            />
            <SweetAlert
              visible={modalDelete}
              type={1}
              text={`You are about to permanently \n delete your account.\n You will not be able to recover it`}
              onYes={() => alert('Yes clicked')}
              onCancel={() => this.hideModal()}
            />
            <SweetAlert
              visible={modalLogout}
              type={3}
              text={`Are you sure you want \n to log out your account?`}
              onYes={() => this._logoutAccount()}
              onCancel={() => this.hideModal()}
            />
            <SweetAlert
              visible={modalNotification}
              type={4}
              text={`Are you sure to \n turn ${push_notification ? 'off' : 'on'} the notification`}
              onYes={() => {
                this.setState({
                  push_notification: !push_notification,
                  modalNotification: !modalNotification,
                })
                // alert(`Success Turn ${push_notification ? 'off' : 'on'} the notification`)
              }}
              textProceed="Yes"
              textCancel="No, Thanks"
              onCancel={() => this.hideModal()}
            />
          </View>
        </SafeAreaView>
      </LinearGradient>
    )
  }

	/**
	 * @function _logoutAccount
	 */
  _logoutAccount = () => {
    AsyncStorage.removeItem('employerToken')
    this.props.navigation.navigate('login')
    this.props.ActlogoutEmployer()
  }

	/**
	 * @function _changePushNotification
	 */
  _changePushNotification = () => {
    this.setState({
      modalNotification: !this.state.modalNotification
    })
  }

	/**
	 * @function _changePassword
	 */
  _changePassword = () => {
    this.setState({
      modalPassword: true
    })
  }

	/**
	 * @function hideModal
	 */
  hideModal() {
    this.setState({
      modalPassword: false,
      modalLogout: false,
      modalDelete: false,
      modalNotification: false,
    })
  }

}

const mapDispatchToProps = dispatch => ({
  ActlogoutEmployer: () => dispatch(logoutEmployer())
})

export default connect(null, mapDispatchToProps)(Settings);

const styles = StyleSheet.create({
  TextHeader: {
    color: colors.white,
    width: '100%',
    paddingVertical: 15,
    fontSize: 18,
    fontWeight: 'bold',
    borderBottomColor: colors.white,
    borderBottomWidth: .8,
    marginBottom: 10
  },
  Menu: {
    backgroundColor: colors.white,
    width: '100%',
    paddingVertical: 10,
    marginVertical: 5,
    justifyContent: 'center',
    paddingHorizontal: 10,
    borderRadius: 8,
    borderColor: colors.gray,
    borderWidth: 1,
    elevation: 3,
  },
  MenuText: {
    color: colors.darkSecond,
    fontSize: 16,
    fontWeight: 'bold'
  },
  MenuToggle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: -5
  },
  ToggleIcon: {
    paddingHorizontal: 10,
    marginRight: -10
  }
})