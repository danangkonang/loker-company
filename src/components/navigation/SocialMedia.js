import React from 'react'
import { View, TouchableOpacity, Linking, StyleSheet } from 'react-native'
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'
// Component
import colors from '../style/Colors'

class SocialMedia extends React.Component {
	render(){
		const {
			facebook,
			twitter,
			linkedin,
			website
		} = this.props
		return (
			<View style={styles.SocialMedia}>
				{
					facebook != '' && facebook ? 
					<TouchableOpacity style={styles.buttonShare} onPress={() => this.onClicked(facebook)}>
						<MdIcon name="facebook" color={colors.primary} size={22} />
					</TouchableOpacity>
					:
					null
				}
				{
					twitter != '' && twitter ?
					<TouchableOpacity style={styles.buttonShare} onPress={() => this.onClicked(twitter)}>
						<MdIcon name="twitter" color={colors.primary} size={22} />
					</TouchableOpacity>
					:
					null
				}
				{
					linkedin != '' && linkedin ?
					<TouchableOpacity style={styles.buttonShare} onPress={() => this.onClicked(linkedin)}>
						<MdIcon name="linkedin" color={colors.primary} size={22} />
					</TouchableOpacity>
					:
					null
				}
				{
					website != '' && website ?
					<TouchableOpacity style={styles.buttonShare} onPress={() => this.onClicked(website)}>
						<MdIcon name="web" color={colors.primary} size={22} />
					</TouchableOpacity>
					:
					null
				}
			</View>
		)
	}

	/**
	 * @function onClicked to Handle Click Social Media
	 */
	onClicked = (params) => {
		// alert(params)
		Linking.canOpenURL(params).then(supported => {
			if (supported) {
				Linking.openURL(params)
			}
		})
	}
}

export default SocialMedia

const styles = StyleSheet.create({
	SocialMedia: {
		width: '100%',
		marginVertical: 10,
		flexDirection: 'row',
		justifyContent: 'center',
	},
	buttonShare: {
		width: 40,
		height: 40,
		elevation: 2,
		borderWidth: 1,
		marginHorizontal: 5,
		borderRadius: 100/2,
		alignItems: 'center',
		borderColor: colors.gray,
		justifyContent: 'center',
		backgroundColor: colors.whiteScale,
	},
})