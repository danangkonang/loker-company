import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import SplashScreen from '../screens/SplashScreen'

import Login from '../screens/Login'
import ForgotPassword from '../screens/Login/ForgotPassword'

import Registrasi from '../screens/registrasion'
import TermCondition from '../screens/registrasion/TermCondition'
import Otp from '../screens/registrasion/Otp'
import Password from '../screens/registrasion/Password'
import CompanyAccount from '../screens/registrasion/CompanyAccount'
import CompanyPage from '../screens/registrasion/CompanyPage'
import CompanyAddress from '../screens/registrasion/CompanyAddress'
import AdminIdentity from '../screens/registrasion/AdminIdentity'

import Home from '../screens/Home'

import Settings from '../screens/Settings'
import Faq from '../screens/Faq'
import FaqCategory from '../screens/Faq/FaqCategory'

import Profil from '../screens/profilecandidate'
import AchiveList from '../screens/profilecandidate/listAll/AchiveList'
import EducationList from '../screens/profilecandidate/listAll/EducationList'
import OrganizationList from '../screens/profilecandidate/listAll/OrganizationList'
import LanguageList from '../screens/profilecandidate/listAll/LanguageList'
import PortfolioList from '../screens/profilecandidate/listAll/PortfolioList'
import WorkList from '../screens/profilecandidate/listAll/WorkList'
import SkillList from '../screens/profilecandidate/listAll/SkillList'
import PersonalitiList from '../screens/profilecandidate/listAll/PersonalitiList'

import JobDetail from '../screens/Jobs/AppDetailJobs'
import AppJobs from '../screens/Jobs/AppJobs'
import AppEditJob from '../screens/Jobs/AppEditJob'

const Routes = createStackNavigator(
	{
		splashScreen: {
			screen: SplashScreen
		},
		login: {
			screen: Login
		},
		forgotpassword: {
			screen: ForgotPassword
		},
		registrasi: {
			screen: Registrasi
		},
		tac: {
			screen: TermCondition
		},
		otp: {
			screen: Otp
		},
		password: {
			screen: Password
		},
		companyAccount: {
			screen: CompanyAccount
		},
		companyPage: {
			screen: CompanyPage
		},
		companyAddress: {
			screen: CompanyAddress
		},
		adminIdentity: {
			screen: AdminIdentity
		},
		home: {
			screen: Home
		},
		settings: {
			screen: Settings
		},
		faq: {
			screen: Faq
		},
		faqcategory: {
			screen: FaqCategory
		},
		profil: {
			screen: Profil
		},
		achiveList: {
			screen: AchiveList
		},
		educationList: {
			screen: EducationList
		},
		organizationList: {
			screen: OrganizationList
		},
		languageList: {
			screen: LanguageList
		},
		portfolioList: {
			screen: PortfolioList
		},
		workList: {
			screen: WorkList
		},
		skillList: {
			screen: SkillList
		},
		personalitiList: {
			screen: PersonalitiList
		},
		// JOB ROUTES
		JobDetail: {
			screen: JobDetail
		},
		Jobs: {
			screen: AppJobs
		},
		EditJob: {
			screen: AppEditJob
		}
	},
	{
		headerMode: 'none',
		defaultNavigationOptions: {
			gesturesEnabled: false
		},
	}
)

export default createAppContainer(Routes)