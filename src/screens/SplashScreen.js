import React, { Component } from 'react'
import { View, StatusBar, Image, Text } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux'
import { refreshTokenEmployer } from '../_redux/_actions/employer'

import myStyles from '../components/style/index'
import colors from '../components/style/Colors'

class SpalshScreen extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.checkingToken()
    }, 1000)
  }

  checkingToken = async () => {
    const employerToken = await AsyncStorage.getItem('employerToken');
    // console.log(userToken);
    if (employerToken) {
      try {
        await this.props.refreshEmployerToken(employerToken);
        this.props.navigation.navigate('home');
      } catch (error) {
        await AsyncStorage.removeItem('userToken')
        this.props.navigation.navigate('login')
      }
    } else {
      this.props.navigation.navigate('login');
    }
  }

  render() {
    return (
      <View style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
        <StatusBar hidden={true} />
        <View style={{
          height: 100, width: 100
        }}>
          <Image source={require('../components/icons/icon_cs.png')} style={myStyles.ImgCover} />
        </View>
        <View style={{ bottom: 5, position: "absolute" }}>
          <Text style={{ color: colors.grayScale }}>Version 0.0.1</Text>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => ({
  employer: state.employer
})

const mapDispatchToProps = dispatch => ({
  refreshEmployerToken: token => dispatch(refreshTokenEmployer(token))
})

export default connect(mapStateToProps, mapDispatchToProps)(SpalshScreen)