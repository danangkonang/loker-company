import React, { Component } from 'react'

import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  TextInput,
  StyleSheet
} from 'react-native'
import HeaderStep from '../../components/header/HeaderStep'
import colors from '../../components/style/Colors'
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import Button from '../../components/button'
import myStyles from '../../components/style'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
// import { getCountry } from '../../_redux/_actions/employer'
import axios from 'axios'
import ErrorText from '../../components/customTextInput/Error'
class App extends Component {
   state = {
      token: '',
      token: '',
      country: [],
      hasilCountry: [],
      countryValue: '',

      province: [],
      hasilProvince: [],
      provinceValue: '',

      city: [],
      hasilCity: [],
      cityValue: '',

      zipCode: '',
      address: '',

      errProvince: false,
      errCountry: false,
      errCity: false,
      errZipCode: false,
      errAddress: false,
      msgErrProvince: '',
      msgErrCountry: '',
      msgErrCity: '',
      msgErrZipCode: '',
      msgErrAddress: '',

      isSave:false
   }
   componentDidMount = async () => {
      await this.getToken()
      await this.getAllConutry()
   }
   getToken = async () => {
      let tok = await AsyncStorage.getItem('employerToken')
      await this.setState({ token: tok })
   }

   getAllConutry = async () => {
      try {
         const getConutry = await axios.get(`http://128.199.224.138:8080/api/v1/region/country/`, {
         headers: {
            Authorization: `JWT ${this.state.token}`
         }
         })
         let cntry = getConutry.data.map((e) => {
         return { label: e.name, value: e.id }
         })
         this.setState({ country: cntry })
      } catch (e) {
         console.log(e)
      }
   }
   serchCountry(v) {
      if (v == '') {
         this.setState({ hasilCountry: [] })
      } else {
         const d = this.state.country;
         const regex = new RegExp(`${v.trim()}`, 'i');
         this.setState({ hasilCountry: d.filter(cntry => cntry.label.search(regex) >= 0) })
      }
   }

   getProvince = async (v) => {
      const provin = await axios.get(`http://128.199.224.138:8080/api/v1/region/province/${v}/`, {
         headers: {
         Authorization: `JWT ${this.state.token}`
         }
      })
      let pro = provin.data.map((e) => {
         return { label: e.name, value: e.id }
      })
      this.setState({ province: pro })
   }

   seachProvince = (v) => {
      if (v == '') {
         this.setState({ hasilProvince: [] })
      } else {
         const d = this.state.province;
         const regex = new RegExp(`${v.trim()}`, 'i');
         this.setState({ hasilProvince: d.filter(pro => pro.label.search(regex) >= 0) })
      }
   }

   getCity = async (v) => {
      const city = await axios.get(`http://128.199.224.138:8080/api/v1/region/city/${v}/`, {
         headers: {
         Authorization: `JWT ${this.state.token}`
         }
      })
      let citi = city.data.map((e) => {
         return { label: e.name, value: e.id }
      })
      this.setState({ city: citi })
   }

   searchCity = (v) => {
      if (v == '') {
         this.setState({ hasilCity: [] })
      } else {
         const d = this.state.city;
         const regex = new RegExp(`${v.trim()}`, 'i');
         this.setState({ hasilCity: d.filter(pro => pro.label.search(regex) >= 0) })
      }
   }

   validasi = () => {
      let { provinceValue, countryValue, cityValue, zipCode, address } = this.state
         if (countryValue == '') {
            this.setState({ errCountry: true, msgErrCountry: 'Required' })
         } else {
            if (provinceValue == '') {
            this.setState({ errProvince: true, msgErrProvince: 'Required' })
            } else {
            if (cityValue == '') {
               this.setState({ errCity: true, msgErrCity: 'Required' })
            } else {
               if (zipCode == '') {
                  this.setState({ errZipCode: true, msgErrZipCode: 'Required' })
               } else {
                  if (address == '') {
                  this.setState({ errAddress: true, msgErrAddress: 'Required' })
                  } else {
                  this.saveAddress()
                  }
               }
            }
         }
      }
   }

   saveAddress=async()=>{
      let res =await fetch(`http://128.199.224.138:8080/api/v1/company/registration/employer_contact/`,{
         method: 'PATCH',
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization':`JWT ${this.state.token}` 
         },
         body: JSON.stringify({
            country:this.state.countryValue,
            province:this.state.provinceValue,
            city: this.state.cityValue,
            postal_code: this.state.zipCode,
            address: this.state.address
         })
      })
      let respon =await res.json()
      console.log(respon)
      this.props.navigation.navigate('adminIdentity')
   }

  render() {
    let { hasilCountry, hasilProvince, hasilCity, provinceValue, countryValue, cityValue, zipCode } = this.state
    return (
      <ScrollView>
        <HeaderStep
          colorNonActive={colors.cyan}
          colorActive={colors.primary}
          step={[true, true, true, true, true, true, false]}
        />

        <View style={{ alignItems: 'center', marginTop: 20 }}>
          <Text style={[myStyles.FontBold, { color: colors.primary, fontSize: 25, textAlign: 'center' }]}>Address and contact {"\n"} information</Text>
        </View>

        <View style={{ alignItems: 'center', paddingTop: 10 }}>
          <TouchableOpacity activeOpacity={.7} style={{ backgroundColor: colors.primary, height: 120, width: 120, borderRadius: 60, justifyContent: 'center' }}>
            <Text style={{ textAlign: 'center', fontSize: 15, color: colors.white, fontWeight: 'bold' }}>
              Icon admin
                        </Text>
          </TouchableOpacity>
        </View>

        <View style={{ flex: 1, paddingHorizontal: 15, marginVertical: 10 }}>
          <View style={{ flex: 1, backgroundColor: '#FFF', alignItems: 'center' }}>
            <Text style={{ textAlign: 'center', color: colors.grayScale }}>
              Esse in aliquip voluptate nostrud qui in elit ad.{"\n"}
              Occaecat nulla mollit in deserunt .{"\n"}
              Voluptate ea est laboris.
                        </Text>
          </View>
        </View>

        <View style={{ flex: 1, alignItems: 'center', marginVertical: 5, marginHorizontal: 35 }}>
          <TextInput
            placeholder='Choose country'
            value={countryValue}
            placeholderTextColor={colors.darkScale}
            onChangeText={(v) => {
              this.setState({ countryValue: v })
              this.serchCountry(v)
            }}
            style={[myStyles.FontMedium, { borderColor: colors.gray, borderWidth: 2, height: 50, width: '100%', borderRadius: 3, paddingLeft: 10, color: colors.darkScale, marginBottom: 5, elevation: 1 }]}
          />
          <MdIcon name="chevron-down" size={20} color={colors.grayScale} style={{ position: 'absolute', right: 15, top: 10 }} />
          <View style={hasilCountry.length !== 0 ? { width: '100%', marginTop: 3 } : null}>
            {
              hasilCountry.map((i, n) => {
                if (n < 5) {
                  return (
                    <TouchableOpacity style={{ borderBottomColor: '#fff', borderBottomWidth: 2, paddingLeft: 15, height: 38, justifyContent: 'center', backgroundColor: '#eee' }} key={n}
                      onPress={() => {
                        this.setState({
                          countryValue: i.label,
                          countryValueErr: false,
                          hasilCountry: []
                        })
                        this.getProvince(i.value)
                      }}>
                      <Text style={{ fontWeight: 'bold', color: '#999' }}>{i.label}</Text>
                    </TouchableOpacity>
                  )
                }
              })
            }
          </View>
        </View>
        <View style={{ marginHorizontal: 35 }}>
          {
            this.state.errCountry == true ?
              <ErrorText
                text={this.state.msgErrCountry}
              /> : null
          }
        </View>

        <View style={{ flex: 1, alignItems: 'center', marginVertical: 5, marginHorizontal: 35 }}>
          <TextInput
            placeholder='Choose province'
            value={provinceValue}
            placeholderTextColor={colors.darkScale}
            onChangeText={(v) => {
              this.setState({ provinceValue: v })
              this.seachProvince(v)
            }}
            style={[myStyles.FontMedium, { borderColor: colors.gray, borderWidth: 2, height: 45, width: '100%', borderRadius: 3, paddingLeft: 10, color: colors.darkScale, marginBottom: 5, elevation: 1 }]}
          />
          <MdIcon name="chevron-down" size={20} color={colors.grayScale} style={{ position: 'absolute', right: 15, top: 10 }} />
          <View style={hasilProvince.length !== 0 ? { width: '100%', marginTop: 3 } : null}>
            {
              hasilProvince.map((i, n) => {
                if (n < 5) {
                  return (
                    <TouchableOpacity style={{ borderBottomColor: '#fff', borderBottomWidth: 2, paddingLeft: 15, height: 38, justifyContent: 'center', backgroundColor: '#eee' }} key={n} onPress={() => {
                      this.setState({
                        provinceValue: i.label,
                        provinceValueErr: false,
                        hasilProvince: []
                      })
                      this.getCity(i.value)
                    }}>
                      <Text style={{ fontWeight: 'bold', color: '#999' }}>{i.label}</Text>
                    </TouchableOpacity>
                  )
                }
              })
            }
          </View>
        </View>
        <View style={{ marginHorizontal: 35 }}>
          {
            this.state.errProvince == true ?
              <ErrorText
                text={this.state.msgErrProvince}
              /> : null
          }
        </View>

        <View style={{ flex: 1, alignItems: 'center', marginVertical: 5, marginHorizontal: 35 }}>
          <TextInput
            placeholder='Choose city'
            value={cityValue}
            placeholderTextColor={colors.darkScale}
            onChangeText={(v) => {
              this.setState({ cityValue: v })
              this.searchCity(v)
            }}
            style={[myStyles.FontMedium, { borderColor: colors.gray, borderWidth: 2, height: 45, width: '100%', borderRadius: 3, paddingLeft: 10, color: colors.darkScale, marginBottom: 5, elevation: 1 }]}
          />
          <MdIcon name="chevron-down" size={20} color={colors.grayScale} style={{ position: 'absolute', right: 15, top: 10 }} />
          <View style={hasilCity.length !== 0 ? { width: '100%', marginTop: 3 } : null}>
            {
              hasilCity.map((i, n) => {
                if (n < 5) {
                  return (
                    <TouchableOpacity style={{ borderBottomColor: '#fff', borderBottomWidth: 2, paddingLeft: 15, height: 38, justifyContent: 'center', backgroundColor: '#eee' }} key={n}
                      onPress={() => {
                        this.setState({
                          cityValue: i.label,
                          cityValueErr: false,
                          hasilCity: []
                        })
                      }}>
                      <Text style={{ fontWeight: 'bold', color: '#999' }}>{i.label}</Text>
                    </TouchableOpacity>
                  )
                }
              })
            }
          </View>
        </View>
        <View style={{ marginHorizontal: 35 }}>
          {
            this.state.errCity == true ?
              <ErrorText
                text={this.state.msgErrCity}
              /> : null
          }
        </View>

        <View style={{ flex: 1, alignItems: 'center', marginVertical: 5, marginHorizontal: 35 }}>
          <TextInput
            placeholder='Choose postal code'
            value={zipCode}
            placeholderTextColor={colors.darkScale}
            onChangeText={(v) => this.setState({ zipCode: v })}
            style={[myStyles.FontMedium, { borderColor: colors.gray, borderWidth: 2, height: 45, width: '100%', borderRadius: 3, paddingLeft: 10, color: colors.darkScale, marginBottom: 5, elevation: 1 }]}
          />
        </View>
        <View style={{ marginHorizontal: 35 }}>
          {
            this.state.errZipCode == true ?
              <ErrorText
                text={this.state.msgErrZipCode}
              /> : null
          }
        </View>

        <View style={{ marginHorizontal: 35 }}>
          <TextInput
            style={{ height: 120, borderColor: colors.gray, borderWidth: 2, borderRadius: 4, paddingHorizontal: 15, fontSize: 14, fontFamily: 'DIN-Medium', color: colors.darkScale, marginVertical: 5, elevation: 1 }}
            onChangeText={(v) => this.setState({ address: v })}
            multiline={true}
            placeholder='Enter address'
            underlineColorAndroid={'transparent'}
            textAlignVertical={'top'}
          />
        </View>
        <View style={{ marginHorizontal: 35 }}>
          {
            this.state.errAddress == true ?
              <ErrorText
                text={this.state.msgErrAddress}
              /> : null
          }
        </View>

        <View style={{ alignItems: 'center' }}>
          <Button
            marginVertical={15}
            width='40%'
            backgroundColor={colors.primary}
            color={colors.white}
            text="Next"
            onclick={this.validasi}
            isSave={this.state.isSave}
            disabled={this.state.isSave}
          // onclick={()=>this.props.navigation.navigate('educationUser')}
          />
        </View>

      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    // user_data: state.user.user_data
  }
}


export default connect(mapStateToProps)(App)


const SelectStyle = StyleSheet.create({
  inputIOS: {
    fontSize: 14,
    paddingVertical: 8,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: colors.grayScale,
    borderRadius: 4,
    color: colors.darkSecond,
    paddingRight: 30,
    marginVertical: 5,
    height: 45,
    fontFamily: 'DIN-Medium'
  },
  inputAndroid: {
    fontSize: 14,
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderWidth: .5,
    borderColor: colors.gray,
    borderRadius: 3,
    color: colors.darkSecond,
    paddingRight: 30,
    marginVertical: 5,
    elevation: 1.5,
  },
})
