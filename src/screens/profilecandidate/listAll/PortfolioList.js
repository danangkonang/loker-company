import React, { Component } from 'react'
import {
	View,
	Text,
	ScrollView,
	Dimensions,
	SafeAreaView,
	StyleSheet,

} from 'react-native'
import Header from '../../../components/header/index'
import HeaderImage from '../../../components/profile/HeaderImage'
import myStyles from '../../../components/style'
import colors from '../../../components/style/Colors'
import List from './List'
import LogoUser from '../../../components/profile/LogoUser'
import { connect } from 'react-redux'
import DetailPortfolio from '../../profilecandidate/PortfolioDetail'
let AndroidHeight = Dimensions.get('window').height

class Portfolio extends Component {

	state = {
		isDelete: false,
		scroll: true,
		showModal: false,
		achiveAddModal: false,
		achiveDetailModal: false,
		achiveEditModal: false,
		userToken: '',
		typeDelete: null,
		data: [1, 2, 3, 4, 5, 6],
		modalPortfolio: false,
		detailFolio:{},
	}

	render() {
		return (
			<SafeAreaView style={myStyles.Flex1}>
				<View style={myStyles.Flex1}>
					<Header
						logo={true}
						onpress={() => this.props.navigation.goBack()}
					/>
					<ScrollView
						showsVerticalScrollIndicator={false}
						scrollEnabled={this.state.scroll}
					>
						<HeaderImage
							fullName={
								this.props.candidate.data_candidate.cv_unlocked ? this.props.candidate.data_candidate.user.full_name : "[Name Hidden]"
							}
							major={
								this.props.candidate.data_candidate.last_education.major.label
							}
							education={
								this.props.candidate.data_candidate.last_education.school.label
							}
						/>
						<LogoUser image={this.props.candidate.data_candidate.avatar} />
						<View style={[myStyles.ProfileSection, { minHeight: AndroidHeight - 281 }]}>
							<View style={{ flex: 1, marginTop: 15, marginBottom: 5, borderRadius: 10, backgroundColor: colors.white, }}>
								<View style={styles.Header}>
									<Text style={[styles.Title, myStyles.FontBold]}>
										Portfolio
									</Text>
								</View>

								<View style={[{ flex: 1, paddingVertical: 10, paddingHorizontal: 20, borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }]}>
									{
										this.props.candidate.portfolio_list.map((item, idx) => (
											<List
												key={idx}
												click={()=>this.detailPortfolio(item)}
												Title={item.name}
												TextDesc={item.title}
												Years={item.start_date}
												YearsOnly={true}
											/>
										))
									}
								</View>
							</View>
						</View>
					</ScrollView>
					<DetailPortfolio
						data={this.state.detailFolio}
						isVisible={this.state.modalPortfolio}
						onClose={() => this.setState({ modalPortfolio: false })}
					/>
				</View>
			</SafeAreaView>
		)
	}
	detailPortfolio =async (data) => {
		await this.setState({detailFolio:data})
		this.setState({ modalPortfolio: true })
		// console.log(this.state.detailFolio)
	}
}

const mapStateToProps = state => ({
	candidate: state.candidate
})

export default connect(mapStateToProps)(Portfolio);

const styles = StyleSheet.create({
	Header: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		width: '100%',
		paddingVertical: 15,
		paddingHorizontal: 20,
		borderBottomWidth: .7,
		borderBottomColor: colors.gray,
	},
	Title: {
		fontSize: 18,
		color: colors.primary,
		fontWeight: 'bold',
	},
	// Content: {
	// 	flex: 1,
	// 	paddingVertical: 10,
	// 	paddingHorizontal: 20,
	// },
	// AddButton: {
	// 	backgroundColor: colors.cyan,
	// 	flexDirection: 'row',
	// 	width: '100%',
	// 	paddingVertical: 8,
	// 	marginVertical: 5,
	// 	justifyContent: 'center',
	// 	alignItems: 'center',
	// 	borderRadius: 6,
	// 	borderWidth: .5,
	// 	borderColor: colors.white,
	// },
	// TextButton: {
	// 	fontSize: 18,
	// 	color: colors.primary,
	// 	fontWeight: 'bold',
	// 	marginRight: 3
	// },
	// Item: {
	// 	paddingBottom: 50,
	// 	marginBottom: 5,
	// },
	// // Skills Searching
	// Search: {
	// 	marginBottom: 10,
	// 	backgroundColor: colors.white,
	// 	flexDirection: 'row',
	// 	paddingLeft: 10,
	// 	alignItems: 'center',
	// 	width: '100%',
	// 	borderColor: colors.gray,
	// 	borderWidth: 1,
	// 	borderRadius: 100/15,
	// 	shadowColor: "#000",
	// 	shadowOffset: {
	// 		width: 1,
	// 		height: 1,
	// 	},
	// 	shadowOpacity: 0.25,
	// 	shadowRadius: 5,
	// 	elevation: 2,
	// },
	// SearchInput: {
	// 	flex: 1,
	// 	paddingHorizontal: 15,
	// 	color: colors.darkScale,
	// 	minHeight: 50
	// },
	// Skills: {
	// 	minHeight: 100,
	// 	flexDirection: 'row',
	// 	justifyContent: 'flex-start',
	// 	flexWrap: 'wrap',
	// 	marginHorizontal: -12,
	// 	padding: '2%'
	// },
	// SkillItem: {
	// 	paddingHorizontal: 10,
	// 	paddingVertical: 8,
	// 	marginHorizontal: 5,
	// 	flexDirection: 'row',
	// 	justifyContent: 'space-between',
	// 	alignItems: 'center',
	// 	backgroundColor: colors.white,
	// 	marginVertical: 6,
	// 	borderRadius: 100/15,
	// 	borderWidth: 1.5,
	// 	borderStyle: "solid",
	// 	shadowColor: "#000",
	// 	shadowOpacity: 0.25,
	// 	shadowOffset: {
	// 		width: 1,
	// 		height: .5
	// 	},
	// 	shadowRadius: 5,
	// 	elevation: 2
	// },
	// SkillText: {
	// 	color: colors.darkSecond,
	// 	fontSize: 12,
	// 	fontWeight: 'bold',
	// 	marginHorizontal: 5
	// },
	// Autocomplete: {
	// 	borderBottomColor: colors.white, 
	// 	borderBottomWidth: 2,
	// 	paddingLeft: 15,
	// 	height: 38, 
	// 	justifyContent:'center',
	// 	backgroundColor:colors.gray
	// }
})
