
import React from 'react'
import {
  View,
} from 'react-native'

import myStyles from '../../components/style'
import colors from '../../components/style/Colors'
import TextComponent from '../../components/Text'

const DetailPersonal = (props) => {
  return (
    <>
      <View style={{
        minHeight: 40,
        marginTop: 8,
        paddingLeft: 20,
        paddingRight: 10,
        flexDirection: 'row',
        justifyContent: 'flex-start'
      }}>
        <View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
          <TextComponent
            fontSize={16}
            color={colors.darkScale}
            text="Gender"
            bold
            bottom
          />
          <TextComponent
            fontSize={14}
            color={colors.darkScale}
            text={props.Gender}
            light
          />
        </View>

        <View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
          <TextComponent
            fontSize={16}
            color={colors.darkScale}
            text="Birth date"
            bold
            bottom
          />
          <TextComponent
            fontSize={14}
            color={colors.darkScale}
            text={props.BirthDate}
            light
          />
        </View>
      </View>
      <View style={{
        minHeight: 40,
        marginTop: 20,
        paddingLeft: 20,
        paddingRight: 10,
        justifyContent: 'flex-start'
      }}>
        <TextComponent
          fontSize={18}
          color={colors.primary}
          text="Address"
          light
        />
      </View>
      <View style={{
        minHeight: 40,
        paddingLeft: 20,
        paddingRight: 10,
        flexDirection: 'row',
        justifyContent: 'flex-start'
      }}>
        <View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
          <TextComponent
            fontSize={16}
            color={colors.darkScale}
            text="Country"
            bold
            bottom
          />
          <TextComponent
            fontSize={14}
            color={colors.darkScale}
            text={props.Country}
            light
          />
        </View>

        <View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
          <TextComponent
            fontSize={16}
            color={colors.darkScale}
            text="Province"
            bold
            bottom
          />
          <TextComponent
            fontSize={14}
            color={colors.darkScale}
            text={props.Province}
            light
          />
        </View>
      </View>
      <View style={{
        minHeight: 40,
        marginTop: 8,
        paddingLeft: 20,
        paddingRight: 10,
        flexDirection: 'row',
        justifyContent: 'flex-start'
      }}>
        <View style={[myStyles.Flex1, { justifyContent: 'flex-start' }]}>
          <TextComponent
            fontSize={16}
            color={colors.darkScale}
            text="City"
            bold
            bottom
          />
          <TextComponent
            fontSize={14}
            color={colors.darkScale}
            text={props.City}
            light
          />
        </View>
      </View>
    </>
  )
}

export default DetailPersonal;