import axios from 'axios'
import { CS_KEY } from 'react-native-dotenv'

/**
 * @function getListJobActive
 * @param is token
 */
export const getListJobActive = (token) => {
	return {
		type: 'GET_LIST_JOB_ACTIVE',
		payload: axios.get(`${CS_KEY}company/jobs/vacancy/active/`, {
			headers: {
				'Authorization': `JWT ${token}`,
				'Content-Type': 'application/json'
			}
		})
	}
}

/**
 * @function getListJobInActive
 * @param is token
 */
export const getListJobInActive = (token) => {
	return {
		type: 'GET_LIST_JOB_INACTIVE',
		payload: axios.get(`${CS_KEY}company/jobs/vacancy/inactive/`, {
			headers: {
				'Authorization': `JWT ${token}`,
				'Content-Type': 'application/json'
			}
		})
	}
}

/**
 * @function getDetailJob
 * @param is id, token
 */
export const getDetailJob = (id, token) => {
	return {
		type: 'GET_DETAIL_JOB',
		payload: axios.get(`${CS_KEY}company/jobs/vacancy/${id}/`, {
			headers: {
				'Authorization': `JWT ${token}`,
				'Content-Type': 'application/json'
			}
		})
	}
}

/**
 * @function getLockedCV
 * @param is id, token
 */
export const getLockedCV = (id, token) => {
	return {
		type: 'GET_LOCKED_CV',
		payload: axios.get(`${CS_KEY}company/jobs/vacancy/${id}/cv/`, {
			headers: {
				'Authorization': `JWT ${token}`,
				'Content-Type': 'application/json'
			}
		})
	}
}

/**
 * @function getUnLockedCV
 * @param is id, token
 */
export const getUnLockedCV = (id, token) => {
	return {
		type: 'GET_UNLOCKED_CV',
		payload: axios.get(`${CS_KEY}company/jobs/vacancy/${id}/cv/unlocked/`, {
			headers: {
				'Authorization': `JWT ${token}`,
				'Content-Type': 'application/json'
			}
		})
	}
}

/**
 * @function getListAllCountry
 * @param is token
 */
export const getListAllCountry = (token) => {
	return {
		type: 'GET_LIST_COUNTRY',
		payload: axios.get(`${CS_KEY}company/region/country/`, {
			headers: {
				'Authorization': `JWT ${token}`
			}
		})
	}
}

/**
 * @function getListAllEmployment
 * @param is token
 */
export const getListAllEmployment = (token) => {
	return {
		type: 'GET_LIST_EMPLOYMENT',
		payload: axios.get(`${CS_KEY}company/jobs/employment/`, {
			headers: {
				'Authorization': `JWT ${token}`,
				'Content-Type': 'application/json'
			}
		})
	}
}

/**
 * @function getListAllCategory
 * @param is token
 */
export const getListAllCategory = (token) => {
	return {
		type: 'GET_LIST_CATEGORY',
		payload: axios.get(`${CS_KEY}company/jobs/category/`, {
			headers: {
				'Authorization': `JWT ${token}`,
				'Content-Type': 'application/json'
			}
		})
	}
}

/**
 * @function getListAllPosition
 * @param is token
 */
export const getListAllPosition = (token) => {
	return {
		type: 'GET_LIST_POSITION',
		payload: axios.get(`${CS_KEY}company/jobs/position/`, {
			headers: {
				'Authorization': `JWT ${token}`,
				'Content-Type': 'application/json'
			}
		})
	}
}

/**
 * @function getProvinceById
 * @param is token
 */
export const getProvinceById = (countryId, token) => {
	return {
		type: 'GET_PROVINCE_BYID',
		payload: axios.get(`${CS_KEY}company/region/province/${countryId}/`, {
			headers: {
				'Authorization': `JWT ${token}`,
				'Content-Type': 'application/json'
			}
		})
	}
}

/**
 * @function getCityById
 * @param is token
 */
export const getCityById = (provinceId, token) => {
	return {
		type: 'GET_CITY_BYID',
		payload: axios.get(`${CS_KEY}company/region/city/${provinceId}/`, {
			headers: {
				'Authorization': `JWT ${token}`,
				'Content-Type': 'application/json'
			}
		})
	}
}

/**
 * @function getJobSpecialization
 * @param is token
 */
export const getJobSpecialization = (categoryId, token) => {
	return {
		type: 'GET_JOB_SPECIALIZATION',
		payload: axios.get(`${CS_KEY}company/jobs/specialization/?category=${categoryId}`, {
			headers: {
				'Authorization': `JWT ${token}`,
				'Content-Type': 'application/json'
			}
		})
	}
}

/**
 * @function updateDetailJob
 * @param is id, data & token
 */
export const updateDetailJob = (id, data, token) => {
	return {
		type: 'UPDATE_DETAIL_JOB',
		payload: axios.patch(`${CS_KEY}company/jobs/detail/${id}/`, data, {
			headers: {
				'Authorization': `JWT ${token}`,
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		})
	}
}