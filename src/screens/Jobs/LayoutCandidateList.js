import React from 'react'
import { View, TouchableOpacity, Image } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import IonIcon from 'react-native-vector-icons/Ionicons'
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import FRIcon from 'react-native-vector-icons/Feather'
// Components
import colors from '../../components/style/Colors'
import myStyles from '../../components/style'
import Text from '../../components/Text'
import { formatDate } from '../../components/constants/format'

const LayoutCandidateList = (props) => {
	const { keyTab, name, graduateYear, appliedOn, gpa, major, image, onPressed, verified } = props
	return (
		<View style={{
			minHeight: 150,
			backgroundColor: colors.white,
			overflow: 'hidden',
			flexDirection: 'row',
			marginBottom: 18
		}}>
			<TouchableOpacity 
				style={[myStyles.Flex1, {
					backgroundColor: colors.white,
					borderTopLeftRadius: 8,
					borderBottomLeftRadius: 8,
					borderBottomWidth: 1.5,
					borderTopWidth: 1.5,
					borderColor: colors.gray,
					borderLeftWidth: 1.5,
					overflow: 'hidden'
				}]}
				onPress={onPressed}
			>
				<View 
					style={[myStyles.Flex1, {
						flexDirection: 'row',
						justifyContent: 'flex-start',
						alignItems: 'flex-start',
						paddingVertical: 10,
						paddingHorizontal: 10,
					}]}
				>
					<View style={{
						width: 50,
						height: 50,
						borderRadius: 100/2,
						borderWidth: 1,
						borderColor: colors.gray,
						marginRight: 5,
					}}>
						<Image source={image && keyTab === 2 ? { uri : image } : require('../../assets/img/default_foto.jpg')} style={[myStyles.Flex1, myStyles.ImgCover, { borderRadius: 100/2 }]} />
					</View>
					<View style={[myStyles.Flex1, {
						paddingHorizontal: 10,
						paddingVertical: 5,
						height: '100%',
					}]}>
						<View style={{
							flexDirection: 'row',
							justifyContent: 'flex-start',
							alignItems: 'center',
						}}>
							<Text 
								text={keyTab === 2 ? name ? name : '' : '[Name Hidden]'}
								fontSize={17}
								color={colors.darkScale}
								bold
								right={5}
							/>
							{
								verified &&
								<MdIcon name="check-decagram" size={18} color={colors.pink} />
							}
						</View>
						<View style={{
							flexDirection: 'row',
							justifyContent: 'space-between',
							marginTop: 8,
						}}>
							<View style={myStyles.Flex1}>
								<Text 
									text="Graduated Year"
									fontSize={15}
									color={colors.darkScale}
								/>
								<Text 
									text={graduateYear ? formatDate(graduateYear, 1) : '-'}
									fontSize={14}
									color={colors.darkScale}
									light
								/>
							</View>

							<View style={myStyles.Flex1}>
								<Text 
									text="Applied on"
									fontSize={15}
									color={colors.darkScale}
								/>
								<Text 
									text={appliedOn ? formatDate(appliedOn) : '-'}
									fontSize={14}
									color={colors.darkScale}
									light
								/>
							</View>
						</View>

						<View style={{
							flexDirection: 'row',
							justifyContent: 'space-between',
							marginTop: 8,
						}}>
							<View style={myStyles.Flex1}>
								<Text 
									text="GPA"
									fontSize={15}
									color={colors.darkScale}
								/>
								<Text 
									text={gpa ? gpa : '0'}
									fontSize={14}
									color={colors.darkScale}
									light
								/>
							</View>

							<View style={myStyles.Flex1}>
								<Text 
									text="Major"
									fontSize={15}
									color={colors.darkScale}
								/>
								<Text 
									text={major ? major : '-'}
									fontSize={14}
									color={colors.darkScale}
									light
								/>
							</View>
						</View>

					</View>
				</View>
			</TouchableOpacity>
			<View style={{
				width: '10%',
				overflow: 'hidden',
				backgroundColor: colors.cyan,
				borderTopRightRadius: 8,
				borderBottomRightRadius: 8
			}}>
				<TouchableOpacity
					onPress={keyTab === 1 ? () => alert('Unlocked') : () => alert('Download CV')}
				>
					<LinearGradient 
					colors={['#4782c3', '#155396']}
					start={{ x: 0, y: 1	}}
					style={{
						justifyContent: 'center',
						alignItems: 'center',
						height: 50,
						borderTopRightRadius: 5,
					}}>
						{
							keyTab === 1 ?
							<IonIcon name="md-unlock" size={25} color={colors.white} />
							:
							<View style={{
								flex: 1,
								paddingVertical: 4,
								alignItems: 'center'
							}}>
								<Text 
									text="CV"
									fontSize={14}
									color={colors.white}
									bold
								/>
								<FRIcon name="download" size={20} color={colors.white} style={{ fontWeight: 'bold'}} />
							</View>
						}
						
					</LinearGradient>
				</TouchableOpacity>
				
				<TouchableOpacity style={{
					flex: 1,
					justifyContent: 'center',
					alignItems: 'center'
				}}>
					<MdIcon name="chevron-right" size={25} color={colors.primary} />
				</TouchableOpacity>
			</View>
		</View>
	)
}

export default LayoutCandidateList;