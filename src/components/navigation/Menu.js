/*
  Component Menu Navigation
  Passing Props to Use this component
  Created : 28 October 2019       Author : Risman Abdilah
  
  Props : 
	- navigation			->	Value must be passing Navigation Stack

	To Using This Component Follow this Code :

	<Menu
		navigation={this.props.navigation}
	/>

    Like Your Coding, Like Your Better Future
 */

import React, { Component } from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Platform } from 'react-native'
import IonIcon from 'react-native-vector-icons/Ionicons'
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import colors from '../style/Colors'

class Menu extends Component {

	state = {
		menu: 1
	}

	render() {
		const { routeName } = this.props.navigation.state
		return (
			<View style={styles.container}>
				<TouchableOpacity
					onPress={() => this.onPressMenu(1)}
					style={[styles.Menu]}>
					<MdIcon
						name="home"
						color={routeName === "home" ? colors.primary : colors.secondary}
						size={30}
					/>
					<Text style={[styles.LabelMenu, routeName === "home" ? { color: colors.primary, fontWeight: 'bold' } : {}]}>Home</Text>
				</TouchableOpacity>
				<TouchableOpacity
					onPress={() => this.onPressMenu(2)}
					style={[styles.Menu]}>
					<IonIcon
						name="ios-briefcase"
						color={routeName === "Jobs" ? colors.primary : colors.secondary}
						size={30}
					/>
					<Text style={[styles.LabelMenu, routeName === "Jobs" ? { color: colors.primary, fontWeight: 'bold' } : {}]}>Job List</Text>
				</TouchableOpacity>
				<TouchableOpacity
					onPress={() => this.onPressMenu(3)}
					style={[styles.Menu]}>
					<IonIcon
						name="ios-send"
						color={routeName === "password" ? colors.primary : colors.secondary}
						size={30}
					/>
					<Text style={[styles.LabelMenu, routeName === "password" ? { color: colors.primary, fontWeight: 'bold' } : {}]}>Job Applied</Text>
				</TouchableOpacity>
				<TouchableOpacity
					onPress={() => this.onPressMenu(4)}
					style={[styles.Menu]}>
					<IonIcon
						name="md-person"
						color={routeName === "profile" ? colors.primary : colors.secondary}
						size={30}
					/>
					<Text style={[styles.LabelMenu, routeName === "password" ? { color: colors.primary, fontWeight: 'bold' } : {}]}>Profile</Text>
				</TouchableOpacity>
			</View>
		)
	}

	/*
		@function Handling Click New Menu
	 */
	onPressMenu(menu) {
		this.setState({
			menu: menu
		})
		const { navigate, isFocused } = this.props.navigation
		if (menu === 1) {
			navigate('home')
			isFocused()
		} else if (menu === 2) {
			navigate('Jobs')
			isFocused()
		} else if (menu === 3) {
			navigate('password')
			isFocused()
		} else {
			navigate('profile')
			isFocused()
		}
	}
}

export default Menu

const styles = StyleSheet.create({
	container: {
		backgroundColor: colors.white,
		flexDirection: 'row',
		elevation: 25,
		borderTopColor: Platform.OS === 'ios' ? colors.gray : colors.white,
		borderTopWidth: Platform.OS === 'ios' ? 1 : 0,
	},
	Menu: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		paddingVertical: 5,
	},
	LabelMenu: {
		fontSize: 12,
		marginTop: -2,
		fontFamily: 'DIN-Medium',
		color: colors.darkScale
	}
})