import React from 'react'
import { View, Text, Image } from 'react-native'

import myStyles from '../style/'
import styles from '../utils/styles'

class LoadingSpinner extends React.Component {
  _isMounted = false;

  state = {
    x: 0
  }

  componentDidMount() {
    this._isMounted = true
    this.timer = setInterval(() => {
      if (this._isMounted === true) {
        this.setState({
          x: this.state.x + 10
        })
        // console.log('Ok')
      }
    }, 20)
  }

  componentWillUnmount() {
    this._isMounted = false;
    clearInterval(this.timer)
  }

  render() {
    const { x } = this.state
    return (
      <View style={[myStyles.Flex1, myStyles.Center]}>
        <View style={[styles.Image, { paddingVertical: 15 }]}>
          <Image
            source={require('../icons/icon_loading.png')}
            style={[myStyles.ImgContain, {
              transform: [{ rotate: x.toString() + 'deg' }]
            }]}
          />
        </View>
        <Text style={styles.Title}>
          {this.props.text ? this.props.text : 'Loading ...'}
        </Text>
      </View>
    )
  }
}

export default LoadingSpinner;
