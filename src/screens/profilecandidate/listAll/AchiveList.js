import React, { Component } from 'react'
import {
	View,
	Text,
	ScrollView,
	Dimensions,
	SafeAreaView,
	StyleSheet,

} from 'react-native'
import Header from '../../../components/header/index'
import HeaderImage from '../../../components/profile/HeaderImage'
import myStyles from '../../../components/style'
import colors from '../../../components/style/Colors'
import List from './List'
import { connect } from 'react-redux'
import LogoUser from '../../../components/profile/LogoUser'
let AndroidHeight = Dimensions.get('window').height

class Achive extends Component {

	state = {
		isDelete: false,
		scroll: true,
		showModal: false,
		achiveAddModal: false,
		achiveDetailModal: false,
		achiveEditModal: false,
		userToken: '',
		typeDelete: null,
		data: ['Indonesia', 'Sunda', 'Betawi', 'Ngapak', 'Melayu'],
		modalAchievements: false,
		detailAchi:{},
	}
	detailAchievements = async (data) => {
		await this.setState({detailAchi:data})
		this.setState({ modalAchievements: true })
		// console.log(this.state.detailAchi)
	}

	render() {
		return (
			<SafeAreaView style={myStyles.Flex1}>
				<View style={myStyles.Flex1}>
					<Header
						logo={true}
						onpress={() => this.props.navigation.goBack()}
					/>
					<ScrollView
						showsVerticalScrollIndicator={false}
						scrollEnabled={this.state.scroll}
					>
						<HeaderImage
							fullName={
								this.props.candidate.data_candidate.cv_unlocked ? this.props.candidate.data_candidate.user.full_name : "[Name Hidden]"
							}
							major={
								this.props.candidate.data_candidate.last_education.major.label
							}
							education={
								this.props.candidate.data_candidate.last_education.school.label
							}
						/>
						<LogoUser image={this.props.candidate.data_candidate.avatar} />
						<View style={[myStyles.ProfileSection, { minHeight: AndroidHeight - 281 }]}>
							<View style={{ flex: 1, marginTop: 15, marginBottom: 5, borderRadius: 10, backgroundColor: colors.white, }}>
								<View style={styles.Header}>
									<Text style={[styles.Title, myStyles.FontBold]}>
										Achievements</Text>
								</View>
								<View style={[{ flex: 1, paddingVertical: 10, paddingHorizontal: 20, borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }]}>
									{
										this.props.candidate.achivements_list.map((item, idx) => (
											<List
												key={idx}
												click={()=>this.detailAchievements(item)}
												Title={item.name}
												TextDesc={item.title}
												Years={item.start_date.split('-')[0]}
											/>
										))
									}
								</View>
							</View>
						</View>
					</ScrollView>
					<DetailAchievements
						// data={this.state.detailLanguage}
						data={this.state.detailAchi}
						isVisible={this.state.modalAchievements}
						onClose={() => this.setState({ modalAchievements: false })}
					/>
				</View>
			</SafeAreaView>
		)
	}
}

const mapStateToProps = state => ({
	candidate: state.candidate
})

export default connect(mapStateToProps)(Achive);

const styles = StyleSheet.create({
	Header: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		width: '100%',
		paddingVertical: 15,
		paddingHorizontal: 20,
		borderBottomWidth: .7,
		borderBottomColor: colors.gray,
	},
	Title: {
		fontSize: 18,
		color: colors.primary,
		fontWeight: 'bold',
	}
})
