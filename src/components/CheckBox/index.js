import React from 'react'
import { TouchableOpacity, StyleSheet } from 'react-native'
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'

// Styles
import colors from '../style/Colors'

const CheckBox = (props) => {
    return (
        <TouchableOpacity 
            style={[styles.CheckBox, {
                width: props.width ? props.width : 25,
                height: props.height ? props.height : 25,
                borderColor: props.value ? colors.white : colors.primary, 
                backgroundColor: props.value ? colors.primary : colors.white,
                marginHorizontal: props.marginHorizontal ? props.marginHorizontal : 10,
            }]} 
            onPress={props.onValueChange}
            disabled={props.disabled}
        >
            {
                props.value &&
                <MdIcon 
                    name="check" 
                    color={colors.white} 
                    size={props.iconSize ? props.iconSize : 20} />
            }
        </TouchableOpacity>
    )
}

export default CheckBox;

const styles = StyleSheet.create({
    CheckBox: {
        borderWidth: 1, 
        borderRadius: 4,
        marginVertical: 4,
        justifyContent: 'center', 
        alignItems: 'center',
        shadowColor: colors.primary,
    }
})