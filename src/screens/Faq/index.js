import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  BackHandler,
  ActivityIndicator,
  ScrollView,
  SafeAreaView,
  Dimensions
} from 'react-native'

// Components
import Header from '../../components/header'
import HeaderSearch from '../../components/faq/header'
import myStyles from '../../components/style'
import colors from '../../components/style/Colors'
import Ionicons from 'react-native-vector-icons/Ionicons'
import AsyncStorage from '@react-native-community/async-storage'
import { getCategoryFaq } from '../../_redux/_actions/employer'
import { connect } from 'react-redux'
import SectionFaq from '../../components/faq/sectionFaq'
const ScreenWidth = Dimensions.get('window').width;

class Faq extends Component {

  constructor(props) {
    super(props);
    this.state = {
      employerToken: ''
    }
  }

  async componentDidMount() {
    const token = await AsyncStorage.getItem('employerToken');
    this.setState({
      employerToken: token
    })
    try {
      await this.props.getCategoryFaq(token)
    } catch (error) {
      console.log(error)
    }
  }

  render() {
    return (
      <SafeAreaView style={myStyles.Flex1}>
        <View style={myStyles.Flex1}>
          <Header
            logo={true}
            onpress={() => this.props.navigation.goBack()}
          />
          <ScrollView>
            <HeaderSearch />
            <View style={myStyles.Flex1, { padding: '5%' }}>
              <Text style={[myStyles.FontBold, { fontSize: 17, color: colors.darkScale }]}>Select a category to browse relevant questions</Text>
              {
                this.props.employer.loading ?
                  <View style={{ justifyContent: 'center', alignItems: 'center', height: 300 }}>
                    <ActivityIndicator size="large" color={colors.primary} />
                  </View>
                  :
                  <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 10 }}>
                    {
                      this.props.employer.faq.category.map((data, i) => {
                        const { employerToken } = this.state
                        return (
                          <SectionFaq
                            key={i}
                            name={data.name}
                            onPressed={() => this.props.navigation.navigate('faqcategory', { dataId: data.id, employerToken: employerToken })}
                          />
                        )
                      })
                    }
                  </View>
              }
              <Text style={[myStyles.FontBold, { marginTop: 20, fontSize: 17, color: colors.darkScale }]}>
                Need more help ? Contact our support  staff bellow!
              </Text>
              <View style={styles.contact}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <View style={[styles.circleIcon, { backgroundColor: colors.pink, paddingHorizontal: 15 }]}>
                    <Ionicons name="ios-phone-portrait" color={colors.white} size={30} />
                  </View>
                  <Text style={[myStyles.FontBold, styles.textContact]}>
                    081234567890&nbsp;
                    <Text style={{ fontSize: 15, fontWeight: 'normal' }}>[Mon-Fri], 09:00 - 17:00</Text>
                  </Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                  <View style={[styles.circleIcon, { backgroundColor: colors.pink }]}>
                    <Ionicons name="ios-mail" color={colors.white} size={30} />
                  </View>
                  <Text style={[myStyles.FontBold, styles.textContact]}>hanang@career.support</Text>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    )
  }
}

const mapStateToProps = state => ({
  employer: state.employer
})

const mapDispatchToProps = dispatch => ({
  getCategoryFaq: token => dispatch(getCategoryFaq(token))
})

export default connect(mapStateToProps, mapDispatchToProps)(Faq);

const styles = StyleSheet.create({
  circleIcon: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#4782c3',
    borderRadius: 100,
    width: 50,
    height: 50,
    paddingVertical: 7,
    paddingHorizontal: 10
  },
  contact: {
    width: '100%',
    backgroundColor: colors.white,
    borderWidth: 2,
    borderColor: colors.gray,
    marginTop: 15,
    borderRadius: 6,
    paddingVertical: 15,
    paddingHorizontal: 10,
  },
  textContact: {
    marginStart: 10,
    fontSize: 20,
    fontWeight: "bold",
    color: colors.darkSecond
  },
})