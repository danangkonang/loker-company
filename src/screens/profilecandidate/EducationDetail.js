import React from 'react'
import {
	View,
	Text,
	TouchableOpacity,
	Dimensions,
	StyleSheet
} from 'react-native'
import Modal from "react-native-modal"
import IonIcon from 'react-native-vector-icons/Ionicons'
import AntDesign from 'react-native-vector-icons/AntDesign'
import colors from '../../components/style/Colors'
// import { modifyDateFormat, modifyMonth } from '../../../components/utils'
import myStyles from '../../components/style'
let AndroidHeight = Dimensions.get('window').height

class ShowWorkExperience extends React.Component {
	render() {
		const {
			isVisible,
			onClose,
			data
		} = this.props
		console.log(this.props.data.id)
		return (
			<Modal
				animationIn='zoomIn'
				animationOut='slideOutDown'
				animationInTiming={1000}
				backdropTransitionOutTiming={800}
				backdropTransitionInTiming={800}
				isVisible={isVisible}
	
			>
				<View style={styles.Modal}>
					<View style={styles.ModalHeader}>
						<View style={{ flex: 1, paddingLeft: 10 }}>
							<Text style={[{ 
								color: colors.primary, 
								fontWeight: 'bold', 
								fontSize: 16 ,
							},myStyles.FontBold]}>
								Education
							</Text>
						</View>
						
						<View style={{ width: 35, alignItems: 'center' }}>
							<TouchableOpacity
								onPress={onClose}
							>
								<IonIcon name="md-close" size={25} color={colors.darkScale} />
							</TouchableOpacity>
						</View>
					</View>
					<View style={{ paddingHorizontal: 15 }}>
						<View style={{ marginBottom: 10 }}>
							<Text style={[{ fontWeight: 'bold', color: colors.darkScale },myStyles.FontBold]}>Scholl type</Text>
							<Text style={[{ color: '#aaa' },myStyles.FontLight]}>
								{data.title}
							</Text>
						</View>
						<View style={{ marginBottom: 10 }}>
							<Text style={[{ fontWeight: 'bold', color: colors.darkScale },myStyles.FontBold]}>Scholl Name</Text>
							<Text style={[{ color: '#aaa' },myStyles.FontLight]}>
							{data.name}
							</Text>
						</View>
						<View style={{ marginBottom: 10 }}>
							<Text style={[{ fontWeight: 'bold', color: colors.darkScale },myStyles.FontBold]}>Degree</Text>
							<Text style={[{ color: '#aaa' },myStyles.FontLight]}>
								{data.degree}
							</Text>
						</View>
                        {/* <View style={{ marginBottom: 10 }}>
							<Text style={[{ fontWeight: 'bold', color: colors.darkScale },myStyles.FontBold]}>Major</Text>
							<Text style={[{ color: '#aaa' },myStyles.FontLight]}>
								major
							</Text>
						</View> */}
                        <View style={{ marginBottom: 10 }}>
							<Text style={[{ fontWeight: 'bold', color: colors.darkScale },myStyles.FontBold]}>Major</Text>
							<Text style={[{ color: '#aaa' },myStyles.FontLight]}>
								{data.major}
							</Text>
						</View>
                        <View style={{ marginBottom: 10 }}>
							<Text style={[{ fontWeight: 'bold', color: colors.darkScale },myStyles.FontBold]}>GPA</Text>
							<Text style={[{ color: '#aaa' },myStyles.FontLight]}>
								{data.nim}
							</Text>
						</View>

						<View style={{ marginBottom: 10, flexDirection: 'row' }}>
							<View style={{ marginRight: 15 }}>
								<Text style={[{ fontWeight: 'bold', color: colors.darkScale },myStyles.FontBold]}>From</Text>
								<Text style={[{ color: '#aaa' },myStyles.FontLight]}>
									{data.start_date}
								</Text>
							</View>
							<View style={{ marginRight: 10 }}>
								<Text style={[{ fontWeight: 'bold', color: colors.darkScale },myStyles.FontBold]}>Until</Text>
								<Text style={[{ color: '#aaa' },myStyles.FontLight]}>
									{data.end_date}
								</Text>
							</View>
						</View>
					</View>
				</View>
			</Modal>
		)
	}
}


export default ShowWorkExperience

const styles = StyleSheet.create({
	Modal: {
		backgroundColor: colors.white, 
		borderRadius: 10, 
		paddingTop: 10, 
		paddingBottom: 10,
	},
	ModalHeader: {
		height: 35, 
		flexDirection: 'row', 
		alignItems: 'center', 
		paddingHorizontal: 5,
	}
})