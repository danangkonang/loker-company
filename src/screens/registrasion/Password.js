import React, { Component } from 'react'
import {
	View,
	Text,
	ScrollView,
	Image,
	StyleSheet,
	StatusBar,
	Platform
} from 'react-native'
import HeaderStep from '../../components/header/HeaderStep'
import Btn from '../../components/button'
import color from '../../components/style/Colors'
import myStyles from '../../components/style'
import PasswordInput from '../../components/customTextInput/Password'
import { connect } from 'react-redux'
import ErrorText from '../../components/customTextInput/Error'
import { employerAuthentication } from '../../_redux/_actions/employer'
import AsyncStorage from '@react-native-community/async-storage'
class Password extends Component {

	constructor(props) {
		super(props);
		this.state = {
			errPass: false,
			errConfPass: false,
			pass: '',
			confPass: '',
			msgPass: '',
			msgConfpass: '',
			hidePassword: true,
			hideConfirm: true,
			id: '',
			isSave:false
		}
	}

	change = (v) => {
		this.setState({ errPass: false })
		this.setState({ pass: v })
	}

	render() {
		return (
			<View style={{ flex: 1 }}>
				<StatusBar backgroundColor="white" barStyle="dark-content" />
				<ScrollView contentContainerStyle={{ paddingTop: 5 }}>
					<HeaderStep
						colorNonActive={color.cyan}
						colorActive={color.primary}
						step={[true, true, true, false, false, false, false]}
					/>
					<View style={{ alignItems: 'center', marginTop: 25 }}>
						<Text style={[myStyles.FontBold, { color: color.primary, fontSize: 25 }]}>Welcome to</Text>
					</View>
					<View style={{ width: '100%', height: 50, paddingHorizontal: '15%', marginBottom: 30 }}>
						<Image source={require('../../assets/img/logo_cs.png')} style={{ flex: 1, width: undefined, height: undefined, resizeMode: 'contain' }} />
					</View>
					<View style={{ alignItems: 'center', marginBottom: 25 }}>
						<Text style={[myStyles.FontLight, { color: color.darkScale }]}>Create password for your account  </Text>
						<Text style={[myStyles.FontMedium, { color: color.darkScale, fontWeight: 'bold' }]}>minimum 8 characters.</Text>
						<Text style={[myStyles.FontLight, { color: color.darkScale, textAlign: 'center' }]}>This will also let you login to your account {"\n"}on your pc through the www.career.support</Text>
					</View>
					<View style={{ alignItems: 'center' }}>
						<PasswordInput
							width='70%'
							borderWidth={Platform.OS === 'ios' ? 2 : 1}
							borderRadius={5}
							password={this.state.hidePassword}
							value={this.state.pass}
							isError={this.state.errPass}
							placeholder="Create Password"
							onChangeText={(value) => this.setState({ pass: value, errPass: false })}
							onClick={() => this.setState({ hidePassword: !this.state.hidePassword })}
						/>
						{
							this.state.errPass == true ?
								<ErrorText
									width='65%'
									text={this.state.msgPass}
								/> : null
						}
						<PasswordInput
							width='70%'
							borderWidth={Platform.OS === 'ios' ? 2 : 1}
							borderRadius={5}
							password={this.state.hideConfirm}
							value={this.state.confPass}
							isError={this.state.errConfPass}
							placeholder="Confirm Password"
							onChangeText={(value) => this.setState({ confPass: value, errConfPass: false })}
							onClick={() => this.setState({ hideConfirm: !this.state.hideConfirm })}
						/>
						{
							this.state.errConfPass == true ?
								<ErrorText
									width='65%'
									text={this.state.msgConfpass}
								/> : null
						}
					</View>
					<View style={{ alignItems: 'center', paddingTop: 30 }}>
						<Btn
							text="Next"
							height={50}
							backgroundColor={"#4682c4"}
							color={"#fff"}
							width='45%'
							onclick={this.registrasi}
							isSave={this.state.isSave}
							disabled={this.state.isSave}
						/>
					</View>
				</ScrollView>
			</View>
		)
	}

	registrasi = () => {
		const { pass, confPass } = this.state
		if (pass === '') {
			this.setState({ errPass: true, msgPass: 'Required' })
		} else {
			if (pass.length < 8) {
				this.setState({ errPass: true, msgPass: 'Min length 8 caracter' })
			} else {
				if (confPass === '') {
					this.setState({ errConfPass: true, msgConfpass: 'Required' })
				} else {
					if (confPass.length < 8) {
						this.setState({ errConfPass: true, msgConfpass: 'Min length 8 caracter' })
					} else {
						if (pass !== confPass) {
							this.setState({ errConfPass: true, msgConfpass: 'Confirm password not mach' })
						} else {
							// alert('tes')
							this.savePassword()
						}
					}
				}
			}
		}
	}

	savePassword = async () => {
		const { pass } = this.state
		try {
			this.setState({ isSave: true })
			const res = await fetch(`http://128.199.224.138:8080/api/v1/authentication/employer/register/verify/`, {
				method: 'POST',
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					password: pass,
					token: this.props.employer_registration.token,
					email: this.props.employer_registration.email,
					phone: this.props.employer_registration.phone
				})
			})
			const response = await res.json()
			if (response.token) {
				await this.props.dispatch(employerAuthentication(this.props.employer_registration.email, this.state.pass))

				let { token } = this.props.employer.employer_data
				AsyncStorage.setItem('employerToken', token)
				this.setState({ isSave: false })
				this.props.navigation.navigate('companyAccount')
			} else if (response.non_field_errors[0]) {
				this.setState({ isSave: false, errPass: true, msgPass: 'This password is too common.' })
			} else {
				this.setState({ isSave: false })
				alert(('gak tau'))
			}
		} catch (e) {
			this.setState({ isSave: false })
			console.log('error')
			console.log(e)
		}
	}

	login = async () => {
		try {
			const res = await fetch(`http://128.199.224.138:8080/api/v1/authentication/employer/`, {
				method: 'POST',
				body: JSON.stringify({
					username: this.props.employer_registration.email,
					password: this.state.pass
				}),
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json',
				}
			})
			const response = await res.json()
			const token = response.token
			if (token) {
				AsyncStorage.setItem('userToken', token)
				this.props.navigation.navigate('companyAccount')
				console.log("oke login", response)
			} else {
				console.log('gagal login', response)
			}
		} catch (e) {
			console.log(('ini error bos', e))
		}

	}

}

const mapStateToProps = (state) => {
	return {
		employer_registration: state.employer.employer_registration,
		employer: state.employer
	}
}

export default connect(mapStateToProps)(Password)

const style = StyleSheet.create({
	msgErr: { minHeight: 10, backgroundColor: '#DC143C', paddingHorizontal: 10, width: '74%', marginTop: -4, borderBottomLeftRadius: 5, borderBottomRightRadius: 5, marginBottom: 5, alignItems: 'flex-start', justifyContent: 'center', alignItems: 'center', paddingVertical: 3 }
})