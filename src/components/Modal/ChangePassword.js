import React, { Component } from 'react'
import { View, Text, TouchableOpacity, ScrollView, StyleSheet, Dimensions, Platform, Alert, ToastAndroid } from 'react-native'

import { connect } from 'react-redux'
import AsyncStorage from '@react-native-community/async-storage'

import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'

// Components
import ErrorText from '../customTextInput/Error'
import Button from '../button'
import PasswordInput from '../customTextInput/Password'

// Styles
import colors from '../style/Colors'
import myStyles from '../style'
import { changePassword } from '../../_redux/_actions/employer'

// Dimensions
let AndroidHeight = Dimensions.get('window').height
let AndroidWidth = Dimensions.get('window').width

class ChangePassword extends Component {
	constructor() {
		super()
		this.state = {
			hideOldPassword: true,
			oldPassword: '',
			errorOldPassword: false,
			errorOldPasswordText: '',
			hideNewPassword: true,
			newPassword: '',
			errorNewPassword: false,
			errorNewPasswordText: '',
			hideConfirmPassword: true,
			confirmPassword: '',
			errorConfirmPassword: false,
			errorConfirmPasswordText: '',
		}
	}

	render() {
		const {
			hideOldPassword,
			oldPassword,
			errorOldPassword,
			errorOldPasswordText,
			hideNewPassword,
			newPassword,
			errorNewPassword,
			errorNewPasswordText,
			hideConfirmPassword,
			confirmPassword,
			errorConfirmPassword,
			errorConfirmPasswordText
		} = this.state

		const { visible } = this.props

		if (visible) {
			return (
				<View style={styles.Container}>
					<View style={{
						flex: 1,
						paddingTop: 100,
						paddingHorizontal: 10,
						justifyContent: 'center'
					}}>
						<ScrollView showsVerticalScrollIndicator={false}>
							<View style={styles.Card}>
								<View style={styles.Header}>
									<Text style={[myStyles.FontBold, styles.TextHead]}>
										Change your password
								</Text>
									<TouchableOpacity style={{
										padding: 5,
										margin: -5
									}} onPress={this.props.hideModal}>
										<MdIcon name="close" size={25} color={colors.primary} />
									</TouchableOpacity>
								</View>
								<View style={styles.Form}>
									<Text style={[styles.FormText, myStyles.FontMedium, {
										marginVertical: 10,
									}]}>
										Enter your current password
								</Text>
									<PasswordInput
										borderWidth={1}
										password={hideOldPassword}
										value={oldPassword}
										noElevation
										borderWidth={2}
										placeholder="Current password"
										onChangeText={(oldPassword) => this.setState({ oldPassword, errorOldPassword: false })}
										onClick={() => this.setState({ hideOldPassword: !hideOldPassword })}
										marginBottom={5}
									/>
									{
										errorOldPassword &&
										<ErrorText
											marginLeft={6}
											width='100%'
											text={errorOldPasswordText}
										/>
									}
									<Text style={[styles.FormText, myStyles.FontMedium, {
										marginTop: 25,
										marginBottom: 10,
									}]}>
										Create your new password
								</Text>
									<PasswordInput
										borderWidth={1}
										password={hideNewPassword}
										value={newPassword}
										noElevation
										borderWidth={2}
										placeholder="New password"
										onChangeText={(newPassword) => this.setState({ newPassword, errorNewPassword: false })}
										onClick={() => this.setState({ hideNewPassword: !hideNewPassword })}
										marginBottom={5}
									/>
									{
										errorNewPassword &&
										<ErrorText
											marginLeft={6}
											width='100%'
											text={errorNewPasswordText}
										/>
									}
									<PasswordInput
										borderWidth={1}
										password={hideConfirmPassword}
										value={confirmPassword}
										noElevation
										borderWidth={2}
										placeholder="Confirm new password"
										onChangeText={(confirmPassword) => this.setState({ confirmPassword, errorConfirmPassword: false })}
										onClick={() => this.setState({ hideConfirmPassword: !hideConfirmPassword })}
										marginBottom={5}
									/>
									{
										errorConfirmPassword &&
										<ErrorText
											marginLeft={6}
											width='100%'
											text={errorConfirmPasswordText}
										/>
									}
									<View style={styles.Button}>
										<Button
											width='50%'
											backgroundColor={colors.primary}
											color={colors.white}
											text="Change"
											onclick={() => this._changePasswordButton()}
										/>
									</View>
								</View>
							</View>
						</ScrollView>
					</View>
				</View>
			)
		}

		return false
	}

	/**
	 * @function _validationOldPassword to Validate Old Password
	 */
	_validationOldPassword = (oldPassword) => {
		if (oldPassword === '') {
			this.setState({
				errorOldPassword: true,
				errorOldPasswordText: 'Enter old password!'
			})
		} else {
			this.setState({
				errorOldPassword: false,
				errorOldPasswordText: ''
			})
			return true
		}

		return false
	}

	/**
	 * @function _validationNewPassword to Validate Input New Password
	 */
	_validationNewPassword = (password) => {
		if (password === '') {
			this.setState({
				errorNewPassword: true,
				errorNewPasswordText: 'Enter new password!'
			})
		} else if (password.length < 8) {
			this.setState({
				errorNewPassword: true,
				errorNewPasswordText: `Password can't be less than 8 characters`
			})
		} else {
			this.setState({
				errorNewPassword: false,
				errorNewPasswordText: ''
			})
			return true
		}

		return false
	}

	/**
	 * @function _validationConfirmPassword
	 */
	_validationConfirmPassword = (confirmPassword, password) => {

		if (confirmPassword === '') {
			this.setState({
				errorConfirmPassword: true,
				errorConfirmPasswordText: '"Enter confirmation password!'
			})
		} else if (password != confirmPassword) {
			this.setState({
				errorConfirmPassword: true,
				errorConfirmPasswordText: 'Password does not match!'
			})
		} else {
			this.setState({
				errorConfirmPassword: false,
				errorConfirmPasswordText: ''
			})
			return true
		}

		return false
	}

	_changePasswordButton = async () => {
		const employerToken = await AsyncStorage.getItem('employerToken')
		const { oldPassword, newPassword, confirmPassword } = this.state
		/**
		 * Validate Form oldPassword,
		 * Validate Form newPassword,
		 * Validate Form confirmPassword
		 */
		if (this._validationOldPassword(oldPassword)) {
			if (this._validationNewPassword(newPassword)) {
				if (this._validationConfirmPassword(confirmPassword, newPassword)) {
					try {
						const data = {
							oldPassword: this.state.oldPassword,
							newPassword: this.state.newPassword,
							token: employerToken,
						}
						await this.props.changePassword(data);
						this.setDefaultState();
						if (Platform.OS == 'ios') {
							Alert.alert(
								'Success',
								'Password has been changed',
								[{
									"text":'OK',
									style:'cancel'
								}],
								{
									cancelable:false
								}
							)
						} else {
							ToastAndroid.show('Password has been changed', ToastAndroid.SHORT);
						}
						this.props.hideModal()
					} catch (error) {
						console.log(error.response)
						if (error.response.data.en == "The password you're typed in is matching the old password. Please type in new password.") {
							this.setState({
								errorNewPassword: true,
								errorNewPasswordText: 'New password should not be same with current password',
								errorConfirmPassword: true,
								errorConfirmPasswordText: 'New password should not be same with current password'
							})
						} else if (error.response.data[0] == "{'en': 'Wrong current password', 'id': 'Password lama salah'}") {
							this.setState({
								errorOldPassword: true,
								errorOldPasswordText: 'Wrong current password!'
							})
						} else if (error.response.data.non_field_errors[0] == "This password is too common.") {
							this.setState({
								errorNewPassword: true,
								errorNewPasswordText: 'This password is too common',
								errorConfirmPassword: true,
								errorConfirmPasswordText: 'This password is too common'
							})
						} else if (error.response.status === 406) {
							this.setState({
								errorNewPassword: true,
								errorNewPasswordText: 'Password should not same!'
							})
						}
					}
				}
			}
		}
	}

	setDefaultState = () => {
		this.setState({
			hideOldPassword: true,
			oldPassword: '',
			errorOldPassword: false,
			errorOldPasswordText: '',
			hideNewPassword: true,
			newPassword: '',
			errorNewPassword: false,
			errorNewPasswordText: '',
			hideConfirmPassword: true,
			confirmPassword: '',
			errorConfirmPassword: false,
			errorConfirmPasswordText: '',
		})
	}
}

const mapDispatchToProps = dispatch => ({
	changePassword: data => dispatch(changePassword(data))
})

export default connect(null, mapDispatchToProps)(ChangePassword);

const styles = StyleSheet.create({
	Container: {
		position: 'absolute',
		width: AndroidWidth,
		height: '100%',
		backgroundColor: "rgba(0,0,0,0.6)",
		padding: 10
	},
	Card: {
		flex: 1,
		backgroundColor: colors.white,
		borderRadius: 8,
		borderWidth: .5,
		borderColor: colors.darkScale,
		elevation: 10,
		shadowColor: colors.darkSecond,
		shadowRadius: 5,
		shadowOpacity: 2.25,
		overflow: 'hidden'
	},
	Header: {
		width: '100%',
		paddingHorizontal: 15,
		paddingVertical: 8,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		borderBottomColor: colors.gray,
		borderBottomWidth: .6,
	},
	TextHead: {
		color: colors.primary,
		fontWeight: 'bold',
		fontSize: 16,
	},
	Form: {
		minHeight: 400,
		paddingTop: 10,
		paddingHorizontal: 15
	},
	FormText: {
		fontSize: 15,
		color: colors.darkScale,
		paddingHorizontal: 5,
	},
	Button: {
		flex: 1,
		paddingVertical: 20,
		justifyContent: 'center',
		alignItems: 'center',
		paddingVertical: 5,
	}
})