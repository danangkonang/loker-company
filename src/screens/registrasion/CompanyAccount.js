import React, { Component } from 'react'
import {
	View,
	Text,
	ScrollView,
	StyleSheet,
	TouchableOpacity,
	Image,
	Dimensions,
	StatusBar
} from 'react-native'
import HeaderStep from '../../components/header/HeaderStep'
import colors from '../../components/style/Colors'
import Input from '../../components/customTextInput'
import Button from '../../components/button'
import myStyles from '../../components/style'
import IconAnt from 'react-native-vector-icons/AntDesign'
import ImagePicker from 'react-native-image-crop-picker'
import Modal from "react-native-modal"
import AsyncStorage from '@react-native-community/async-storage'
import ErrorText from '../../components/customTextInput/Error'
const { height } = Dimensions.get('window')
export default class App extends Component {
	state = {
		isModalVisible: false,
		image: null,
		companyName: '',
		industriType: '',
		industri: '',
		errCompanyName: false,
		errIndustriType: false,
		msgErrName: '',
		msgErrType: '',
		token: '',
		type: [],
		hasil: [],
		isSave: false
	}

	validasi = () => {
		if (this.state.companyName == '') {
			this.setState({ errCompanyName: true, msgErrName: 'Required' })
		} else {
			if (this.state.industri == '') {
				this.setState({ errIndustriType: true, msgErrType: 'Required' })
			} else {
				this.registrasiCompany()
			}
		}
	}

	cekdobleName = async (key) => {
		try {
			const res = await fetch(`http://128.199.224.138:8080/api/v1/company/registration/check/`, {
				method: 'POST',
				headers: {
					'Authorization': `JWT ${this.state.token}`,
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					company_name: key
				})
			})
			let response = await res.json()
			// console.log(response)
			if (response.is_exists == true) {
				this.setState({ errCompanyName: true, msgErrName: 'company is exist' })
			} else {
				this.setState({ errCompanyName: false })
			}
		} catch (e) {
			console.log(e)
		}
	}

	registrasiCompany = async () => {
		this.setState({ isSave: true })
		const res = await fetch(`http://128.199.224.138:8080/api/v1/company/registration/employer_name/`, {
			method: 'PATCH',
			headers: {
				'Authorization': `JWT ${this.state.token}`,
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				company_name: this.state.companyName,
				industry: this.state.industri,
			})
		})
		let response = await res.json()
		// console.log(response)
		this.setState({ isSave: false })
		this.props.navigation.navigate('companyPage')
	}

	componentDidMount = async () => {
		// await this.login()
		await this.getToken()
		await this.getCompanyType()
		this.getProfil()
	}

	getProfil = async () => {
		let res = await fetch(`http://128.199.224.138:8080/api/v1/company/user/profile/1007/`, {
			method: 'GET',
			headers: {
				'Authorization': `JWT ${this.state.token}`
			}
		})
		let respon = await res.json()
		// console.log(respon.user_employer_personals.employer.company_logo)
	}

	getCompanyType = async () => {
		try {
			let res = await fetch(`http://128.199.224.138:8080/api/v1/company/profile/industry/`, {
				method: 'GET',
				headers: {
					'Authorization': `JWT ${this.state.token}`
				}
			})
			let respon = await res.json()
			// console.log(respon)
			this.setState({ type: respon })
		} catch (e) {
			console.log('dlancok', e)
		}
	}

	getToken = async () => {
		let tok = await AsyncStorage.getItem('employerToken')
		this.setState({ token: tok })
		// console.log(tok)
	}

	serchType = (v) => {
		if (v == '') {
			this.setState({ hasil: [] })
		} else {
			const d = this.state.type;
			const regex = new RegExp(`${v.trim()}`, 'i')
			this.setState({ hasil: d.filter(x => x.name.search(regex) >= 0) })
		}
	}

	renderImage(image) {
		return (
			<TouchableOpacity onPress={this.modalUpload}>
				<Image style={{ width: 120, height: 120, resizeMode: 'contain', borderRadius: 60 }} source={{ uri: image }} />
			</TouchableOpacity>
		)
	}

	render() {
		const { errCompanyName, errIndustriType, msgErrName, msgErrType, hasil, type } = this.state
		return (
			<View>
				<StatusBar backgroundColor="white" barStyle="dark-content" />
				<ScrollView>
					<View style={{ backgroundColor: '#fff', height: height - 25 }}>
						<HeaderStep
							colorNonActive={colors.cyan}
							colorActive={colors.primary}
							step={[true, true, true, true, false, false, false]}
						/>

						<View style={{ alignItems: 'center', marginTop: 20 }}>
							<Text style={[myStyles.FontBold, { color: colors.primary, fontSize: 25, textAlign: 'center' }]}>Set up {"\n"} company accunt</Text>
						</View>

						<View style={{ alignItems: 'center', paddingTop: 10 }}>
							<View activeOpacity={.7} style={{ backgroundColor: colors.primary, height: 120, width: 120, borderRadius: 60 }}>
								{this.state.image ?
									this.renderImage(this.state.image)
									:
									<TouchableOpacity style={{ paddingTop: 30 }} onPress={this.modalUpload}>
										<Text style={{ textAlign: 'center', fontSize: 15, color: colors.white }}>
											Button add {"\n"}logo
                                    </Text>
										<View style={{ alignItems: 'center', marginTop: 10 }}>
											<IconAnt name="plus" color={colors.white} size={27} />
										</View>
									</TouchableOpacity>
								}
							</View>
						</View>

						<View style={{ alignItems: 'center', marginTop: 10 }}>
							<Input
								width='70%'
								marginBottom={8}
								placeholder="Company name"
								type='emailAddress'
								keyboardType='email-address'
								borderRadius={3}
								shadowRadius={6}
								zIndex={20}
								onchange={(v) => {
									this.setState({ companyName: v, errCompanyName: false })
									this.cekdobleName(v)
								}}
								value={this.state.companyName}
							/>
							{
								errCompanyName &&
								<ErrorText
									text={msgErrName}
								/>
							}
						</View>

						<View style={{ alignItems: 'center', marginTop: 10 }}>
							<Input
								width='70%'
								marginBottom={8}
								placeholder="Select industri type"
								borderRadius={3}
								shadowRadius={6}
								zIndex={20}
								onchange={(v) => {
									this.setState({ industriType: v, errIndustriType: false })
									this.serchType(v)
								}}
								value={this.state.industriType}
							/>
							{
								errIndustriType &&
								<ErrorText
									text={msgErrType}
								/>
							}
							<View style={hasil.length !== 0 ? { width: '70%', marginTop: 3 } : null}>
								{
									hasil.map((i, n) => {
										if (n < 3) {
											return (
												<TouchableOpacity style={{ borderBottomColor: '#fff', borderBottomWidth: 2, paddingLeft: 15, height: 38, justifyContent: 'center', backgroundColor: '#eee' }} key={n}
													onPress={() => {
														this.setState({
															industriType: i.name,
															industri: i.id,
															errIndustriType: false,
															hasil: []
														})
													}}>
													<Text style={{ fontWeight: 'bold', color: '#999' }}>{i.name}</Text>
												</TouchableOpacity>
											)
										}
									})
								}
							</View>
						</View>

						<View style={{ alignItems: 'center' }}>
							<Button
								marginVertical={15}
								width='40%'
								backgroundColor={colors.primary}
								color={colors.white}
								text="Next"
								onclick={this.validasi}
								isSave={this.state.isSave}
								disabled={this.state.isSave}
							/>
						</View>

						<View style={{ alignItems: 'center', paddingTop: 30 }}>
							<TouchableOpacity>
								<Text style={{ fontWeight: 'bold', color: colors.primary }}>Cancel registrasion</Text>
							</TouchableOpacity>
						</View>
					</View>

					<Modal
						animationIn='zoomIn'
						animationOut='slideOutDown'
						animationInTiming={1000}
						backdropTransitionInTiming={800}
						backdropTransitionInTiming={800}
						isVisible={this.state.isModalVisible}>

						<View style={{ backgroundColor: '#fff', borderRadius: 10, paddingTop: 10, paddingBottom: 10, marginHorizontal: 50 }}>
							<View style={{ flexDirection: 'row', justifyContent: 'space-around', height: 50, alignItems: 'center' }}>
								<TouchableOpacity onPress={() => this.pickSingleWithCamera(true)}>
									<IconAnt name='camera' size={40} color='#aaa' />
								</TouchableOpacity>
								<TouchableOpacity onPress={() => this.pickSingleWhiteGalery(true)}>
									<IconAnt name='picture' size={40} color='#aaa' />
								</TouchableOpacity>
							</View>
							<View style={{ padding: 5, alignItems: 'flex-end' }}>
								<TouchableOpacity style={{ width: 100, padding: 10, borderRadius: 5, alignItems: 'center', justifyContent: 'center' }} onPress={this.modalUpload}>
									<Text style={{ color: '#545454', fontWeight: 'bold', fontSize: 18 }}>Cancel</Text>
								</TouchableOpacity>
							</View>
						</View>

					</Modal>

				</ScrollView>
			</View>
		)
	}

	modalUpload = () => {
		this.setState({ isModalVisible: !this.state.isModalVisible })
	}
	pickSingleWhiteGalery(cropit, circular = false, mediaType) {
		ImagePicker.openPicker({
			width: 300,
			height: 300,
			cropping: true,
			includeBase64: true,
		}).then(image => {
			// this.setState({isUpload:true})
			let i = image.path
			let imageName = i.split('/')
			let name = imageName[7]

			const avatar = {
				uri: image.path,
				type: 'image/jpeg',
				name: name
			}
			this.uploadFile(avatar)
		})
			.catch(e => {
				if (e) {
					this.upload()
					this.setState({
						errFile: true,
						msgErrFile: 'format (jpg, jpeg, and png)',
					})
				}
			})
	}

	uploadFile = async (image) => {
		try {
			const formData = new FormData()
			formData.append('company_logo', image)
			const up = await fetch(`http://128.199.224.138:8080/api/v1/company/registration/employer_image/`, {
				method: 'PATCH',
				headers: {
					'Content-Type': 'multipart/form-data',
					'Authorization': `JWT ${this.state.token}`
				},
				body: formData
			})
			let u = await up.json()
			console.log(u)
			this.upload()
			this.setState({ image: u.company_logo })
		} catch (err) {
			console.log(err)
		}
	}

	upload = () => {
		this.setState({ isModalVisible: !this.state.isModalVisible })
	}

	pickSingleWithCamera(cropping, mediaType = 'photo') {
		ImagePicker.openCamera({
			width: 300,
			height: 300,
			cropping: true,
			includeBase64: true
		}).then(image => {
			this.setState({
				image: { uri: image.path, width: image.width, height: image.height, mime: image.mime },
				isModalVisible: false,
			})
		})
	}
}


