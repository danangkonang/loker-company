import React from 'react'
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import colors from '../style/Colors'
import LinearGradient from 'react-native-linear-gradient'

const Header = (props) => {
  return (
    <LinearGradient start={{x: 0, y: 0}} end={{x: 0, y: 1}} colors={['#155396', '#155396']} >
      <View style={[styles.container]}>
        <TouchableOpacity onPress={props.onpress} style={styles.Icon}>
          <Icon name={props.icon ? props.icon : 'chevron-left'} 
                size={props.iconSize ? props.iconSize : 25} 
                color={props.iconColor ? props.iconColor : colors.white}
          />
        </TouchableOpacity>
        {
          props.logo && props.logo === true ?
          <View style={{width: '50%', paddingHorizontal: 14}}>
            <Image style={{width: undefined, height: undefined, flex: 1, resizeMode: 'contain'}} source={require('../icons/logo_white.png')} />
          </View>
          :
          <Text style={[styles.logo, {fontSize: props.titleSize ? props.iconSize : 18}]}>
            {props.title ? props.title : 'Career Support'}
          </Text>
        }
      </View>
    </LinearGradient>
  )
}

export default Header

const styles = StyleSheet.create({
  container: {
    paddingVertical: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  Icon: {
    paddingHorizontal: 12
  },  
  logo: {
    fontWeight: '800',
    color: colors.white,
    paddingHorizontal: 14
  }
})