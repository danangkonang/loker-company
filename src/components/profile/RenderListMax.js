import React from 'react'
import { View, Text, TouchableOpacity, FlatList, Image, StyleSheet } from 'react-native'
import IonIcon from 'react-native-vector-icons/Ionicons'
import color from '../style/Colors'
import myStyles from '../style'
import { modifyMonth } from '../utils'
import MdIcon from 'react-native-vector-icons/MaterialCommunityIcons'

class renderListMax extends React.Component {
	render() {
		const { Title, onView, Logo, date, clickShow, data, yearOnly } = this.props
		let defaultLogo
		if (Logo === 1) {
			defaultLogo = 'https://career.support/candidate/img/photo_placeholder-school.df476433.jpg'
		} else {
			defaultLogo = 'https://career.support/candidate/img/photo_placeholder-company.7f27c701.jpg'
		}
		return (
			<View style={styles.Container}>
				{
					data && data.length > 3 ?
						<TouchableOpacity onPress={onView}>
							<View style={styles.Header}>
								<Text style={[styles.TextHeader, myStyles.FontBold]}>
									{Title}
								</Text>
								{
									data && data.length > 3 ?
										<View style={styles.ViewMore} >
											<Text style={styles.TextViewMore} >
												View {data.length - 3} more</Text>
											<IonIcon name="ios-arrow-forward" size={20} color={color.darkScale} />
										</View>
										:
										null
								}
							</View>
						</TouchableOpacity>
						:
						<View style={styles.Header}>
							<Text style={[styles.TextHeader, myStyles.FontBold]}>
								{Title}
							</Text>
						</View>
				}
				<FlatList
					data={data}
					contentContainerStyle={{ paddingHorizontal: 10 }}
					renderItem={({ item, index }) => {
						let started = new Date(item.start_date).getFullYear()
						let ended = new Date(item.end_date).getFullYear()
						if (index < 3) {
							return (
								<TouchableOpacity style={styles.DataItem}
									key={index}
									onPress={() => {
										clickShow(item)
									}}>
									{
										Logo &&
										<View style={styles.LogoWrap}>
											<View style={styles.Logo}>
												<Image
													source={{ uri: item.logo ? item.logo : defaultLogo }}
													style={{
														flex: 1,
														resizeMode: 'cover',
														borderRadius: 4
													}} />
											</View>
										</View>
									}
									<View style={[styles.Desc, { width: Logo ? '80%' : '90%' }]}>
										<Text style={[styles.DescName, myStyles.FontBold]}>
											{item.name ? item.name : 'Your Name'}
											{
												item.verified === true &&
												<>&nbsp;
													<MdIcon name="check-decagram" size={15} color={color.pink} />
												</>
											}
										</Text>
										{
											item.title &&
											<Text style={[myStyles.FontMedium, { color: color.darkScale }]} numberOfLines={2}>
												{item.title}
											</Text>
										}
										{
											date && item.start_date &&
											<Text style={[myStyles.FontLight, { color: color.darkScale, fontSize: 13, marginTop: 3 }]}>
												{
													yearOnly ?
														item.start_date.split('-')[0]
														:
														`${modifyMonth(item.start_date)} ${started}  -  ${item.end_date ? `${modifyMonth(item.start_date)} ${ended}` : 'Now'}`
												}
											</Text>
										}
									</View>
								</TouchableOpacity>
							)
						}
						return false
					}}
					keyExtractor={item => item.id.toString()}
				/>
				{/* <TouchableOpacity style={styles.AddButton}
					onPress={clickAdd} >
					<Text style={[styles.TextButton, myStyles.FontMedium]}>
						Add new
					</Text>
					<IonIcon name="ios-add" size={25} color={color.primary} />
				</TouchableOpacity> */}
			</View>
		)
	}
}

export default renderListMax;

const styles = StyleSheet.create({
	Container: {
		backgroundColor: color.white,
		marginHorizontal: 10,
		marginBottom: 10,
		borderRadius: 10,
		borderWidth: 2,
		borderColor: color.gray
	},
	Header: {
		height: 40,
		backgroundColor: color.white,
		borderTopLeftRadius: 10,
		borderTopRightRadius: 10,
		borderBottomWidth: 1,
		borderBottomColor: color.gray,
		paddingLeft: 15,
		justifyContent: 'center',
		marginBottom: 10,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	TextHeader: {
		color: color.primary,
		fontWeight: 'bold',
		fontSize: 17
	},
	ViewMore: {
		flexDirection: 'row',
		paddingRight: 15,
		alignItems: 'center',
		justifyContent: 'center'
	},
	TextViewMore: {
		color: color.darkScale,
		fontFamily: 'DIN-Bold',
		fontSize: 15,
		paddingRight: 5,
		marginTop: -5
	},
	DataItem: {
		borderColor: color.gray,
		width: '100%',
		borderWidth: 2,
		flexDirection: 'row',
		marginBottom: 10,
		borderRadius: 10,
		paddingVertical: 6
	},
	LogoWrap: {
		height: 58,
		width: 68,
		alignItems: 'center',
		justifyContent: 'center',
	},
	Logo: {
		height: 45,
		width: 45,
		borderRadius: 24
	},
	Desc: {
		justifyContent: 'space-around',
		paddingVertical: 5,
		paddingLeft: 10,
		paddingRight: 10
	},
	DescName: {
		fontWeight: 'bold',
		color: color.darkScale,
		fontSize: 16
	},
	AddButton: {
		paddingVertical: 6,
		backgroundColor: color.cyan,
		marginHorizontal: 10,
		marginBottom: 10,
		borderRadius: 8,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center'
	},
	TextButton: {
		paddingHorizontal: 10,
		fontWeight: 'bold',
		color: color.primary,
		fontSize: 16
	}
})