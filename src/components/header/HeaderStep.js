import React from 'react'
import { View } from 'react-native'

export default HeaderStep = (props) => {
	return (
		<View style={{ 
			height: 50, 
			flexDirection: 'row', 
			marginTop: 10,
			paddingRight: 20, 
			justifyContent: 'space-around',
		}}>
			{
				props.step.map((item, i) => (
					<View style={{ 
						width: '13.5%',
						paddingVertical: 20, 
						flexDirection: 'row' 
					}} 
					key={i} >
						<View style={[{
							zIndex: -999,
						}, item == true ? 
							{ backgroundColor: props.colorNonActive, flex: 1 } 
							: 
							{ backgroundColor: '#fff', flex: 1 }
						]}>
						</View>
						<View style={{ 
							width: 13, 
							justifyContent: 'center', 
							alignItems: 'flex-end',
						}}>
							<View style={[{ 
								width: 20, 
								height: 20, 
								borderRadius: 50, 
								position: 'absolute', 
								right: -3, 
								zIndex: 999,
							}, item == true ? 
								{ backgroundColor: props.colorActive } 
								: 
								{ backgroundColor: props.colorNonActive }
							]}>
								
							</View>
						</View>
					</View>
				))
			}

		</View>
	)
}